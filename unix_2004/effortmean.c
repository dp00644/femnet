#define PRINCIPAL 0
#include "4c19.h"

double effortmean()
/* Calcul de l effort moyen sur toutes les coordonnees des noeuds*/
{
  int zi;
  double Rx,RR;

  Rx = 0.0;
  for (zi = 1; zi<= NOMBRE_NOEUDS; zi++)
  {
    Rx += sqrt(wa[3*zi-2]*wa[3*zi-2] + wa[3*zi-1]*wa[3*zi-1] + wa[3*zi-0]*wa[3*zi-0]);
  }
  RR = Rx / (double) NOMBRE_NOEUDS;

  return RR;
}


