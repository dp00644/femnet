#define INFINI			9e30
#define RAD			6.283185
#define MLEN 90
#define NB_MAX_COURBES		30
#define NB_MAX_ELEMENTS		500
#define NB_MAX_Q_STAT_DEFL 	200
#define NB_MAX_NODES		200

#define ECH_GAUCHE		1
#define ECH_DROITE		2

#define AFF_SYMBOLE		1
#define AFF_TRAIT		2
#define AFF_NODE		4
#define AFF_RIEN		8

#define RESOLUTION_X 		1.
#define RESOLUTION_Y 		1.

#define MARGE_X_PS		10.
#define MARGE_Y_PS		10.
#define RESOLUTION_X_PS 	(600.-2*MARGE_X_PS)
#define RESOLUTION_Y_PS 	(800.-2*MARGE_Y_PS)

#define NOM_POLICE_PS 		"Times-Roman"


#define LINE_MOTIF_0 	0
#define LINE_MOTIF_1 	1
#define LINE_MOTIF_2 	2
#define LINE_MOTIF_3 	3
#define LINE_MOTIF_4 	4
#define LINE_MOTIF_5	5
#define LINE_MOTIF_6 	6
#define LINE_MOTIF_7 	7
#define LINE_MOTIF_8 	8
#define LINE_MOTIF_9 	9

#ifndef TRUE
#define TRUE  1
#define FALSE 0
#endif
