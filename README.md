# FEMNET

Modeling mechanical behaviour of netting cables and bars structures.
FEMNET is a numerical modelling dedicated to mechanical assessment of flexible structures such as fishing gear and fish cage.
This numerical modelling is based on the finite element method with a specific emphasis on netting structures.
Netting is a main component of fish cage and fishing gear.
This numerical modelling using the finite element method also takes into account cables, bars, floats and netting.
FEMNET has been used in several scientific papers and book.

## Installation

Download the source code from https://gitlab.ifremer.fr/dp00644/femnet

In the following these files are placed in ~/hexa.

Libsx is the graphics library which is  used by FEMNET. You could use ~/hexa/libsx/src/libsx.a as explained in the following, or you could find libsx on internet, compile and use libsx.a you get.

Edit makefile of ~/hexa/phobos_2005,  batz and  ~/hexa/hector and place libsx.a with the right path.

Do make in ~/hexa/lib_dp, in ~/hexa/unix_2004, in ~/hexa/dyna2, in ~/hexa/phobos_2005, in ~/hexa/batz and in ~/hexa/hector. 

## Usage

Have a look at ~/hexa/data_2001/readme/readme.pdf

