#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include "libsx.h"
#include "hector.h"
#include <string.h>		/*pas sur que ce soit utile : provient de phobos.h*/
#include <stdlib.h>


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

typedef struct {
  Widget window;
  int cancelled;

  Widget *w;
  TagList *tags;
  
} MReqData;



			/*fonction qui donne les parametres necessaires a maillage_losange_fond_ecran2
			l utilisateur peux entrer ses propres parametres*/

void coordonnees_utilisateur()
   {
   int triangle_rectangle;
   
   		/*l utilisateur donne les coordonnees du 1er et du 2eme coin*/
		
   if(GetMenuItemChecked(w[3])==0) demande_coordonnees_utilisateur();
   else demande_lcm_utilisateur();
   
   nb_maille_fenetre = taille_fenetre/(sqrt(2.0)*lcm);
   rapport=nb_maille_fenetre/taille_fenetre;
   for (triangle_rectangle = 1;triangle_rectangle<=2;triangle_rectangle++)
      {
      if (triangle_rectangle==1)
         {
	 UU1[1] = 0.0; 			VV1[1] = 0.0;		/*coordonnees en nb de mailles des trois coins du triangle a mailler*/
	 UU2[1] = 0.0; 			VV2[1] = nb_maille_fenetre;
	 UU3[1] = nb_maille_fenetre; 	VV3[1] = nb_maille_fenetre;

	 
	 xx1[1] = 0.0; 			yy1[1] = 0.0;			zz1[1] = 0.0;	/*memes coordonnees, taille physique du chalut*/
	 xx2[1] = 0.0; 			yy2[1] = taille_fenetre;	zz2[1] = 0.0;
	 xx3[1] = taille_fenetre; 	yy3[1] = taille_fenetre;	zz3[1] = 0.0;
	 
	 }
       
      if (triangle_rectangle==2)
         {
	 
	 UU1[2] = 0.0; 			VV1[2] = 0.0;		/*coordonnees en nb de mailles des trois coins du triangle a mailler*/
	 UU2[2] = nb_maille_fenetre; 	VV2[2] = 0.0;
	 UU3[2] = nb_maille_fenetre; 	VV3[2] = nb_maille_fenetre;
	 
	 xx1[2] = 0.0; 			yy1[2] = 0.0;			zz1[2] = 0.0;	
	 xx2[2] = taille_fenetre; 	yy2[2] = 0.0;			zz2[2] = 0.0;
	 xx3[2] = taille_fenetre; 	yy3[2] = taille_fenetre;	zz3[2] = 0.0;
	 
	 }
       
       }
   /*
   printf("triangle 1\n");
   printf("UU1 %8.2f  VV1 %8.2f  UU2 %8.2f  VV2 %8.2f  UU3 %8.2f  VV3 %8.2f\n",UU1[1],VV1[1],UU2[1],VV2[1],UU3[1],VV3[1]);
   
   printf("triangle 2\n");
   printf("UU1 %8.2f  VV1 %8.2f  UU2 %8.2f  VV2 %8.2f  UU3 %8.2f  VV3 %8.2f\n",UU1[2],VV1[2],UU2[2],VV2[2],UU3[2],VV3[2]);

   printf("triangle 1\n");
   printf("xx1 %8.2f  yy1 %8.2f  xx2 %8.2f  yy2 %8.2f  xx3 %8.2f  yy3 %8.2f\n",xx1[1],yy1[1],xx2[1],yy2[1],xx3[1],yy3[1]);
   
   printf("triangle 2\n");
   printf("xx1 %8.2f  yy1 %8.2f  xx2 %8.2f  yy2 %8.2f  xx3 %8.2f  yy3 %8.2f\n",xx1[2],yy1[2],xx2[2],yy2[2],xx3[2],yy3[2]);
   */
   }

	   
void demande_coordonnees_utilisateur() /*les coordonnees des deux coins*/
   {
   int i;
   TagList tags[] = 
  	   {
    	   {TAG_FLOAT,	"window size (m) : ",	&taille_fenetre,  	TAG_INIT}, 
     	   {TAG_FLOAT,	"mesh side length (m) : ",	&lcm,  	TAG_INIT}, 
 	   {TAG_DONE,	NULL,           	NULL,     		TAG_NOINIT}
  	   };

   if(GetValues_2(tags))
    	   printf("Cancelled\n");
   else
    	   {
   	   pas = sqrt(2.0)/2.0*lcm;
    	   printf("lcm %f\n",lcm);
    	   printf("pas %f\n",pas);
    	   for (i=1;i<=chalut.nb_panneau;i++)
    	      {
     	      panneau[i].raideur_traction = raideur_traction;
    	      panneau[i].raideur_compression = raideur_compression;
    	      panneau[i].raideur_ouverture = raideur_ouverture;		/*les parametres sont les memes pour tous les panneaux*/
    	      panneau[i].longueur_repos = maille_au_repos;		/*ils peuvent etre modifies ensuite cf modifier_parametres_pan()*/
    	      panneau[i].rho = mass_vol;
    	      panneau[i].diam_hydro = diam_hydro;
    	      panneau[i].largeurnoeud = largeur_noeud;
    	      panneau[i].cdnormal = cd_normal;
    	      panneau[i].ftangent = cd_tang;
    	      panneau[i].pas_maillage = pas_maillage;
    	      panneau[i].type_noeud = type_noeud_interieur;
    	      panneau[i].type_maillage = type_maillage;
    	      }
    	   for (i=1;i<=chalut.nb_element;i++)
     	      {
    	      element[i].raideur_traction = raideur_traction_el;
    	      element[i].raideur_compression = raideur_compression_el;
    	      element[i].longueur_repos = long_repos_el;
    	      element[i].rho = mass_vol_el;
    	      element[i].diam_hydro = diam_hydro_el;
    	      element[i].cdnormal = coef_trainee_norm_el;
    	      element[i].ftangent = coef_trainee_tang_el;
    	      element[i].nb_barre = nb_barre_el;
    	      element[i].type_noeud = type_noeud_int_el;
   	      }
   	   
    	   }
   }
  
			/*la fonction ne demande que le parametre lcm*/

void demande_lcm_utilisateur() 
   {
   int i;
   TagList tags[] = 
  	   {
     	   {TAG_FLOAT,	"mesh side length (m) : ",	&lcm,  	TAG_INIT}, 
 	   {TAG_DONE,	NULL,           	NULL,     		TAG_NOINIT}
  	   };

   if(GetValues_2(tags))
    	   printf("Cancelled\n");
   else
    	   {
    	   printf("lcm %f\n",lcm);
    	   pas = sqrt(2.0)/2.0*lcm;
    	   printf("pas %f\n",pas);
    	   for (i=1;i<=chalut.nb_panneau;i++)
    	      {
     	      panneau[i].raideur_traction = raideur_traction;
    	      panneau[i].raideur_compression = raideur_compression;
    	      panneau[i].raideur_ouverture = raideur_ouverture;		/*les parametres sont les memes pour tous les panneaux*/
    	      panneau[i].longueur_repos = maille_au_repos;		/*ils peuvent etre modifies ensuite cf modifier_parametres_pan()*/
    	      panneau[i].rho = mass_vol;
    	      panneau[i].diam_hydro = diam_hydro;
    	      panneau[i].largeurnoeud = largeur_noeud;
    	      panneau[i].cdnormal = cd_normal;
    	      panneau[i].ftangent = cd_tang;
    	      panneau[i].pas_maillage = pas_maillage;
    	      panneau[i].type_noeud = type_noeud_interieur;
    	      panneau[i].type_maillage = type_maillage;
    	      }
    	   for (i=1;i<=chalut.nb_element;i++)
     	      {
    	      element[i].raideur_traction = raideur_traction_el;
    	      element[i].raideur_compression = raideur_compression_el;
    	      element[i].longueur_repos = long_repos_el;
    	      element[i].rho = mass_vol_el;
    	      element[i].diam_hydro = diam_hydro_el;
    	      element[i].cdnormal = coef_trainee_norm_el;
    	      element[i].ftangent = coef_trainee_tang_el;
    	      element[i].nb_barre = nb_barre_el;
    	      element[i].type_noeud = type_noeud_int_el;
    	      
     	      /*printf("element[%d].diam_hydro %f\n",i,element[i].diam_hydro);*/
   	      }
    	   }
   }

void changer_lcm(Widget w, void *data)
  	   {
  	   maillage_losange_fond_ecran2();
  	   }


 
 				/************************************************
 				**********anciennement multireq.c****************
 				************************************************/
 				
 
/* protos */
static void mreq_ok(Widget w, MReqData *mdata);
static void mreq_cancel(Widget w, MReqData *mdata);
     
int GetValues_2(TagList *tags)
{
  MReqData mdata;
  int num_tags=0, num_widg=0, num_labels=0;
  int i, w, l;
  Widget *label_w, ok_w, cancel_w;
  char string[256], window_name[256]="user coordinates";
  int maxwidth = 0, widest_label = -1;
  
  /* first count the number of tag items and required widgets */
  for(; tags[num_tags].tag != TAG_DONE; num_tags++)
    {
      if(tags[num_tags].tag == TAG_LABEL)
	{
	  num_labels++;
	  continue;
	}
      else if(tags[num_tags].tag == TAG_WINDOW_LABEL)
	{
	  if(!tags[num_tags].label)
	    {
	      fprintf(stderr, "Invalid window name passed to GetValues_2()\n");
	      return(TRUE);
	    }

	  strcpy(window_name, tags[num_tags].label);
	  continue;
	}
      
      /* determine the widest label */
      if(strlen(tags[num_tags].label) > maxwidth)
	{
	  maxwidth = strlen(tags[num_tags].label);
	  widest_label = num_labels;
	}

      num_labels++;
      num_widg++;
    }
      
  /* allocate mem for the widgets */
  if(!(mdata.w = (Widget *)malloc(num_widg * sizeof(Widget))))
    return(TRUE);
  if(!(label_w = (Widget *)malloc(num_labels * sizeof(Widget))))
    {
      free(mdata.w);
      return(TRUE);
    }
  
  mdata.window = MakeWindow(window_name, SAME_DISPLAY, EXCLUSIVE_WINDOW);
  mdata.tags = tags;

  /* create the label widgets first */
  for(i=0, l=0; i<num_tags; i++)
    {
      if(tags[i].tag == TAG_WINDOW_LABEL)
	continue;
      
      label_w[l] = MakeLabel(tags[i].label);
      if(l)
	SetWidgetPos(label_w[l], PLACE_UNDER, label_w[l-1], NO_CARE, NULL);
      l++;
    }
  
  for(i=0, w=0, l=0; i<num_tags; i++)
    {
      switch(tags[i].tag)
	{
	case TAG_STRING:
	  if(tags[i].init)
	    mdata.w[w]=MakeStringEntry((char *)tags[i].data, 250, NULL, NULL);
	  else
	    mdata.w[w] = MakeStringEntry(NULL, 250, NULL, NULL);
	  
	  if(l)
	    SetWidgetPos(mdata.w[w], PLACE_UNDER, label_w[l-1],
			 PLACE_RIGHT, label_w[widest_label]);
	  else
	    SetWidgetPos(mdata.w[w], PLACE_RIGHT, label_w[widest_label],
			 NO_CARE, NULL);

	  l++;
	  w++;
	  break;

	case TAG_INT:
	  if(tags[i].init)
	    {
	      sprintf(string, "%d", *((int *)tags[i].data));
	      mdata.w[w] = MakeStringEntry(string, 250, NULL, NULL);
	    }
	  else
	    mdata.w[w] = MakeStringEntry(NULL, 250, NULL, NULL);
	  
	  if(l)
	    SetWidgetPos(mdata.w[w], PLACE_UNDER, label_w[l-1],
			 PLACE_RIGHT, label_w[widest_label]);
	  else
	    SetWidgetPos(mdata.w[w], PLACE_RIGHT, label_w[widest_label],
			 NO_CARE, NULL);

	  l++;
	  w++;
	  break;

	case TAG_FLOAT:
	  if(tags[i].init)
	    {
	      sprintf(string, "%f", *((float *)tags[i].data));
	      mdata.w[w] = MakeStringEntry(string, 250, NULL, NULL);
	    }
	  else
	    mdata.w[w] = MakeStringEntry(NULL, 250, NULL, NULL);
	  
	  if(l)
	    SetWidgetPos(mdata.w[w], PLACE_UNDER, label_w[l-1],
			 PLACE_RIGHT, label_w[widest_label]);
	  else
	    SetWidgetPos(mdata.w[w], PLACE_RIGHT, label_w[widest_label],
			 NO_CARE, NULL);

	  w++;
	  l++;
	  break;

	case TAG_LABEL:
	  l++;
	  break;
	case TAG_WINDOW_LABEL:
	  break;
	  
	default:
	  fprintf(stderr, "GetValues_2() : Invalid tag item %d\n", tags[i].tag);
	}
    }
  
  ok_w = MakeButton("Ok", (void *)mreq_ok, &mdata);
  SetWidgetPos(ok_w, PLACE_UNDER, label_w[num_labels-1], NO_CARE, NULL);

  cancel_w = MakeButton("Cancel", (void *)mreq_cancel, &mdata);
  SetWidgetPos(cancel_w, PLACE_UNDER, label_w[num_labels-1],
	       PLACE_RIGHT, ok_w);

  ShowDisplay();			/*sans doute inutile*/
  MainLoop();
  
  SetCurrentWindow(ORIGINAL_WINDOW);

  /* check for cancel */
  if(mdata.cancelled)
    return(TRUE);

  free(mdata.w);
  free(label_w);
  
  return(FALSE);
}

static void mreq_ok(Widget w, MReqData *mdata)
{
  int widg_num;
  TagList *tagptr;
  char *cptr;
  
  /* extract the info from the widgets */
  for(widg_num=0, tagptr=mdata->tags; tagptr->tag != TAG_DONE; tagptr++)
    {
      switch(tagptr->tag)
	{
	case TAG_STRING:
	  cptr = GetStringEntry(mdata->w[widg_num++]);
	  strcpy(tagptr->data, cptr);
	  break;
	case TAG_INT:
	  cptr = GetStringEntry(mdata->w[widg_num++]);
	  *((int *)tagptr->data) = atoi(cptr);
	  break;
	case TAG_FLOAT:
	  cptr = GetStringEntry(mdata->w[widg_num++]);
	  *((float *)tagptr->data) = atof(cptr);
	  break;
	case TAG_WINDOW_LABEL:
	case TAG_LABEL:
	  break;
	default:
	  fprintf(stderr, "GetValues_2() : Invalid tag item %d\n", tagptr->tag);
	}
    }
  
  mdata->cancelled = FALSE;
  SetCurrentWindow(mdata->window);
  CloseWindow();
}


static void mreq_cancel(Widget w, MReqData *mdata)
	{
  	mdata->cancelled = TRUE;
  	SetCurrentWindow(mdata->window);
  	CloseWindow();
	}


















 
 
  
