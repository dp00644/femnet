#include "hector.h"              /* where program specific stuff is defined */
int button_number,i;
float x_local,y_local,u_local,v_local;


void contour()		/*remplit la BD des points qui vont creer le contour du panneau*/
	{
	SetMenuItemChecked(w[4],0);	/*zoom a zero*/
	//SetMenuItemChecked(w[20],0);
	//SetMenuItemChecked(w[21],0);

	//i=GetMenuItemChecked(w[6]);
	i=0;
	if ((i==1)||(creation_panneau_terminee))
		{
		printf("il existe deja un contour en cours de creation\n");	/*si i=1*/
		printf("ou alors tous les panneaux ont deja ete cree\n");	/*si creation_panneau_terminee = 1*/
		}
	else
		{
		//SetMenuItemChecked(w[6],1);
		//SetMenuItemChecked(w[7],0);
		
		if (reprendre_le_contour) /*s il faut reprendre le contour on n initialise pas*/
			{
			}
		else 			/*sinon, on cree un nouveau contour : on initialise*/
			{
			chalut.nb_panneau++;
			panneau[chalut.nb_panneau].flag_ordre_maillage=0;
			panneau[chalut.nb_panneau].nb_point=0;
			panneau[chalut.nb_panneau].contour_ferme=0;
			}
		
		printf("chalut.nb_panneau  %d\n",chalut.nb_panneau);
		SetButtonUpCB(w[2],    button_up_contour);
		}
	}

void button_up_contour(Widget w, int which_button, int x, int y, void *data)
	{
	
	Color(BLUE);

	if (which_button == 1)	/*clic gauche*/
		{
		panneau[chalut.nb_panneau].nb_point++;
		DrawBox(x,y,6,3);
		nb_point=panneau[chalut.nb_panneau].nb_point;
		realloc_panneau();
		
		x_local=binx + (float) x/ RESOLUTION_X_FEN*(baxx-binx);		/*coord x qui prend en compte le zoom. origine en bas a gauche*/
		y_local=baxy - (float) y/ RESOLUTION_Y_FEN*(baxy-biny);		/*idem*/
		u_local=rapport*x_local;
		v_local=rapport*y_local;
		if (mode_accrochage)
		     {
		     /*u_local=nint (u_local/pas_accrochage)*pas_accrochage;     ...version DEC		
		     	   v_local=nint (v_local/pas_accrochage)*pas_accrochage;*/

		     u_local=rint (u_local/pas_accrochage)*pas_accrochage;	/*rint (version linux) arrondit a l entier le plus proche*/	
		     v_local=rint (v_local/pas_accrochage)*pas_accrochage;
		     
		     x_local=u_local/rapport;
		     y_local=v_local/rapport;
		     }
		printf("point numero %d  u %f v %f\n",nb_point,u_local,v_local);
		
		if((x_local==panneau[chalut.nb_panneau].x[nb_point-1])&&(y_local==panneau[chalut.nb_panneau].y[nb_point-1])&&(panneau[chalut.nb_panneau].nb_point>1)) 
			{
			printf("point deja selectionne\n");
			panneau[chalut.nb_panneau].nb_point--;	/*le point avait ete incremente alors qu il a deja ete selectionne juste avant : annulation*/
			}
		else
			{

			panneau[chalut.nb_panneau].x[nb_point] = x_local;	/*stockage*/
			panneau[chalut.nb_panneau].y[nb_point] = y_local;
			panneau[chalut.nb_panneau].U[nb_point] = u_local;
			panneau[chalut.nb_panneau].V[nb_point] = v_local;
			/*Realloc_int(panneau[chalut.nb_panneau].type_du_noeud,nb_point+1);*/
	/**********************************************/
	panneau[chalut.nb_panneau].type_du_noeud = (int *) realloc(panneau[chalut.nb_panneau].type_du_noeud, (1+ nb_point) * sizeof(int));
	if ( panneau[chalut.nb_panneau].type_du_noeud   == NULL)
		{
		printf("panneau[chalut.nb_panneau].type_du_noeud  2 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
			affectation_carateristiques_meca_pan();

			/*
			printf("panneau[%d].x[%d] = %f  ",chalut.nb_panneau,nb_point,panneau[chalut.nb_panneau].x[nb_point]);
			printf("panneau[%d].y[%d] = %f  \n",chalut.nb_panneau,nb_point,panneau[chalut.nb_panneau].y[nb_point]);
			printf("panneau[%d].U[%d] = %f  ",chalut.nb_panneau,nb_point,panneau[chalut.nb_panneau].U[nb_point]);
			printf("panneau[%d].V[%d] = %f  \n",chalut.nb_panneau,nb_point,panneau[chalut.nb_panneau].V[nb_point]);
			*/

			if (panneau[chalut.nb_panneau].nb_point > 1)	/*des qu on a deux points, on dessine une droite*/
				{
				xxx1 = panneau[chalut.nb_panneau].x[nb_point - 1];
				xxx2 = panneau[chalut.nb_panneau].x[nb_point];
				yyy1 = panneau[chalut.nb_panneau].y[nb_point - 1];
				yyy2 = panneau[chalut.nb_panneau].y[nb_point];

				xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);	/*prise en compte du zoom*/
				xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
				yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
				yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);


				Line(xxx1,yyy1,xxx2,yyy2);
				}
			}
		panneau[chalut.nb_panneau].lcm = lcm;
		/*printf("panneau[chalut.nb_panneau].lcm = %f\n",panneau[chalut.nb_panneau].lcm);*/
		reprendre_le_contour=FALSE;	/*contour termine, il ne sera pas la peine de le reprendre*/
		}
		
	}
	

void fermer_contour()
	{
	//i=GetMenuItemChecked(w[6]);
	i=0;
	SetMenuItemChecked(w[4],0);	/*zoom a zero*/
	if (i==0)
		{
		printf("il faut creer un nouveau contour avant d utiliser 'fermer contour'\n");
		}
	else
		{
		SetButtonUpCB(w[2],    NULL);
		//SetMenuItemChecked(w[6],0);
		panneau[chalut.nb_panneau].contour_ferme=1;
		
		xxx1 = panneau[chalut.nb_panneau].x[1];
		xxx2 = panneau[chalut.nb_panneau].x[nb_point];
		yyy1 = panneau[chalut.nb_panneau].y[1];
		yyy2 = panneau[chalut.nb_panneau].y[nb_point];

		xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);
		xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
		yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
		yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);
		   
		Line(xxx1,yyy1,xxx2,yyy2);
		}
	dessiner();
	}

void dessiner_contour()		/*appelee par redisplay*/
	{
	int n_pt,n_pan;
	Color(BLUE);
	if (chalut.nb_panneau>=1)
	   {
	   for (n_pan=1;n_pan<=chalut.nb_panneau;n_pan++)
	      {
	      if (panneau[n_pan].nb_point >= 2)
		 {
		 for (n_pt=2;n_pt<=panneau[n_pan].nb_point;n_pt++)
		    {	
		    xxx1 = panneau[n_pan].x[n_pt - 1];
		    xxx2 = panneau[n_pan].x[n_pt];
		    yyy1 = panneau[n_pan].y[n_pt - 1];
		    yyy2 = panneau[n_pan].y[n_pt];
		    
		    xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);
		    xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
		    yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
		    yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);

		    Line(xxx1,yyy1,xxx2,yyy2);
		    }

		 if (panneau[n_pan].contour_ferme)
	      	   {
		   xxx1 = panneau[n_pan].x[1];
		   xxx2 = panneau[n_pan].x[panneau[n_pan].nb_point];
		   yyy1 = panneau[n_pan].y[1];
		   yyy2 = panneau[n_pan].y[panneau[n_pan].nb_point];

		   xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);
		   xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
		   yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
		   yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);
		   
		   Line(xxx1,yyy1,xxx2,yyy2);
		   }
		 }
	      }
	   }
	}


	
void creer_element()		/*remplit la BD des points qui vont creer le contour du panneau*/
	{
	SetMenuItemChecked(w[4],0);	/*zoom a zero*/
	//SetMenuItemChecked(w[20],0);	/*effacer a zero*/
	//SetMenuItemChecked(w[21],0);	/*effacer liaison a zero*/
	
	//i=GetMenuItemChecked(w[9]);
	i=0;
	if (i==1)
		{
		printf("il existe deja un element en cours de creation\n");
		}
	else
		{
		//SetMenuItemChecked(w[9],1);
		//SetMenuItemChecked(w[7],0);
		
		if (reprendre_element) 
			{
			}
		else 
			{
			chalut.nb_element++;
			element[chalut.nb_element].flag_ordre_maillage=0;
			element[chalut.nb_element].nb_point=0;
			printf("chalut.nb_element  %d\n",chalut.nb_element);
			}
		
		SetButtonUpCB(w[2],    button_up_element);
		}
	}


void button_up_element(Widget w, int which_button, int x, int y, void *data)
	{
	Color(BLUE);
	if (which_button == 1)
	    {
	    Color(RED);
	        
	    DrawBox(x,y,3,3);
	    element[chalut.nb_element].nb_point++;
	    nb_point_el=element[chalut.nb_element].nb_point;
	    printf("point numero %d\n\n",nb_point_el);

	    x_local=binx + (float) x/ RESOLUTION_X_FEN*(baxx-binx);
	    y_local=baxy - (float) y/ RESOLUTION_Y_FEN*(baxy-biny);
	    u_local=rapport*x_local;
	    v_local=rapport*y_local;

	    if (mode_accrochage)
	       {
	       /*u_local=nint (u_local/pas_accrochage)*pas_accrochage;    ... version DEC
		     v_local=nint (v_local/pas_accrochage)*pas_accrochage;*/

	       u_local=rint (u_local/pas_accrochage)*pas_accrochage;	/*version Linux*/
	       v_local=rint (v_local/pas_accrochage)*pas_accrochage;

	       x_local=u_local/rapport;
	       y_local=v_local/rapport;
	       }

	    if((x_local==element[chalut.nb_element].x[nb_point_el-1])&&(y_local==element[chalut.nb_element].y[nb_point_el-1])&&(element[chalut.nb_element].nb_point>1)) 
		    {
		    printf("point deja selectionne\n");
		    element[chalut.nb_element].nb_point--;
		    }
	    else
		    {
		    element[chalut.nb_element].x[nb_point_el] = x_local;
		    element[chalut.nb_element].y[nb_point_el] = y_local;
	/**********************************************/
	element[chalut.nb_element].type_du_noeud = (int *) realloc(element[chalut.nb_element].type_du_noeud, (1+ nb_point_el) * sizeof(int));
	if ( element[chalut.nb_element].type_du_noeud   == NULL)
		{
		printf("element[chalut.nb_element].type_du_noeud  2 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
		    affectation_carateristiques_meca_ele();
		    /*printf("element[chalut.nb_element].type_du_noeud[nb_point_el] %d\n",element[chalut.nb_element].type_du_noeud[nb_point_el]);*/
	    
		    if (element[chalut.nb_element].nb_point>=2)
			{
			xxx1 = element[chalut.nb_element].x[1];
			xxx2 = element[chalut.nb_element].x[2];
			yyy1 = element[chalut.nb_element].y[1];
			yyy2 = element[chalut.nb_element].y[2];

			xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);
			xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
			yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
			yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);

			Line(xxx1,yyy1,xxx2,yyy2);
			stop();					/*arret de la fonction : les deux points de l element sont crees*/
			}
		     }
	    }
	
	element[chalut.nb_element].lcm = lcm;		/*lcm : longueur de cote de maille*/
	reprendre_element=FALSE;			/*l element a ete cree jusqu au bout, il n y a pas besoin de le reprendre*/
	}
	

void stop()
	{
	SetButtonUpCB(w[2],    NULL);
	//SetMenuItemChecked(w[9],0);
	printf("fin creation element %d\n",chalut.nb_element);
	dessiner();
	}

void dessiner_element()
	{
	int n_ele;
	Color(RED);
		
	if (chalut.nb_element>=1)
	   {
	   for (n_ele=1;n_ele<=chalut.nb_element;n_ele++)
	      {
	      if (element[n_ele].nb_point>=2)
		{
		xxx1 = element[n_ele].x[1];
		xxx2 = element[n_ele].x[2];
		yyy1 = element[n_ele].y[1];
		yyy2 = element[n_ele].y[2];
		
		xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);
		xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
		yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
		yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);
		   
		Line(xxx1,yyy1,xxx2,yyy2);
		}
	      }
	   }
	}
	
void creer_liaison()
	{
	int k;
	SetMenuItemChecked(w[4],0);		/*zoom a zero*/
	//SetMenuItemChecked(w[20],0);		/*effacer a zero*/
	//SetMenuItemChecked(w[21],0);		/*effacer liaison a zero*/
	
	k=GetMenuItemChecked(w[15]); 		/*creer_liaison*/
	if (k==0||reprendre_liaison)
		{
		SetMenuItemChecked(w[15],1); 	/*creer_liaison a 1*/
		if (reprendre_liaison==0)
		   {
		   chalut.nb_liaison++;
		   liaison[chalut.nb_liaison].nb_pt=0;
		   }
		SetButtonUpCB(w[2],    button_up_liaison);
		}
	else 
		{
		printf("\nil y a deja une liaison en cours de creation\n");
		}
	}


void button_up_liaison(Widget w, int which_button, int x, int y, void *data)	
/*fonction qui permet de retrouver des points existants juste en cliquant sur la figure*/
    {
    int num_entite,num_pt,nb_pt;
    float abs_x,x_liaison,dist,mini_dist;
    float abs_y,y_liaison,lx,ly,lx2,ly2;
   
    Color(WHITE);
    Color(BLACK);
    if (which_button == 1)		/*selection du point a lier*/
	{
	
	liaison[chalut.nb_liaison].nb_pt++;		/*stockage du point le plus proche dans la struct liaison*/
	nb_pt=liaison[chalut.nb_liaison].nb_pt;		/*numero du point pour cette liaison*/
	
	/**********************************************/
	liaison[chalut.nb_liaison].type = (int *) realloc(liaison[chalut.nb_liaison].type, (1+ nb_pt) * sizeof(int));
	if (liaison[chalut.nb_liaison].type    == NULL)
		{
		printf("liaison[chalut.nb_liaison].type  2 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
	liaison[chalut.nb_liaison].num_entite = (int *) realloc(liaison[chalut.nb_liaison].num_entite, (1+ nb_pt) * sizeof(int));
	if (liaison[chalut.nb_liaison].num_entite    == NULL)
		{
		printf("liaison[chalut.nb_liaison].num_entite  2 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
	liaison[chalut.nb_liaison].nd = (int *) realloc(liaison[chalut.nb_liaison].nd, (1+ nb_pt) * sizeof(int));
	if (liaison[chalut.nb_liaison].nd    == NULL)
		{
		printf("liaison[chalut.nb_liaison].nd  2 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
	liaison[chalut.nb_liaison].x = (float *) realloc(liaison[chalut.nb_liaison].x, (1+ nb_pt) * sizeof(float));
	if (liaison[chalut.nb_liaison].x    == NULL)
		{
		printf("liaison[chalut.nb_liaison].x  2 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
	liaison[chalut.nb_liaison].y = (float *) realloc(liaison[chalut.nb_liaison].y, (1+ nb_pt) * sizeof(float));
	if (liaison[chalut.nb_liaison].y    == NULL)
		{
		printf("liaison[chalut.nb_liaison].y  2 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
	liaison[chalut.nb_liaison].type_du_noeud = (int *) realloc(liaison[chalut.nb_liaison].type_du_noeud, (1+ nb_pt) * sizeof(int));
	if (liaison[chalut.nb_liaison].type_du_noeud    == NULL)
		{
		printf("liaison[chalut.nb_liaison].type_du_noeud  2 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
	
	//DrawBox(x,y,3,3);
	liaison[chalut.nb_liaison].type[nb_pt]=0;			/*type 0 : panneau ; type 1 : element INITIALISATION (pas sur que ce soit necessaire)*/
	mini_dist=10000.0;						/*FAIRE calculer mini_dist a partir des dimensions donnees par l utilisateur*/
	x_liaison=binx + (float) x/ RESOLUTION_X_FEN*(baxx-binx);	/*coord x qui prend en compte le zoom. origine en bas a gauche*/
	y_liaison=baxy - (float) y/ RESOLUTION_Y_FEN*(baxy-biny);	/*idem*/
	//printf("\n");
	for (num_entite=1 ; num_entite<=chalut.nb_panneau ; num_entite++)	/*num_entite : num du panneau ou num de l element suivant le cas*/
	   {
	   for (num_pt=1 ; num_pt<=panneau[num_entite].nb_point ; num_pt++)
	      {
	      abs_x=x_liaison - panneau[num_entite].x[num_pt];		/*abs_x : dist selon x entre le point clique et le point en cours (panneau[].x[])*/
	      abs_y=y_liaison - panneau[num_entite].y[num_pt];
	      dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));			/*calcul de la distance*/
	      
	      if (dist<=mini_dist)			/*recuperation des donnees du point le plus proche sur ce panneau*/
	         {
	         mini_dist=dist;					/*distance mini*/
	         liaison[chalut.nb_liaison].type[nb_pt]=0;						/*indique si on a a faire a un panneau (0) ou un element (1)*/
	         liaison[chalut.nb_liaison].num_entite[nb_pt]=num_entite;				/*numero de panneau (ou numero d element)*/
	         liaison[chalut.nb_liaison].nd[nb_pt]=num_pt;						/*numero du point*/
	         liaison[chalut.nb_liaison].type_du_noeud[nb_pt]=panneau[num_entite].type_du_noeud[num_pt];
	        x = (panneau[liaison[chalut.nb_liaison].num_entite[nb_pt]].x[liaison[chalut.nb_liaison].nd[nb_pt]]-binx)*RESOLUTION_X_FEN/(baxx-binx);
		y = (baxy-panneau[liaison[chalut.nb_liaison].num_entite[nb_pt]].y[liaison[chalut.nb_liaison].nd[nb_pt]])*RESOLUTION_Y_FEN/(baxy-biny);
	         }
	      }
	   }
   	
	for (num_entite=1 ; num_entite<=chalut.nb_element ; num_entite++)	/*meme chose que pour les panneaux*/
	   {
	   for (num_pt=1 ; num_pt<=2 ; num_pt++)
	      {
	      abs_x=x_liaison - element[num_entite].x[num_pt];
	      abs_y=y_liaison - element[num_entite].y[num_pt];
	      dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));
	      
	      if (dist<=mini_dist)
	         {
	         mini_dist=dist;			/*distance mini*/
	         liaison[chalut.nb_liaison].type[nb_pt]=1;			
	         liaison[chalut.nb_liaison].num_entite[nb_pt]=num_entite;
	         liaison[chalut.nb_liaison].nd[nb_pt]=num_pt;
	         liaison[chalut.nb_liaison].type_du_noeud[nb_pt]=element[num_entite].type_du_noeud[num_pt];
		x = (element[liaison[chalut.nb_liaison].num_entite[nb_pt]].x[liaison[chalut.nb_liaison].nd[nb_pt]]-binx)*RESOLUTION_X_FEN/(baxx-binx);
		y = (baxy-element[liaison[chalut.nb_liaison].num_entite[nb_pt]].y[liaison[chalut.nb_liaison].nd[nb_pt]])*RESOLUTION_Y_FEN/(baxy-biny);
	         }
	      }
	   }
	DrawBox(x-5,y-5,10,10);
  	
	printf("liaison %4d  point de la liaison %4d  ",chalut.nb_liaison,nb_pt);
	if (liaison[chalut.nb_liaison].type[nb_pt]==0) 
		{
		printf("panneau numero %d  Point numero  %d\n",liaison[chalut.nb_liaison].num_entite[nb_pt],liaison[chalut.nb_liaison].nd[nb_pt]);
		}
	else 
		{
		printf("element numero %d  Point numero  %d\n",liaison[chalut.nb_liaison].num_entite[nb_pt],liaison[chalut.nb_liaison].nd[nb_pt]);
		}
	
	if(liaison[chalut.nb_liaison].nb_pt>1)	/*uniquement pour dessiner au fure et a mesure de la creation*/
	   {
	   lx = liaison_x(chalut.nb_liaison,nb_pt-1);
	   ly = liaison_y(chalut.nb_liaison,nb_pt-1);
	   
	   lx2 = liaison_x(chalut.nb_liaison,nb_pt);
	   ly2 = liaison_y(chalut.nb_liaison,nb_pt);
	   
	   if((lx==lx2)&&(ly==ly2))
	       {
	       printf("ANNULE point deja selectionne\n");
	       liaison[chalut.nb_liaison].nb_pt--;
	       terminer_liaison();
	       }
	   else
	       {
	       lx = ((REEL) lx - binx) / (baxx - binx);
	       lx2 = ((REEL) lx2 - binx) / (baxx - binx);
	       ly = ((REEL) ly - biny) / (baxy - biny);
	       ly2 = ((REEL) ly2 - biny) / (baxy - biny);

	       Line(lx,ly,lx2,ly2);
	       }
	    }
	   
	}
    }
	
	
	


	
void terminer_liaison()
    {
    int test_type_noeud = 0, num_point_entite,num_pt,num_entite;
    
    SetButtonUpCB(w[2],    NULL);		/*MakeDrawArea*/
    SetMenuItemChecked(w[15],0);		/*creer_liaison*/
    SetMenuItemChecked(w[4],0);			/*zoom*/
    
    if (liaison[chalut.nb_liaison].nb_pt>=2)
       {
       for (num_pt=2 ; num_pt<=liaison[chalut.nb_liaison].nb_pt ; num_pt++)
	  {
	  if (test_type_noeud==0 && liaison[chalut.nb_liaison].type_du_noeud[num_pt]!=liaison[chalut.nb_liaison].type_du_noeud[num_pt-1])
	     {
	     printf("ATTENTION TOUS LES NOEUDS CLIQUES NE SONT PAS DE MEME TYPE\n");
	     type_noeud_propose=liaison[chalut.nb_liaison].type_du_noeud[1];
	     test_type_noeud=1;
	     }
	   }
   
	if (test_type_noeud)	/*si il y a des noeuds de types differents dans la liaison que l on est en train de creer*/
	   {
	   TagList tags[] = 
  	      {
    	      {TAG_INT,	"type a affecter a tous les points : ",	&type_noeud_propose,  	TAG_INIT}, 
 	      {TAG_DONE,	NULL,           	NULL,     		TAG_NOINIT}
  	      };

	   if(GetValues_2(tags))
    		   printf("Cancelled\n");
	   else
    	      {
	      for (num_pt=1 ; num_pt<=liaison[chalut.nb_liaison].nb_pt ; num_pt++)
	         {
		 if (liaison[chalut.nb_liaison].type[num_pt]==0)
		    {
		    num_entite = liaison[chalut.nb_liaison].num_entite[num_pt];
		    num_point_entite = liaison[chalut.nb_liaison].nd[num_pt];
		    panneau[num_entite].type_du_noeud[num_point_entite] = type_noeud_propose;
		    printf("panneau %d noeud %d type %d\n",num_entite,num_point_entite,type_noeud_propose,panneau[num_entite].type_du_noeud[num_point_entite]); 
		    }
		 
		 if (liaison[chalut.nb_liaison].type[num_pt]==1)
		    {
		    num_entite = liaison[chalut.nb_liaison].num_entite[num_pt];
		    num_point_entite = liaison[chalut.nb_liaison].nd[num_pt];
		    element[num_entite].type_du_noeud[num_point_entite] = type_noeud_propose;
		    printf("element %d noeud %d type %d\n",num_entite,num_point_entite,type_noeud_propose,element[num_entite].type_du_noeud[num_point_entite]); 
		    }
		 
		 
		 }
	       }
	     }
	   }
    printf("liaison %d cree\n",chalut.nb_liaison);
    dessiner();
	 }
    
	        
void dessiner_liaison()
    {
    int j;
    Color(BLACK);
    if(chalut.nb_liaison!=0)
       {
       for(i=1;i<=chalut.nb_liaison;i++)
	  {
	  if(liaison[i].nb_pt>1)
             {
             for(j=2;j<=liaison[i].nb_pt;j++)
		{
		/*printf("liaison[%d].type[%d] %d\n",i,j,liaison[i].type[j]);*/
		   
		if (liaison[i].type[j-1] == 0)				/*panneau*/
		   {
		   xxx1 = panneau[liaison[i].num_entite[j-1]].x[liaison[i].nd[j-1]];
		   yyy1 = panneau[liaison[i].num_entite[j-1]].y[liaison[i].nd[j-1]];
		   }
		if (liaison[i].type[j-1] == 1)				/*element*/
		   {
		   xxx1 = element[liaison[i].num_entite[j-1]].x[liaison[i].nd[j-1]];
		   yyy1 = element[liaison[i].num_entite[j-1]].y[liaison[i].nd[j-1]];
		   }
		
		if (liaison[i].type[j] == 0)
		   {
		/*printf("panneau\n\n");*/
		   xxx2 = panneau[liaison[i].num_entite[j]].x[liaison[i].nd[j]];
		   yyy2 = panneau[liaison[i].num_entite[j]].y[liaison[i].nd[j]];
	   
		   }
		if (liaison[i].type[j] == 1)
		   {
		/*printf("element\n\n");*/
		   xxx2 = element[liaison[i].num_entite[j]].x[liaison[i].nd[j]];
		   yyy2 = element[liaison[i].num_entite[j]].y[liaison[i].nd[j]];
		   
		   }
		/*
		printf("xxx1_1 %f xxx2_1 %f\n",xxx1,xxx2);
		printf("yyy1_1 %f yyy2_1 %f\n",yyy1,yyy2);
		*/
		xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);
		xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
		yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
		yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);

		Line(xxx1,yyy1,xxx2,yyy2);
		}
	      }
	   }
	}
    }
    




void ordre_maillage()
    {
    int j;
    /*MyProgram *me = (MyProgram *)data;*/
    int ans;
    flag_ordre_maillage = 1;				/*vaut 0 si il n y a pas d ordre de maillage defini, et 1 si il est defini*/
    /*
    j=GetMenuItemChecked(w[16]);
    chalut.nb_ordre_objet = chalut.nb_panneau + chalut.nb_pan_hexa + chalut.nb_element + chalut.nb_coulisse;


    ordre_maill_entite = (int *) malloc((1 + chalut.nb_ordre_objet) * sizeof(int));
    if (ordre_maill_entite    == NULL)
	    {
	    printf("ordre_maill_entite  = NULL  \n" );
	    exit(0);
	    }
 
    ordre_maill_num_entite = (int *) malloc((1 + chalut.nb_ordre_objet) * sizeof(int));
    if (ordre_maill_num_entite    == NULL)
	    {
	    printf("ordre_maill_num_entite  = NULL  \n" );
	    exit(0);
	    }

    
    if (j==1)
	    {
    	    printf("menu deja active\n");
	    }
    else 
    	    {
 	    SetMenuItemChecked(w[17],1);	    
	    SetButtonUpCB(w[2],    click_me);
   	    }
   */
    }
   
    


void button_up_ordre_maillage(Widget w, int which_button, int x, int y, void *data)	/*fonction qui permet de retrouver les panneaux et ele sur lesquels on clique*/
    {											/*copie de button_up_liaison*/
    int num_entite,num_pt,mini_num_entite,mini_num_pt,entite,j;
    float abs_x,x_liaison,dist,mini_dist;
    float abs_y,y_liaison;
   
    Color(YELLOW);
    
    if (which_button == 1)		/*selection du point*/
	{
	entite=0;
	mini_dist=10000;						/*distance entre x clique et les x des points deja stockes*/
	x_liaison=binx + (float) x/ RESOLUTION_X_FEN*(baxx-binx);	/*coord x qui prend en compte le zoom. origine en bas a gauche*/
	y_liaison=baxy - (float) y/ RESOLUTION_Y_FEN*(baxy-biny);	/*idem*/
	printf("\n");
	for (num_entite=1 ; num_entite<=chalut.nb_panneau ; num_entite++)
	   {
	   for (num_pt=1 ; num_pt<=panneau[num_entite].nb_point ; num_pt++)
	      {
	      abs_x=x_liaison-panneau[num_entite].x[num_pt];
	      abs_y=y_liaison-panneau[num_entite].y[num_pt];
	      dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));			/*calcul de la distance*/
	      
	      if (dist<=mini_dist)			/*recuperation des donnees du point le plus proche sur ce panneau*/
	         {
	         mini_dist=dist;
	         entite=0;
	         mini_num_entite=num_entite;
	         }
	      }
	   }
   	
	for (num_entite=1 ; num_entite<=chalut.nb_element ; num_entite++)	/*meme chose pour les elements*/
	   {
	   for (num_pt=1 ; num_pt<=2 ; num_pt++)
	      {
	      abs_x=x_liaison-element[num_entite].x[num_pt];
	      abs_y=y_liaison-element[num_entite].y[num_pt];
	      dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));
	      
	      if (dist<=mini_dist)
	         {
	         mini_dist=dist;
	         entite=1;
	         mini_num_entite=num_entite;
	         }
	      }
	   }
	
	
			/*test pour eviter de cliquer deux fois sur la meme figure*/
	/*printf("entite %d numero d entite %d\n",entite,mini_num_entite);*/
	if (entite==0)			
	   {
	   if (panneau[mini_num_entite].flag_ordre_maillage==1) 
	   		printf ("panneau %d deja selectionne\n",mini_num_entite);
	   else
	      {
	      nb_entite++;
	      panneau[mini_num_entite].flag_ordre_maillage=1;
	      ordre_maill_entite[nb_entite]=entite;
	      ordre_maill_num_entite[nb_entite]=mini_num_entite;
	      printf("panneau %d selectionne\n",mini_num_entite);
	      }
	    }
	if (entite==1)
	   {
	   if (element[mini_num_entite].flag_ordre_maillage==1) 
	   		printf ("element %d deja selectionne\n",mini_num_entite);
	   else
	      {
	      nb_entite++;
	      element[mini_num_entite].flag_ordre_maillage=1;
	      ordre_maill_entite[nb_entite]=entite;
	      ordre_maill_num_entite[nb_entite]=mini_num_entite;
	      printf("element %d selectionne\n",mini_num_entite);
	      }
	    }
		   
	dessiner();
	
	if(nb_entite==chalut.nb_ordre_objet) 
		{
		printf("\nL ordre de maillage est defini\n");
		stop_ordre_maillage();
		}
	}
    }
	


void stop_ordre_maillage()
	{
	SetButtonUpCB(w[2],    NULL);
	SetMenuItemChecked(w[17],0);
	}

void realloc_panneau()
   {
   /**********************************************/
   panneau[chalut.nb_panneau].x = (float *) realloc(panneau[chalut.nb_panneau].x, (1+ nb_point) * sizeof(float));
   if (panneau[chalut.nb_panneau].x    == NULL)
	   {
	   printf("panneau[chalut.nb_panneau].x  2 = NULL  \n" );
	   exit(0);
	   }
   /**********************************************/
   panneau[chalut.nb_panneau].y = (float *) realloc(panneau[chalut.nb_panneau].y, (1+ nb_point) * sizeof(float));
   if (panneau[chalut.nb_panneau].y    == NULL)
	   {
	   printf("panneau[chalut.nb_panneau].y  2 = NULL  \n" );
	   exit(0);
	   }
   /**********************************************/
   panneau[chalut.nb_panneau].U = (float *) realloc(panneau[chalut.nb_panneau].U, (1+ nb_point) * sizeof(float));
   if (panneau[chalut.nb_panneau].U    == NULL)
	   {
	   printf("panneau[chalut.nb_panneau].U 2 = NULL  \n" );
	   exit(0);
	   }
   /**********************************************/
   panneau[chalut.nb_panneau].V = (float *) realloc(panneau[chalut.nb_panneau].V, (1+ nb_point) * sizeof(float));
   if (panneau[chalut.nb_panneau].V    == NULL)
	   {
	   printf("panneau[chalut.nb_panneau].V  2 = NULL  \n" );
	   exit(0);
	   }
   /**********************************************/		
   }


void affectation_carateristiques_meca_pan()
   {
   panneau[chalut.nb_panneau].raideur_traction = raideur_traction;
   panneau[chalut.nb_panneau].raideur_compression = raideur_compression;
   panneau[chalut.nb_panneau].raideur_ouverture = raideur_ouverture;		/*les parametres sont les memes pour tous les panneaux*/
   panneau[chalut.nb_panneau].longueur_repos = maille_au_repos;		/*ils peuvent etre modifies ensuite cf modifier_parametres_pan()*/
   panneau[chalut.nb_panneau].rho = mass_vol;
   panneau[chalut.nb_panneau].diam_hydro = diam_hydro;
   panneau[chalut.nb_panneau].largeurnoeud = largeur_noeud;
   panneau[chalut.nb_panneau].cdnormal = cd_normal;
   panneau[chalut.nb_panneau].ftangent = cd_tang;
   panneau[chalut.nb_panneau].pas_maillage = pas_maillage;
   panneau[chalut.nb_panneau].type_noeud = type_noeud_interieur;
   panneau[chalut.nb_panneau].type_du_noeud[nb_point] = 2;			/*affectation par defaut, possible de modifier avec la fonction choix_type_de_noeud*/
   panneau[chalut.nb_panneau].type_maillage = type_maillage;
   }
   
void affectation_carateristiques_meca_ele()
   {
   element[chalut.nb_element].raideur_traction = raideur_traction_el;
   element[chalut.nb_element].raideur_compression = raideur_compression_el;
   element[chalut.nb_element].longueur_repos = long_repos_el;
   element[chalut.nb_element].rho = mass_vol_el;
   element[chalut.nb_element].diam_hydro = diam_hydro_el;
   element[chalut.nb_element].cdnormal = coef_trainee_norm_el;
   element[chalut.nb_element].ftangent = coef_trainee_tang_el;
   element[chalut.nb_element].nb_barre = nb_barre_el;
   element[chalut.nb_element].type_du_noeud[nb_point_el] = 2;			/*affectation par defaut, possible de modifier avec la fonction choix_type_de_noeud*/
   element[chalut.nb_element].type_noeud = type_noeud_int_el;
   }

























