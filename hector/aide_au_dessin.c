#include "hector.h"

void effacer()
   {
   int i,j;
   /*
   i=GetMenuItemChecked(w[20]);
   j=GetMenuItemChecked(w[21]);
   if (i==0)
      {
      if (j==1) SetMenuItemChecked(w[21],0);
      SetMenuItemChecked(w[20],1);
      SetButtonUpCB(w[2],    button_up_effacer2);
      }
   else
      {
      SetMenuItemChecked(w[20],0);
      SetButtonUpCB(w[2],    NULL);
      } 
   */
   }

void button_up_effacer2(Widget w, int which_button, int x, int y, void *data)
   {
   int num_entite,num_pt,type_garde,num_entite_garde;
   float abs_x,x_liaison,dist,mini_dist;
   float abs_y,y_liaison;

   if (which_button == 1)		/*selection du point a recuperer*/
       {       
       type_garde=0;			/*type 0 : panneau ; type 1 : element ;  INITIALISATION (pas sur que ce soit necessaire)*/
       mini_dist=10000;
       x_liaison=binx + (float) x/ RESOLUTION_X_FEN*(baxx-binx);	/*coord x qui prend en compte le zoom. origine en bas a gauche*/
       y_liaison=baxy - (float) y/ RESOLUTION_Y_FEN*(baxy-biny);	/*idem*/
       //printf("\n");
       for (num_entite=1 ; num_entite<=chalut.nb_panneau ; num_entite++)	/*num_entite : num du panneau ou num de l element suivant le cas*/
	  {
          abs_x = 0.0;
          abs_y = 0.0;
	  for (num_pt=1 ; num_pt<=panneau[num_entite].nb_point ; num_pt++)
	     {
	     abs_x+=x_liaison - panneau[num_entite].x[num_pt];		/*abs_x : dist selon x entre le point clique et le point en cours (panneau[].x[])*/
	     abs_y+=y_liaison - panneau[num_entite].y[num_pt];
	     }
          abs_x = abs_x/panneau[num_entite].nb_point;
          abs_y = abs_y/panneau[num_entite].nb_point;
          dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));			/*calcul de la distance*/
          if (dist<=mini_dist)			/*recuperation des donnees du point le plus proche sur ce panneau*/
	     {
	     mini_dist=dist;					/*distance mini*/
	     type_garde=0;						/*indique si on a a faire a un panneau (0) ou un element (1)*/
	     num_entite_garde=num_entite;				/*numero de panneau (ou numero d element)*/
	     }
	  }

       for (num_entite=1 ; num_entite<=chalut.nb_element ; num_entite++)	/*meme chose que pour les panneaux*/
	  {
          abs_x = 0.0;
          abs_y = 0.0;
	  for (num_pt=1 ; num_pt<=2 ; num_pt++)
	     {
	     abs_x+=x_liaison - element[num_entite].x[num_pt];
	     abs_y+=y_liaison - element[num_entite].y[num_pt];
	     }
          abs_x = abs_x/2;
          abs_y = abs_y/2;
          dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));

          if (dist<=mini_dist)
             {
	     mini_dist=dist;			/*distance mini*/
	     type_garde=1;						/*indique si on a a faire a un panneau (0) ou un element (1)*/
	     num_entite_garde=num_entite;				/*numero de panneau (ou numero d element) qui va etre supprime*/
	     }
	  }

       if (type_garde==0) 
	  {
	  printf("panneau numero %d\n",num_entite_garde);
	  
	  for (num_entite=num_entite_garde ; num_entite < chalut.nb_panneau ; num_entite++)
	     {
	     panneau[num_entite].nb_point=panneau[num_entite+1].nb_point;
	     
	     for (num_pt=1 ; num_pt<=panneau[num_entite+1].nb_point ; num_pt++)	/*peut etre pas besoin de tout ce for... faire les tests*/
	        {
	  /*il faut d abord regarder si il y a une liaison accrochee a ce point*/
	  xxx1 = panneau[num_entite_garde].x[num_pt];
	  yyy1 = panneau[num_entite_garde].y[num_pt];
	  num_pt_liaison = appartient_liaison(xxx1,yyy1);
	  /*if (num_pt_liaison != 0) ajuster_liaison();		pour que la liaison suive lorsque l on efface*/
	  
	  
	  
	        panneau[num_entite].x[num_pt]=panneau[num_entite+1].x[num_pt];
	        panneau[num_entite].y[num_pt]=panneau[num_entite+1].y[num_pt];
	        }
	     
	     }
	  chalut.nb_panneau--;
	  creation_panneau_terminee=FALSE;		/*doit etre inutile maintenant*/
	  }
       		
       if (type_garde==1) 
	  {
	  printf("element numero %d\n",num_entite_garde);
	  
	  for (num_entite=num_entite_garde ; num_entite < chalut.nb_element ; num_entite++)	/*variable num_entite deja utilisee !!! confusions !!!*/
	     {
	     element[num_entite].nb_point=element[num_entite+1].nb_point;
	     for (num_pt=1 ; num_pt<=element[num_entite+1].nb_point ; num_pt++)
	        {
	        element[num_entite].x[num_pt]=element[num_entite+1].x[num_pt];
	        element[num_entite].y[num_pt]=element[num_entite+1].y[num_pt];
	        }
	     
	     }
	  chalut.nb_element--;
	  creation_element_terminee=FALSE;
	  }
       		
       }
   dessiner();
   }


void effacer_liaison()
   {
   int i,j;
   /*
   i=GetMenuItemChecked(w[21]);
   j=GetMenuItemChecked(w[20]);
   
   if (i==0)
      {
      if (j==1) SetMenuItemChecked(w[20],0);
      SetMenuItemChecked(w[21],1);
      SetButtonUpCB(w[2],    button_up_effacer_liaison);
      }
   else
      {
      SetMenuItemChecked(w[21],0);
      SetButtonUpCB(w[2],    NULL);
      } 
   */
   }

void button_up_effacer_liaison(Widget w, int which_button, int x, int y, void *data)
   {
   int num_entite,num_pt,num_entite_garde;
   float abs_x,x_liaison,dist,mini_dist;
   float abs_y,y_liaison;

   if (which_button == 1)		/*selection du point a lier*/
       {       
       mini_dist=10000;
       x_liaison=binx + (float) x/ RESOLUTION_X_FEN*(baxx-binx);	/*coord x qui prend en compte le zoom. origine en bas a gauche*/
       y_liaison=baxy - (float) y/ RESOLUTION_Y_FEN*(baxy-biny);	/*idem*/
       printf("\n");
       
       for (num_entite=1 ; num_entite<=chalut.nb_liaison ; num_entite++)	/*meme chose que pour les panneaux*/
	 {
	 for (num_pt=1 ; num_pt<=liaison[num_entite].nb_pt ; num_pt++)
	    {
	    printf("type %d\n",liaison[num_entite].type[num_pt]);
	    if (liaison[num_entite].type[num_pt] == 0)
	       {
	       abs_x = panneau[liaison[num_entite].num_entite[num_pt]].x[liaison[num_entite].nd[num_pt]];
	       abs_y = panneau[liaison[num_entite].num_entite[num_pt]].y[liaison[num_entite].nd[num_pt]];
	       abs_x=x_liaison - abs_x;
	       abs_y=y_liaison - abs_y;
	       printf(" p abs_x %f abs_y %f\n",abs_x,abs_y);
	       
	       /*
	       abs_x=x_liaison - liaison[num_entite].x[num_pt];
	       abs_y=y_liaison - liaison[num_entite].y[num_pt];
	       
	       printf(" p abs_x %f abs_y %f\n",abs_x,abs_y);
	       */
	       }
	    if (liaison[num_entite].type[num_pt] == 1)
	       {
	       abs_x = element[liaison[num_entite].num_entite[num_pt]].x[liaison[num_entite].nd[num_pt]];
	       abs_y = element[liaison[num_entite].num_entite[num_pt]].y[liaison[num_entite].nd[num_pt]];
	       abs_x=x_liaison - abs_x;
	       abs_y=y_liaison - abs_y;
	       
	       printf("abs_x %f abs_y %f\n",abs_x,abs_y);
	       /*
	       abs_x=x_liaison - liaison[num_entite].x[num_pt];
	       abs_y=y_liaison - liaison[num_entite].y[num_pt];
	       
	       printf("abs_x %f abs_y %f\n",abs_y);
	       */
	       }
	    
	    dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));

	    if (dist<=mini_dist)
	       {
	       mini_dist=dist;			/*distance mini*/
	       num_entite_garde=num_entite;				/*numero de panneau (ou numero d element)*/
	       }
	    }
	 }
       		
	 printf("liaison numero %d\n",num_entite_garde);

	 for (num_entite=num_entite_garde ; num_entite < chalut.nb_liaison ; num_entite++)
	    {
	    liaison[num_entite].nb_pt=liaison[num_entite+1].nb_pt;
	    for (num_pt=1 ; num_pt<=liaison[num_entite+1].nb_pt ; num_pt++)
	       {
	       /*
	       liaison[num_entite].x[num_pt]=liaison[num_entite+1].x[num_pt];
	       liaison[num_entite].y[num_pt]=liaison[num_entite+1].y[num_pt];
	       */
	       liaison[num_entite].type[num_pt]=liaison[num_entite+1].type[num_pt];
	       liaison[num_entite].nd[num_pt]=liaison[num_entite+1].nd[num_pt];
	       liaison[num_entite].num_entite[num_pt]=liaison[num_entite+1].num_entite[num_pt];
	       }
	    }
	 chalut.nb_liaison--;
       }
       		
   dessiner();
   }







