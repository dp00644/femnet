#include "hector.h" 
 

void creer_fichier_don(Widget w, void *data)
   {
   /*MyProgram *me = (MyProgram *)data;*/
   int pan,ele,pt,numero_liaison,n_point,i,modulo,no_du_type;
   char tonom[128],*nom_fichier,*axe_perpendiculaire;
   
   chalut.nb_ordre_objet = chalut.nb_panneau + chalut.nb_element + chalut.nb_coulisse + chalut.nb_pan_hexa;
   if (chalut.nb_ordre_objet == 0)
   	{
   	printf("nothing has been created, impossible to create .don file\n");
   	exit(0);
   	}
   
   nom_fichier = GetString("\nfile name\n", "new_file");
   ///axe_perpendiculaire = GetString("\nnormal axe\n", "z");
   if (nom_fichier) strcpy(tonom,nom_fichier);
   else 
   	{
   	printf("cancel the file creation\n");
   	exit(0);
   	}
   strcat(tonom,".don");
   f1 = fopen(tonom,"w");

   ///if(strcmp(axe_perpendiculaire,"x")==0) coordonnee_neutre=1;
   ///if(strcmp(axe_perpendiculaire,"y")==0) coordonnee_neutre=2;
   ///if(strcmp(axe_perpendiculaire,"z")==0) coordonnee_neutre=3;
   coordonnee_neutre=3;

   ///fprintf(f1,"work in the plane normal to %s which means coordinates %s constant : %d\n",axe_perpendiculaire,axe_perpendiculaire,coordonnee_neutre);
   ///fprintf(f1,"panels number: %d\n\n",chalut.nb_panneau);
      
   for(pan= 1;pan<=chalut.nb_panneau;pan++)				/*PANNEAUX*/
      {
      ///fprintf(f1,"panel: %d\n\n",pan);
      ///fprintf(f1,"number of panel corners: %d\n",panneau[pan].nb_point);
      ///fprintf(f1,"panel corners no x y z U V no_type no_type_following:\n");
      
      if(coordonnee_neutre==1)	/*if avant for pour eviter au prog de boucler trop souvent*/
         {        
	 for(pt=1; pt<=panneau[pan].nb_point;pt++)
            {
	    ///fprintf(f1,"%d	",pt);
	    ///fprintf(f1,"0	");
	    ///fprintf(f1,"%5.2f	",panneau[pan].x[pt]);
	    ///fprintf(f1,"%5.2f	",panneau[pan].y[pt]);
	    ///fprintf(f1,"%5.2f	",panneau[pan].U[pt]);
	    ///fprintf(f1,"%5.2f	",panneau[pan].V[pt]);
	    ///fprintf(f1,"%d	",panneau[pan].type_du_noeud[pt]);
	    ///fprintf(f1,"2	\n");
	    }
	 }
      
      if(coordonnee_neutre==2)		/*les y sont nuls*/
         {
	 for(pt=1;pt<=panneau[pan].nb_point;pt++)
            {
	    ///fprintf(f1,"%d	",pt);
	    ///fprintf(f1,"%5.2f	",panneau[pan].x[pt]);
	    ///fprintf(f1,"0	");
	    ///fprintf(f1,"%5.2f	",panneau[pan].y[pt]);
	    ///fprintf(f1,"%5.2f	",panneau[pan].U[pt]);
	    ///fprintf(f1,"%5.2f	",panneau[pan].V[pt]);
	    ///fprintf(f1,"%d	",panneau[pan].type_du_noeud[pt]);
	    ///fprintf(f1,"2	\n");
	    }
      	 }

      if(coordonnee_neutre==3)		/*les z sont nuls*/
         {
	 for(pt=1;pt<=panneau[pan].nb_point;pt++)
            {
	    ///fprintf(f1,"%d	",pt);
	    ///fprintf(f1,"%5.2f	",panneau[pan].x[pt]);
	    ///fprintf(f1,"%5.2f	",panneau[pan].y[pt]);
	    ///fprintf(f1,"0	");
	    ///fprintf(f1,"%5.2f	",panneau[pan].U[pt]);
	    ///fprintf(f1,"%5.2f	",panneau[pan].V[pt]);
	    ///fprintf(f1,"%d	",panneau[pan].type_du_noeud[pt]);
	    ///fprintf(f1,"2	\n");
	    }
         }
      ///fprintf(f1,"\n\n");
      ///fprintf(f1,"traction stiffness (N):		%e\n",panneau[pan].raideur_traction);	/*remplissage des caracteristiques meca*/
      ///fprintf(f1,"compression stiffness (N):		%e\n",panneau[pan].raideur_compression);
      ///fprintf(f1,"opening stiffness (N.m/rad):		%f\n",panneau[pan].raideur_ouverture);
      ///fprintf(f1,"unstretches mesh side (m):		%f\n",panneau[pan].lcm);
      ///fprintf(f1,"density (kg/m3):			%f\n",panneau[pan].rho);
      ///fprintf(f1,"hydrodynamic diameter (m):		%f\n",panneau[pan].diam_hydro);
      ///fprintf(f1,"knot size:				%f\n",panneau[pan].largeurnoeud);
      ///fprintf(f1,"Cd normal:				%f\n",panneau[pan].cdnormal);
      ///fprintf(f1,"Cd tangential:			%f\n",panneau[pan].ftangent);
      ///fprintf(f1,"meshing step (m):			%f\n",panneau[pan].pas_maillage);
      ///fprintf(f1,"type of inner nodes:			%d\n",panneau[pan].type_noeud);
      ///fprintf(f1,"meshing type:				%d\n",panneau[pan].type_maillage);
      ///fprintf(f1,"\n\n");
     	
      }
    
   
   
   
   /*panneaux hexa**************************************************************/
   
   ///fprintf(f1,"number of hexagional panels: 0\n\n\n");					/*panneaux hexa pas traites pour l instant*/
   
   
   
   /*Elements**************************************************************/
   
   ///fprintf(f1,"elements number: %d\n\n",chalut.nb_element);	
   
   for(ele= 1;ele<=chalut.nb_element;ele++)
      {
      ///fprintf(f1,"Element : %d\n\n",ele);
      ///fprintf(f1,"extremities description no x y z no_type:\n");
      
      if(coordonnee_neutre==1)	/*if avant for pour eviter au prog de boucler trop souvent*/
         {        
	 for(pt=1; pt<=element[ele].nb_point;pt++)
            {
	    ///fprintf(f1,"%d	",pt);
	    ///fprintf(f1,"0	");
	    ///fprintf(f1,"%5.2f	",element[ele].x[pt]);
	    ///fprintf(f1,"%5.2f	",element[ele].y[pt]);
	    ///fprintf(f1,"%d	\n",element[ele].type_du_noeud[pt]);
	    }
	 }
      
      if(coordonnee_neutre==2)		/*les y sont nuls*/
         {
	 for(pt=1;pt<=element[ele].nb_point;pt++)
            {
	    ///fprintf(f1,"%d	",pt);
	    ///fprintf(f1,"%5.2f	",element[ele].x[pt]);
	    ///fprintf(f1,"0	");
	    ///fprintf(f1,"%5.2f	",element[ele].y[pt]);
	    ///fprintf(f1,"%d	\n",element[ele].type_du_noeud[pt]);
	    }
      	 }

      if(coordonnee_neutre==3)		/*les z sont nuls*/
         {
	 for(pt=1;pt<=element[ele].nb_point;pt++)
            {
	    ///fprintf(f1,"%d	",pt);
	    ///fprintf(f1,"%5.2f	",element[ele].x[pt]);
	    ///fprintf(f1,"%5.2f	",element[ele].y[pt]);
	    ///fprintf(f1,"0	");
	    ///fprintf(f1,"%d	\n",element[ele].type_du_noeud[pt]);
	    }
         }
      
      ///fprintf(f1,"\n\n\n");
      ///fprintf(f1,"traction stiffness (N):				%e\n",element[ele].raideur_traction);	/*caraterisiques meca*/
      ///fprintf(f1,"compression stiffness (N):			%e\n",element[ele].raideur_compression);
      ///fprintf(f1,"unstretched length (m):				%f\n",element[ele].longueur_repos);
      ///fprintf(f1,"density (kg/m3):			%f\n",element[ele].rho);
      ///fprintf(f1,"hydrodynamic diameter (m):			%f\n",element[ele].diam_hydro);
      ///fprintf(f1,"normal drag coefficient:			%f\n",element[ele].cdnormal);
      ///fprintf(f1,"tangential drag coefficient:		%f\n",element[ele].ftangent);
      ///fprintf(f1,"bar number:					%d\n",element[ele].nb_barre);
      ///fprintf(f1,"inner nodes type:			%d\n",element[ele].type_noeud);
      ///fprintf(f1,"\n");
      
      }
    ///fprintf(f1,"sliding ropes number: 0\n\n");    				/*coulisses non traitees*/
    
    
    
    
    
   /*Liaisons************************************************/ 
    
    fprintf(f1,"links number: %d\n",chalut.nb_liaison);    
    for(numero_liaison=1;numero_liaison<=chalut.nb_liaison;numero_liaison++)		/*LAISONS*/
      {
      fprintf(f1,"nb_pt :%d	",liaison[numero_liaison].nb_pt);
      for (n_point=1;n_point<=liaison[numero_liaison].nb_pt;n_point++)
         {
         if(liaison[numero_liaison].type[n_point]==0)
              fprintf(f1,"pa: %d	",liaison[numero_liaison].num_entite[n_point]);
         else fprintf(f1,"el: %d	",liaison[numero_liaison].num_entite[n_point]);
         fprintf(f1,"nd: %d	",liaison[numero_liaison].nd[n_point]);
         }
      fprintf(f1,"\n");
      }
    fprintf(f1,"\n");
    
    
    
    /*Ordre de maillage************************************/
    
    ///fprintf(f1,"meshing order:\n"); 
    
    if (flag_ordre_maillage == 1)		/*cas ou un ordre de maillage a ete choisi*/
       {
       for (i=1;i<=chalut.nb_ordre_objet;i++)	/*boucle sur la totalité des objets créés (pan, pan hexa, ele etc)*/
	 {
	 printf("i %d\n",i);
	 printf("ordre_maill_entite[i] %d\n",ordre_maill_entite[i]);
	 if(ordre_maill_entite[i]==0)
              ///fprintf(f1,"pa: %d ",ordre_maill_num_entite[i]);
	 if(ordre_maill_entite[i]==1)
              ///fprintf(f1,"el: %d ",ordre_maill_num_entite[i]);
	 modulo=i%12;			/*pour aller a la ligne toutes les 12 fois*/
	 if(modulo==0)
	 	{
	 	///fprintf(f1,"\n");
	 	}
	 }
       }
    else 
    	{
    	/*printf ("il n y a pas d ordre de maillage dans le fichier cree\n");*/
    
   	   for(pan= 1;pan<=chalut.nb_panneau;pan++)				/*PANNEAUX*/
	      {
              ///fprintf(f1,"pa: %d ",pan);
	      }
	   for(ele= 1;ele<=chalut.nb_element;ele++)
	      {
              ///fprintf(f1,"el: %d ",ele);
	      }
	}
    ///fprintf(f1,"\n\n\n\n\n\n");
    
    
    
    
    
    /*Types de noeuds****************************************/
    
    ///fprintf(f1,"number of node types: %d\n",chalut.nb_type_noeud);
    
    printf("Structure.nb_type_node %d\n",chalut.nb_type_noeud);
    for(no_du_type=1;no_du_type<=chalut.nb_type_noeud;no_du_type++)
       {
       ///fprintf(f1,"type no:			%d\n",no_du_type);
       ///fprintf(f1,"mass X,Y,Z (kg):		%5.3f	%5.3f	%5.3f\n",TypeNoeud[no_du_type].mx,TypeNoeud[no_du_type].my,TypeNoeud[no_du_type].mz);
       ///fprintf(f1,"added mass X,Y,Z (kg):	%5.3f	%5.3f	%5.3f\n",TypeNoeud[no_du_type].majx,TypeNoeud[no_du_type].majy,TypeNoeud[no_du_type].majz);
       ///fprintf(f1,"length X,Y,Z (m):		%5.3f	%5.3f	%5.3f \n",TypeNoeud[no_du_type].lonx,TypeNoeud[no_du_type].lony,TypeNoeud[no_du_type].lonz);
       ///fprintf(f1,"drag coefficient X,Y,Z :		%5.3f	%5.3f	%5.3f \n",TypeNoeud[no_du_type].cdx,TypeNoeud[no_du_type].cdy,TypeNoeud[no_du_type].cdz);
       ///fprintf(f1,"external force X,Y,Z (N):	%5.3f	%5.3f	%5.3f\n",TypeNoeud[no_du_type].fextx,TypeNoeud[no_du_type].fexty,TypeNoeud[no_du_type].fextz);
       ///fprintf(f1,"displacement X,Y,Z:		%d	%d	%d\n",TypeNoeud[no_du_type].fixx,TypeNoeud[no_du_type].fixy,TypeNoeud[no_du_type].fixz);
       ///fprintf(f1,"limit X,Y,Z (m):		%5.3f	%5.3f	%5.3f\n",TypeNoeud[no_du_type].limx,TypeNoeud[no_du_type].limy,TypeNoeud[no_du_type].limz);
       ///fprintf(f1,"limit direction X,Y,Z:	%d	%d	%d\n",TypeNoeud[no_du_type].senx,TypeNoeud[no_du_type].seny,TypeNoeud[no_du_type].senz);
       ///fprintf(f1,"symmetry X,Y,Z:		%d	%d	%d\n",TypeNoeud[no_du_type].symx,TypeNoeud[no_du_type].symy,TypeNoeud[no_du_type].symz);
       ///fprintf(f1,"\n");
       }
	
	
	
	/*ENVIRONNEMENT NUMERIQUE*****************************************************/
	
    ///fprintf(f1,"\n\n\n\n");
    ///fprintf(f1,"numerical environnement\n");
    ///fprintf(f1,"divisor:\n");
    ///fprintf(f1,"%f\n",Numerique.DIVISEUR);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"convergence threshold (N):\n");
    ///fprintf(f1,"%f\n",Numerique.Seuilconvergence);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"displacement limit for each iteration (m):\n");
    ///fprintf(f1,"%f\n",Numerique.Deplacement);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"maximal iteration number:\n");
    ///fprintf(f1,"%d\n",Numerique.Nbmaxiterations);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"dynamic: time step (s):\n");
    ///fprintf(f1,"%f\n",Numerique.Pascalcul);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"dynamic: recording step (s):\n");
    ///fprintf(f1,"%f\n",Numerique.Passtockage);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"dynamic: recording beginning (s):\n");
    ///fprintf(f1,"%f\n",Numerique.Debutstockage);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"dynamic: calculation and recording end (s):\n");
    ///fprintf(f1,"%f\n",Numerique.Finstockage);
    ///fprintf(f1,"\n");
    
    
	/*ENVIRONNEMENT METEOROLOGIQUE ET OCEANIQUE***********************************/
    
    ///fprintf(f1,"oceanic environnement\n");
    
    ///fprintf(f1,"current\n");
    ///fprintf(f1,"direction (deg):\n");
    ///fprintf(f1,"%f\n",Courant.direction);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"speed (m/s):\n");
    ///fprintf(f1,"%f\n",Courant.vitesse);
    ///fprintf(f1,"\n");
    
    ///fprintf(f1,"wave\n");
    ///fprintf(f1,"period (s):\n");
    ///fprintf(f1,"%f\n",Houle.periode);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"wave height (m):\n");
    ///fprintf(f1,"%f\n",Houle.hauteur);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"direction relatively to X (deg):\n");
    ///fprintf(f1,"%f\n",Houle.direction);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"depth (m):\n");
    ///fprintf(f1,"%f\n",Houle.Depth1);
    ///fprintf(f1,"\n");
    
    ///fprintf(f1,"catch description\n");
    ///fprintf(f1,"volume (m3):\n");
    ///fprintf(f1,"%f\n",Prise.volume);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"volume accuracy (m3):\n");
    ///fprintf(f1,"%f\n",Prise.seuil);
    ///fprintf(f1,"\n");
    ///fprintf(f1,"drag coeffivient on catch:\n");
    ///fprintf(f1,"%f\n",Prise.cd);
    ///fprintf(f1,"\n");
    
    ///fprintf(f1,"sea bottom  \n");
    ///fprintf(f1,"wearing coefficient on bottom:\n");
    ///fprintf(f1,"%f\n",Fond.coef_frottement);
    ///fprintf(f1,"bottom stiffness (N/m):\n");
    ///fprintf(f1,"%f\n",Fond.raideur);
    ///fprintf(f1,"\n");
    
    ///fprintf(f1,"text output\n");
    ///fprintf(f1,"distance numer 			:			1\n");
    ///fprintf(f1,"distance numero		: 1\n");
    ///fprintf(f1,"comment		: #Comment#\n");
    ///fprintf(f1,"no global node 1	: 1\n");
    ///fprintf(f1,"no global node 2	: 1\n");
    ///fprintf(f1,"decimal number	: 0\n");
    ///fprintf(f1,"force number 			:			0\n");
    ///fprintf(f1,"tension number			:			0\n");
    ///fprintf(f1,"sliding ropes tension number		:	0\n");
    ///fprintf(f1,"position number			:			0\n");
    ///fprintf(f1,"structure force display	:	0\n");
    ///fprintf(f1,"catch diameter display 	:	0\n");
    ///fprintf(f1,"catch thickness display 	:	0\n");
    ///fprintf(f1,"catch volume display 	:	0\n");
    ///fprintf(f1,"filtrated surface display 	:	0\n");
    ///fprintf(f1,"speed display 		:		1\n");
    ///fprintf(f1,"\n");
				 /*fin de la creation du fichier .don*/    
    }
    
    
    
    
void init_don()
   {
  
  raideur_traction=7000.0;	/*init de creer_fichier_don*/
  raideur_compression=0.01;
  raideur_ouverture=0.0;
  maille_au_repos=0.08;
  mass_vol=1025;
  diam_hydro=0.0015;
  largeur_noeud=0.003;
  cd_normal=1.2;
  cd_tang=0.08;
  pas_maillage=1;
  type_noeud_interieur=2;
  type_maillage=2;

   }

void init_don_el()	/*initialisation des parametres des elements*/
   {
   raideur_traction_el=2e+07;
   raideur_compression_el=1.0;
   long_repos_el=3.8;
   mass_vol_el=4800.0;
   diam_hydro_el=0.014;
   coef_trainee_norm_el=1.8;
   coef_trainee_tang_el=0.08;
   nb_barre_el=1;
   type_noeud_int_el=2;	
   }

void modifier_parametres_pan()
   {
   if (chalut.nb_panneau>=1)
      {
      TagList tags[] = 
  	      {
    	      {TAG_INT,	"panel to modify: ",	&dernier_pan_modifie,  	TAG_INIT}, 
    	      {TAG_FLOAT,	"traction stiffness (N): ",	&raideur_traction,  	TAG_INIT}, 
     	      {TAG_FLOAT,	"compression stiffness (N): ",	&raideur_compression,  	TAG_INIT}, 
      	      {TAG_FLOAT,	"opening stiffness (N.m/rad): ",	&raideur_ouverture,  	TAG_INIT}, 
     	      {TAG_FLOAT,	"unstretched mesh side (m): ",	&lcm,  	TAG_INIT}, 
      	      {TAG_FLOAT,	"density (kg/m3): ",	&mass_vol,  	TAG_INIT}, 
      	      {TAG_FLOAT,	"hydrodynamic diameter (m): ",	&diam_hydro,  	TAG_INIT}, 
    	      {TAG_FLOAT,	"knot size: ",	&largeur_noeud,  	TAG_INIT}, 
      	      {TAG_FLOAT,	"Cd normal: ",	&cd_normal,  	TAG_INIT}, 
    	      {TAG_FLOAT,	"Cd tangential : ",	&cd_tang,  	TAG_INIT}, 
      	      {TAG_INT,	"meshing step (m): ",	&pas_maillage,  	TAG_INIT}, 
   	      {TAG_INT,	"type of inner nodes: ",	&type_noeud_interieur,  	TAG_INIT}, 
     	      {TAG_INT,	"meshing type: ",	&type_maillage,  	TAG_INIT}, 
 	      {TAG_DONE,	NULL,           	NULL,     		TAG_NOINIT}
  	      };



      if(GetValues_2(tags))  printf("values of modified panels\n");
      else
    	      {
    	      panneau[dernier_pan_modifie].raideur_traction = raideur_traction;
    	      panneau[dernier_pan_modifie].raideur_compression = raideur_compression;
    	      panneau[dernier_pan_modifie].raideur_ouverture = raideur_ouverture;
    	      panneau[dernier_pan_modifie].lcm = lcm;
    	      panneau[dernier_pan_modifie].rho = mass_vol;
    	      panneau[dernier_pan_modifie].diam_hydro = diam_hydro;
    	      panneau[dernier_pan_modifie].largeurnoeud = largeur_noeud;
    	      panneau[dernier_pan_modifie].cdnormal = cd_normal;
    	      panneau[dernier_pan_modifie].ftangent = cd_tang;
    	      panneau[dernier_pan_modifie].pas_maillage = pas_maillage;
    	      panneau[dernier_pan_modifie].type_noeud = type_noeud_interieur;
    	      panneau[dernier_pan_modifie].type_maillage = type_maillage;

	      printf("panels values %d modified\n",dernier_pan_modifie);
    	      }
      }
   else printf("no panel to modify\n");
   }
   
void modifier_parametres_ele()
   {
   if (chalut.nb_element>=1)
      {
      TagList tags[] = 
  	      {
    	      {TAG_INT,	"elementy to modify: ",	&dernier_ele_modifie,  	TAG_INIT}, 
    	      {TAG_FLOAT,	"traction stiffness (N): ",	&raideur_traction_el,  	TAG_INIT}, 
     	      {TAG_FLOAT,	"compression stiffness (N): ",	&raideur_compression_el,  	TAG_INIT}, 
      	      {TAG_FLOAT,	"unstretched length (m): ",	&long_repos_el,  	TAG_INIT}, 
     	      {TAG_FLOAT,	"density (kg/m3): ",	&mass_vol_el,  	TAG_INIT}, 
      	      {TAG_FLOAT,	"hydrodynamic diameter (m): ",	&diam_hydro_el,  	TAG_INIT}, 
      	      {TAG_FLOAT,	"normal drag coefficient: ",	&coef_trainee_norm_el,  	TAG_INIT}, 
    	      {TAG_FLOAT,	"tangential drag coefficient: ",	&coef_trainee_tang_el,  	TAG_INIT}, 
      	      {TAG_INT,	"nb bar: ",	&nb_barre_el,  	TAG_INIT}, 
   	      {TAG_INT,	"type of inner nodes: ",	&type_noeud_int_el,  	TAG_INIT}, 
 	      {TAG_DONE,	NULL,           	NULL,     		TAG_NOINIT}
  	      };



      if(GetValues_2(tags))  printf("values element non modified\n");
      else
    	      {

    	      element[dernier_ele_modifie].raideur_traction = raideur_traction_el;
    	      element[dernier_ele_modifie].raideur_compression = raideur_compression_el;
    	      element[dernier_ele_modifie].longueur_repos = long_repos_el;
      	   printf("element[dernier_ele_modifie].longueur_repos %f\n",element[dernier_ele_modifie].longueur_repos);
    	      element[dernier_ele_modifie].rho = mass_vol_el;
    	      element[dernier_ele_modifie].diam_hydro = diam_hydro_el;
    	      element[dernier_ele_modifie].cdnormal = coef_trainee_norm_el;
    	      element[dernier_ele_modifie].ftangent = coef_trainee_tang_el;
    	      element[dernier_ele_modifie].nb_barre = nb_barre_el;
    	      element[dernier_ele_modifie].type_noeud = type_noeud_int_el;

    	      printf("values element %d modified\n",dernier_ele_modifie);
    	      }

      }
   else printf("no element to modify\n");
   }
   

















