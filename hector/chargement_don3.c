#include "hector.h"

void charger()
	{
	/*chargement du fichier *.don, le nom generique est stocke dans fname1*/
	
	char c,t1[256],tonom[200],temp_char,*buffer;
	char chaine[] = "                                               ";
	char chainebis[] = "                                               ";
	char comment[] = "                                               ";
	int i,j,zi,zj,zk,pa,no,temp_int,temp2_int,temp3_int,type_maillage,NOMBRE_ORDRE_OBJET,symm,tmp_i1,type_ref,noeud_ref;
	float inutile_float;				/*trouver une meilleure solution pour ne pas prendre en compte z*/
	FILE *fic;
	maillage_losange_fond_ecran1();		/*creation de l arriere plan (filet a decouper)*/
	
	strcpy(newfic , (char *) GetFile("../data_2001"));
	if(newfic[0])
		{
		printf("Selected file:\n %s\n" , newfic);
		Flag_Fichier_charge = 1;
		}
	else
		{
		printf("File not found\n");
		return;
		}
        strcpy(fname1,newfic);
        j=strlen(fname1);
        while ((fname1[j])!='.' && j>0) j--;
        if (fname1[j]=='.') fname1[j]=0;

	printf("fname1 = %s \n",fname1);
	
	/* Lecture du fichier selectionne */	
	strcpy(newfic,fname1);
	strcat(newfic,".don");
	fic = fopen(newfic,"r");

	/*orientation du dessin****************************************/
    	do  c=fgetc(fic); while (c !=':'); 
    	fscanf(fic,"%d\n",&coordonnee_neutre);		/*ce qui va etre remis par defaut lors de la nouvelle creation d un fichier .don*/
	
	if ( (coordonnee_neutre != 1) && (coordonnee_neutre != 2) && (coordonnee_neutre != 3))
		{
		printf(" neutral coordinate %4d must be 1 2 or 3  \n",coordonnee_neutre );
		exit(0);
		}
		
	
	/*panneaux*****************************************************/
	do  c=fgetc(fic); while (c !=':'); 
    	fscanf(fic,"%d\n",&chalut.nb_panneau);
    	
  	if ( chalut.nb_panneau >= NBMAXPANNEAU)
		{
		printf(" NBMAXPANNEAU too small %4d < %4d \n",NBMAXPANNEAU,chalut.nb_panneau );
		exit(0);
		}
    	printf("number of diamond panels  = %8d\n", chalut.nb_panneau);
  	
 	for (pa=1;pa<=chalut.nb_panneau;pa++)
		{
		
		panneau[pa].contour_ferme = TRUE;
	    	
	    	do  c=fgetc(fic); while (c !=':'); 
	    	do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%d\n",&panneau[pa].nb_point);
		/**********************************************/
  		panneau[pa].x = (float *) malloc((1 + panneau[pa].nb_point) * sizeof(float));
		if (panneau[pa].x    == NULL)
			{
			printf(" panneau[pa].x 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		panneau[pa].y = (float *) malloc((1 + panneau[pa].nb_point) * sizeof(float));
		if (panneau[pa].y    == NULL)
			{
			printf(" panneau[pa].y 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		panneau[pa].U = (float *) malloc((1 + panneau[pa].nb_point) * sizeof(float));
		if (panneau[pa].U    == NULL)
			{
			printf(" panneau[pa].U 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		panneau[pa].V = (float *) malloc((1 + panneau[pa].nb_point) * sizeof(float));
		if (panneau[pa].V    == NULL)
			{
			printf(" panneau[pa].V 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
 	  	panneau[pa].type_du_noeud = (int *) malloc((1 + panneau[pa].nb_point) * sizeof(int));
		if (panneau[pa].type_du_noeud    == NULL)
			{
			printf(" panneau[pa].type_du_noeud 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		printf("panel %d nb of nodes = %8d\n",pa, panneau[pa].nb_point);
 		fgets(t1,256,fic);			/*?????*/
		
		for (no=1;no<=panneau[pa].nb_point;no++)
			{
			fscanf(fic,"%d",&temp_int);	/*le numero du noeud n est pas stoque dans hector*/
			printf ("neutral coordinate %d\n",coordonnee_neutre);
			if (coordonnee_neutre == 1)
				{
				fscanf(fic,"%f %f%f",&inutile_float,
						     &panneau[pa].x[no],
						     &panneau[pa].y[no]);
				}
			if (coordonnee_neutre == 2)
				{
				fscanf(fic,"%f %f%f",&panneau[pa].y[no],
						     &inutile_float,
						     &panneau[pa].x[no]);
				printf("x %5.3f y %5.3f\n",panneau[pa].x[no],panneau[pa].y[no]);
				}
			if (coordonnee_neutre == 3)
				{
				fscanf(fic,"%f %f%f",&panneau[pa].x[no],
						     &panneau[pa].y[no],
						     &inutile_float);
				printf("x %5.3f y %5.3f\n",panneau[pa].x[no],panneau[pa].y[no]);
				}
			fscanf(fic,"%f %f",&panneau[pa].U[no],
					   &panneau[pa].V[no]);
			
 			fscanf(fic,"%d",&panneau[pa].type_du_noeud[no]);
 			fscanf(fic,"%d",&temp_int);
 			}
		
		//fgets(t1,256,fic); 
		//fgets(t1,256,fic); 
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].raideur_traction);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].raideur_compression);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].raideur_ouverture);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].lcm);
   		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].rho);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].diam_hydro);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].largeurnoeud);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].cdnormal);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].ftangent);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&panneau[pa].pas_maillage);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%d\n",&panneau[pa].type_noeud);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%d\n",&panneau[pa].type_maillage);
		printf("raideur_traction 	: %9.2f\n",panneau[pa].raideur_traction );
		printf("cdnormal 		: %9.2f\n",panneau[pa].cdnormal );
		printf("ftangent 		: %9.2f\n",panneau[pa].ftangent );
		printf("type_noeud 		: %9d\n",panneau[pa].type_noeud );
		printf("pas_maillage 		: %9.2f\n",panneau[pa].pas_maillage );
		}


	/*panneaux hexagonaux*******************************************/
    	do  c=fgetc(fic); while (c !=':'); 
    	fscanf(fic,"%d\n",&chalut.nb_pan_hexa);
	if ( chalut.nb_pan_hexa >= NBMAXPANHEXA)
		{
		printf(" NBMAXPANHEXA trop petit %4d < %4d \n",NBMAXPANHEXA,chalut.nb_pan_hexa );
		exit(0);
		}
    	printf("chalut.nb_pan_hexa  = %8d\n", chalut.nb_pan_hexa);
	for (pa=1;pa<=chalut.nb_pan_hexa;pa++)
		{
	    	do  c=fgetc(fic); while (c !=':'); 
	    	do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%d\n",&pan_hexa[pa].nb_noeud_contour);
		/**********************************************/
  		pan_hexa[pa].noeud_contour = (int *) malloc((1 + pan_hexa[pa].nb_noeud_contour) * sizeof(int));
		if (pan_hexa[pa].noeud_contour    == NULL)
			{
			printf(" pan_hexa[pa].noeud_contour 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		pan_hexa[pa].suivant_contour = (int *) malloc((1 + pan_hexa[pa].nb_noeud_contour) * sizeof(int));
		if (pan_hexa[pa].suivant_contour    == NULL)
			{
			printf(" pan_hexa[pa].suivant_contour 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		pan_hexa[pa].type_suivant_contour = (int *) malloc((1 + pan_hexa[pa].nb_noeud_contour) * sizeof(int));
		if (pan_hexa[pa].type_suivant_contour    == NULL)
			{
			printf(" pan_hexa[pa].type_suivant_contour 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		printf("nb_noeud_contour  = %8d\n", pan_hexa[pa].nb_noeud_contour);
		fgets(t1,256,fic);
		for (no=1;no<=pan_hexa[pa].nb_noeud_contour;no++)
			{
			chalut.nb_total++;
			if (chalut.nb_total >= NBMAXNOEUD)
				{
				printf(" NBMAXNOEUD trop petit %4d < %4d \n",NBMAXNOEUD,chalut.nb_total );
				exit(0);
				}
			fscanf(fic,"%d",&pan_hexa[pa].noeud_contour[no]);
			pan_hexa[pa].noeud_contour[no] = chalut.nb_total;
			pan_hexa[pa].suivant_contour[no] = 0;
			if (chalut.orientation == 1)
				{
				fscanf(fic,"%f %f%f",&noeud[pan_hexa[pa].noeud_contour[no]].z,
						     &noeud[pan_hexa[pa].noeud_contour[no]].x,
						     &noeud[pan_hexa[pa].noeud_contour[no]].y);
				}
			if (chalut.orientation == 2)
				{
				fscanf(fic,"%f %f%f",&noeud[pan_hexa[pa].noeud_contour[no]].y,
						     &noeud[pan_hexa[pa].noeud_contour[no]].z,
						     &noeud[pan_hexa[pa].noeud_contour[no]].x);
				}
			if (chalut.orientation == 3)
				{
				fscanf(fic,"%f %f%f",&noeud[pan_hexa[pa].noeud_contour[no]].x,
						     &noeud[pan_hexa[pa].noeud_contour[no]].y,
						     &noeud[pan_hexa[pa].noeud_contour[no]].z);
				}
			fscanf(fic,"%f %f",&noeud[pan_hexa[pa].noeud_contour[no]].U,
					   &noeud[pan_hexa[pa].noeud_contour[no]].V);
			noeud[pan_hexa[pa].noeud_contour[no]].u =  noeud[pan_hexa[pa].noeud_contour[no]].U	+
			noeud[pan_hexa[pa].noeud_contour[no]].V; 
			noeud[pan_hexa[pa].noeud_contour[no]].v = -noeud[pan_hexa[pa].noeud_contour[no]].U	+
			noeud[pan_hexa[pa].noeud_contour[no]].V; 
			fscanf(fic,"%d",&noeud[pan_hexa[pa].noeud_contour[no]].type);
			fscanf(fic,"%d",&pan_hexa[pa].type_suivant_contour[no]);
			}
		fgets(t1,256,fic); 
		fgets(t1,256,fic); 
    		do  c=fgetc(fic); while (c !=':');
    		fscanf(fic,"%f%f%f\n",&pan_hexa[pa].raideur_traction_l,&pan_hexa[pa].raideur_traction_m,&pan_hexa[pa].raideur_traction_n);
    		do  c=fgetc(fic); while (c !=':'); 
     		fscanf(fic,"%f%f%f\n",&pan_hexa[pa].raideur_compression_l,&pan_hexa[pa].raideur_compression_m,&pan_hexa[pa].raideur_compression_n);
    		do  c=fgetc(fic); while (c !=':'); 
    		fscanf(fic,"%f%f%f\n",&pan_hexa[pa].lo_repos,&pan_hexa[pa].mo_repos,&pan_hexa[pa].no_repos);
    		do  c=fgetc(fic); while (c !=':'); 
     		fscanf(fic,"%f%f%f\n",&pan_hexa[pa].diam_hydro_l,&pan_hexa[pa].diam_hydro_m,&pan_hexa[pa].diam_hydro_n);
  		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&pan_hexa[pa].rho);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&pan_hexa[pa].cdnormal);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&pan_hexa[pa].ftangent);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%f\n",&pan_hexa[pa].pas_maillage);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%d\n",&pan_hexa[pa].type_noeud);
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%d\n",&pan_hexa[pa].type_maillage);
		/*printf("t1  : %s\n",t1); */
		printf("raideur_traction 	: %9.2f\n",pan_hexa[pa].raideur_traction_l );
		printf("cdnormal 		: %9.2f\n",pan_hexa[pa].cdnormal );
		printf("ftangent 		: %9.2f\n",pan_hexa[pa].ftangent );
		printf("type_noeud 		: %9d\n",pan_hexa[pa].type_noeud );
		printf("pas_maillage 		: %9.2f\n",pan_hexa[pa].pas_maillage );
		}
	
 	
 	
 	/*elements*****************************************************/
    	do  c=fgetc(fic); while (c !=':'); 
    	fscanf(fic,"%d\n",&chalut.nb_element);
	if (chalut.nb_element >= NBMAXELEMENT)
		{
		printf(" NBMAXELEMENT trop petit %4d < %4d \n",NBMAXELEMENT,chalut.nb_element );
		exit(0);
		}
	printf("chalut.nb_element  = %8d\n", chalut.nb_element); 
	for (pa=1;pa<=chalut.nb_element;pa++)
		{
    		element[pa].nb_point = 2;
    		
    		do  c=fgetc(fic); while (c !=':'); 
    		do  c=fgetc(fic); while (c !=':'); 
		
		/**********************************************/
 	  	element[pa].type_du_noeud = (int *) malloc((1 + element[pa].nb_point) * sizeof(int));
		if (element[pa].type_du_noeud    == NULL)
			{
			printf(" element[pa].type_du_noeud 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		
		for (no=1;no<=2;no++)
			{
			fscanf(fic,"%d",&temp_int);
			
			if (coordonnee_neutre == 1)
				{
				fscanf(fic,"%f %f%f",&inutile_float,
						     &element[pa].x[no],
						     &element[pa].y[no]);
				printf("x %5.3f y %5.3f\n",element[pa].x[no],element[pa].y[no]);
				}
			if (coordonnee_neutre == 2)
				{
				fscanf(fic,"%f %f%f",&element[pa].y[no],
						     &inutile_float,
						     &element[pa].x[no]);
				printf("x %5.3f y %5.3f\n",element[pa].x[no],element[pa].y[no]);
				}
			if (coordonnee_neutre == 3)
				{
				fscanf(fic,"%f %f%f",&element[pa].x[no],
						     &element[pa].y[no],
						     &inutile_float);
				printf("x %5.3f y %5.3f\n",element[pa].x[no],element[pa].y[no]);
				}
			fscanf(fic,"%d",&element[pa].type_du_noeud[no]);
			}
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&element[pa].raideur_traction);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&element[pa].raideur_compression);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&element[pa].longueur_repos);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&element[pa].rho);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&element[pa].diam_hydro);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&element[pa].cdnormal);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&element[pa].ftangent);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&element[pa].nb_barre);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&element[pa].type_noeud);
		printf("longueur_repos = %8.3f  \n",element[pa].longueur_repos);
		}

	/*coulisses*****************************************************/
    	do  c=fgetc(fic); while (c !=':'); 
    	fscanf(fic,"%d\n",&chalut.nb_coulisse);
	if (chalut.nb_coulisse >= NBMAXELEMENT)
		{
		printf(" NBMAXELEMENT trop petit %4d < %4d \n",NBMAXELEMENT,chalut.nb_coulisse );
		exit(0);
		}
	printf("chalut.nb_coulisse  = %8d\n", chalut.nb_coulisse); 
	for (pa=1;pa<=chalut.nb_coulisse;pa++)
		{
    		do  c=fgetc(fic); while (c !=':'); 
    		do  c=fgetc(fic); while (c !=':'); 
		for (no=1;no<=2;no++)
			{
			chalut.nb_total++;
			if (chalut.nb_total >= NBMAXNOEUD)
				{
				printf(" NBMAXNOEUD trop petit %4d < %4d \n",NBMAXNOEUD,chalut.nb_total );
				exit(0);
				}
			fscanf(fic,"%d",&coulisse[pa].extremite[no]);
			coulisse[pa].extremite[no] = chalut.nb_total;
			if (chalut.orientation == 1)
				{
				fscanf(fic,"%f %f%f",&noeud[coulisse[pa].extremite[no]].z,
						     &noeud[coulisse[pa].extremite[no]].x,
						     &noeud[coulisse[pa].extremite[no]].y);
				}
			if (chalut.orientation == 2)
				{
				fscanf(fic,"%f %f%f",&noeud[coulisse[pa].extremite[no]].y,
						     &noeud[coulisse[pa].extremite[no]].z,
						     &noeud[coulisse[pa].extremite[no]].x);
				}
			if (chalut.orientation == 3)
				{
				fscanf(fic,"%f %f%f",&noeud[coulisse[pa].extremite[no]].x,
						     &noeud[coulisse[pa].extremite[no]].y,
						     &noeud[coulisse[pa].extremite[no]].z);
				}
			fscanf(fic,"%d",&noeud[coulisse[pa].extremite[no]].type);
			}
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&coulisse[pa].raideur_traction);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&coulisse[pa].raideur_compression);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&coulisse[pa].longueur_repos);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&coulisse[pa].rho);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&coulisse[pa].diam_hydro);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&coulisse[pa].cdnormal);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%f\n",&coulisse[pa].ftangent);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&coulisse[pa].nb_noeud);
    		do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&coulisse[pa].type_noeud);
		printf("longueur_repos = %8.2f  \n",coulisse[pa].longueur_repos);
		/**********************************************/
  		coulisse[pa].noeud = (int *) malloc((1 + coulisse[pa].nb_noeud) * sizeof(int));
		if (coulisse[pa].noeud    == NULL)
			{
			printf(" coulisse[pa].noeud 1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
    		}



	/*liaisons*****************************************************/
	
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&chalut.nb_liaison); 
	printf("chalut.nb_liaison  = %8d\n", chalut.nb_liaison);
	if ( chalut.nb_liaison >= NBMAXLIAISON)
		{
		printf(" NBMAXLIAISON trop petit %4d < %4d \n",NBMAXLIAISON,chalut.nb_liaison );
		exit(0);
		}
	for (pa=1;pa<=chalut.nb_liaison;pa++)
		{
    		do  c=fgetc(fic); while (c !=':'); fscanf(fic,"%d",&liaison[pa].nb_pt);
		printf("liaison[%d].nb_pt  = %8d\n",pa,liaison[pa].nb_pt);
		
		/**********************************************/
  		liaison[pa].type = (int *) malloc((1 + chalut.nb_liaison) * sizeof(int));
		if (liaison[pa].type    == NULL)
			{
			printf("liaison[pa].type  1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		liaison[pa].nd = (int *) malloc((1 + chalut.nb_liaison) * sizeof(int));
		if (liaison[pa].nd    == NULL)
			{
			printf("liaison[pa].nd  1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		liaison[pa].num_entite = (int *) malloc((1 + chalut.nb_liaison) * sizeof(int));
		if (liaison[pa].num_entite    == NULL)
			{
			printf("liaison[pa].num_entite  1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
   		liaison[pa].x = (float *) malloc((1 + chalut.nb_liaison) * sizeof(float));
		if (liaison[pa].x    == NULL)
			{
			printf("liaison[pa].x  1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		liaison[pa].y = (float *) malloc((1 + chalut.nb_liaison) * sizeof(float));
		if (liaison[pa].y    == NULL)
			{
			printf("liaison[pa].y  1 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		
 		
		for (no=1;no<=liaison[pa].nb_pt;no++)
			{
			temp2_int = FALSE;				/*varable de condition de sortie de while =TRUE si le prog a lu un p ou un e*/
    		    	printf ("num de noeud = %d\n",no);
	    		do  
	    			{
	    			c=fgetc(fic); 
	    			printf ("c = %c\n",c);
				/*exit(0);*/
	    			if (c == 'p') liaison[pa].type[no]= 0;
	    			if (c == 'e') liaison[pa].type[no]= 1;
	    			printf("c %c\n",c);
	    			printf("liaison[%d].type[%d] %d\n",pa,no,liaison[pa].type[no]);
				if (c == 'e' || c == 'p') temp2_int = TRUE;	/*condition de sortie de while*/
	    			}
	    		while (temp2_int != 1); 
	    		
    		    	printf ("num de noeud = %d\n",no);
    		    	printf ("liaison[%d].type[%d]= %d\n",pa,no,liaison[pa].type[no]);
	    		
			do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&liaison[pa].num_entite[no]);
    			do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&liaison[pa].nd[no]);
 			
    		    	
    		    	if (liaison[pa].type[no]== 0)		/*a virer qd hector n utilisera plus lisaison.x et liaison.y*/
    		    	   {
    		    	   liaison[pa].x[no]=panneau[liaison[pa].num_entite[no]].x[liaison[pa].nd[no]];
    		    	   liaison[pa].y[no]=panneau[liaison[pa].num_entite[no]].y[liaison[pa].nd[no]];
    		    	   }
    		    	
     		    	if (liaison[pa].type[no]== 1)		/*a virer qd hector n utilisera plus lisaison.x et liaison.y*/
    		    	   {
    		    	   liaison[pa].x[no]=element[liaison[pa].num_entite[no]].x[liaison[pa].nd[no]];
    		    	   liaison[pa].y[no]=element[liaison[pa].num_entite[no]].y[liaison[pa].nd[no]];
    		    	   }
    		    	
   		    	printf ("liaison[%d].num_entite[%d]= %d\n",pa,no,liaison[pa].num_entite[no]);
    		    	printf ("liaison[%d].nd[%d]= %d\n\n",pa,no,liaison[pa].nd[no]);
	    		}
 			/*fgets(t1,256,fic);			retour a la ligne*/
	    	}
	    
	    
	
	    
			
	/*ordre de maillage***************************************************/
	
	chalut.nb_ordre_objet = chalut.nb_panneau + chalut.nb_element + chalut.nb_coulisse + chalut.nb_pan_hexa;
	
	/**********************************************/
  	ordre_maill_entite = (int *) malloc((1 + chalut.nb_ordre_objet) * sizeof(int));
	if (ordre_maill_entite    == NULL)
		{
		printf("ordre_maill_entite  1 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
		
	/**********************************************/
  	ordre_maill_num_entite = (int *) malloc((1 + chalut.nb_ordre_objet) * sizeof(int));
	if (ordre_maill_num_entite    == NULL)
		{
		printf("ordre_maill_num_entite  1 = NULL  \n" );
		exit(0);
		}
	/**********************************************/
	
	do  
		{
		c=fgetc(fic); 		/*pour aller a la ligne (aucune action)*/
		printf("rho %c\n",c);
		}
	while (c !=':'); 
		
		
	if (chalut.nb_ordre_objet > 0)
	   {
	   for (i=1;i<=chalut.nb_ordre_objet;i++)
		   { 
 		   temp2_int = FALSE;				/*variable de condition de sortie de while =TRUE si le prog a lu un p ou un e*/
 		   temp3_int = 0;
	    	   do  
	    		   {
	    		   c=fgetc(fic); 
	    		   printf ("c1 = %c\n",c);
	    		   if (c == 'p') ordre_maill_entite[i]= 0;
	    		   if (c == 'e') ordre_maill_entite[i]= 1;
			   if (c == 'e' || c == 'p') 
			      {
			      temp2_int = TRUE;			/*condition de sortie de while (while n accepte pas les characteres*/
			      flag_ordre_maillage = 1;		/*vaut 1 si il y a un ordre de maillage defini, 0 sinon*/
			      }
			   temp3_int++;
			   printf("temp3_int = %d\n",temp3_int);

	    		   }
	    	   while ((temp2_int != 1) && (temp3_int < 6));		/*s il y a deja eu 6 iterations, on est dans le cas ou il n y a pas d ordre de maillage*/ 
	    	   						/*et il faut forcer le prog a sortir de la boucle while*/
		   printf("ordre_maill_entite[%d] %d\n",i,ordre_maill_entite[i]);

		   if (temp3_int < 6)
		      {
		      do   
	    		 {
	    		 c=fgetc(fic); 
	    		 printf ("c2 = %c\n",c);
	    		 }
		      while (c !=':');
		      fscanf(fic,"%d\n",&ordre_maill_num_entite[i]);
		      printf("ordre_maill_num_entite[%d] %d\n",i,ordre_maill_num_entite[i]);
		      }
 	           }
 	    }
	
		
		
		
		
	/*Types de Noeuds*****************************************************/
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&chalut.nb_type_noeud); 
	printf("chalut.nb_type_noeud = %4d\n",chalut.nb_type_noeud );
	if ( chalut.nb_type_noeud >= NBMAXTYPENOEUD)
		{
		printf(" NBMAXTYPENOEUD trop petit %4d < %4d \n",NBMAXTYPENOEUD,chalut.nb_type_noeud );
		exit(0);
		}
	for (pa=1;pa<=chalut.nb_type_noeud;pa++)
		{
		do  c=fgetc(fic); while (c !=':');	
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f %f%f",&TypeNoeud[pa].mx,&TypeNoeud[pa].my,&TypeNoeud[pa].mz);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f %f%f",&TypeNoeud[pa].majx,&TypeNoeud[pa].majy,&TypeNoeud[pa].majz);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f %f%f",&TypeNoeud[pa].lonx,&TypeNoeud[pa].lony,&TypeNoeud[pa].lonz);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f %f%f",&TypeNoeud[pa].cdx,&TypeNoeud[pa].cdy,&TypeNoeud[pa].cdz);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f %f%f",&TypeNoeud[pa].fextx,&TypeNoeud[pa].fexty,&TypeNoeud[pa].fextz);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d %d%d",&TypeNoeud[pa].fixx,&TypeNoeud[pa].fixy,&TypeNoeud[pa].fixz);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f %f%f",&TypeNoeud[pa].limx,&TypeNoeud[pa].limy,&TypeNoeud[pa].limz);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d %d%d",&TypeNoeud[pa].senx,&TypeNoeud[pa].seny,&TypeNoeud[pa].senz);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d %d%d",&TypeNoeud[pa].symx,&TypeNoeud[pa].symy,&TypeNoeud[pa].symz);
		}
		
	/*ENVIRONNEMENT NUMERIQUE*****************************************************/
		do  c=fgetc(fic);  while (c !=':');	
		fscanf(fic,"%f\n",&Numerique.DIVISEUR); 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Numerique.Seuilconvergence); 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Numerique.Deplacement); 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%10d\n",&Numerique.Nbmaxiterations); 
		do  c=fgetc(fic); while (c !=':');	
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Numerique.Pascalcul); 
		do  c=fgetc(fic); while (c !=':');	
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Numerique.Passtockage); 
		do  c=fgetc(fic); while (c !=':');	
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Numerique.Debutstockage); 
		do  c=fgetc(fic); while (c !=':');	
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Numerique.Finstockage); 

	/*ENVIRONNEMENT METEOROLOGIQUE ET OCEANIQUE***********************************/
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Courant.direction); 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Courant.vitesse); 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Houle.periode); 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Houle.hauteur); 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Houle.direction); 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Houle.Depth1);
		 
	/*DESCRIPTION DE LA PRISE*****************************************************/
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Prise.volume);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Prise.seuil);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Prise.cd);
		
	/*ENVIRONNEMENT DU FOND MARIN*************************************************/
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Fond.coef_frottement);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%f\n",&Fond.raideur);
		

	/*sortie texte distance*****************************************************/
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.nb_distance); 
	printf("Sortie_texte.nb_distance  = %8d\n",Sortie_texte.nb_distance );
       /**********************************************/
       Sortie_texte.type_structure1_distance = (char *) malloc((1 + Sortie_texte.nb_distance) * sizeof(char));
       if (Sortie_texte.type_structure1_distance    == NULL)
               {
               printf("Sortie_texte.type_structure1_distance  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.numero_structure1_distance = (int *) malloc((1 + Sortie_texte.nb_distance) * sizeof(int));
       if (Sortie_texte.numero_structure1_distance    == NULL)
               {
               printf("Sortie_texte.numero_structure1_distance  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.noeud1_distance = (int *) malloc((1 + Sortie_texte.nb_distance) * sizeof(int));
       if (Sortie_texte.noeud1_distance    == NULL)
               {
               printf("Sortie_texte.noeud1_distance  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.type_structure2_distance = (char *) malloc((1 + Sortie_texte.nb_distance) * sizeof(char));
       if (Sortie_texte.type_structure2_distance    == NULL)
               {
               printf("Sortie_texte.type_structure2_distance  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.numero_structure2_distance = (int *) malloc((1 + Sortie_texte.nb_distance) * sizeof(int));
       if (Sortie_texte.numero_structure2_distance    == NULL)
               {
               printf("Sortie_texte.numero_structure2_distance  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.noeud2_distance = (int *) malloc((1 + Sortie_texte.nb_distance) * sizeof(int));
       if (Sortie_texte.noeud2_distance    == NULL)
               {
               printf("Sortie_texte.noeud2_distance  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.decimale_distance = (int *) malloc((1 + Sortie_texte.nb_distance) * sizeof(int));
       if (Sortie_texte.decimale_distance    == NULL)
               {
               printf("Sortie_texte.decimale_distance  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.comment_distance = (COMMENTAIRE *) malloc((1 + Sortie_texte.nb_distance) * sizeof(COMMENTAIRE));
       /*Sortie_texte.comment_distance = (char *) malloc((1 + Sortie_texte.nb_distance) * 156 * sizeof(char));*/
       if (Sortie_texte.comment_distance    == NULL)
               {
               printf("Sortie_texte.comment_distance  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
	for (pa=1;pa<=Sortie_texte.nb_distance;pa++)
		{
    		do  c=fgetc(fic); while (c !=':');
    		do  c=fgetc(fic); while (c !=':');
    		
		c=fgetc(fic); 
		while ((c =='#') || (c ==' '))
			{
			c=fgetc(fic); 
			}
		for (i=1;i<=155;i++)
			{
			Sortie_texte.comment_distance[pa].texte[i] = ' ';
			}
		i = 0;
		while ((c !='#') && (i<155))
			{
			i++;
			Sortie_texte.comment_distance[pa].texte[i] = c;
			c=fgetc(fic);
			}
		 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.noeud1_distance[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.noeud2_distance[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.decimale_distance[pa]);
		
		printf("noeud1 = %d ",Sortie_texte.noeud1_distance[pa]);
		printf("noeud2 = %d ",Sortie_texte.noeud2_distance[pa]);
		printf("decimale_distance = %d\n",Sortie_texte.decimale_distance[pa]);
		}



	/*sortie texte effort*****************************************************/
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.nb_effort); 
	printf("Sortie_texte.nb_effort  = %8d\n",Sortie_texte.nb_effort );
       /**********************************************/
       Sortie_texte.noeud_effort = (int *) malloc((1 + Sortie_texte.nb_effort) * sizeof(int));
       if (Sortie_texte.noeud_effort    == NULL)
               {
               printf("Sortie_texte.noeud_effort  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.axe_effort = (int *) malloc((1 + Sortie_texte.nb_effort) * sizeof(int));
       if (Sortie_texte.axe_effort    == NULL)
               {
               printf("Sortie_texte.axe_effort  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.decimale_effort = (int *) malloc((1 + Sortie_texte.nb_effort) * sizeof(int));
       if (Sortie_texte.decimale_effort    == NULL)
               {
               printf("Sortie_texte.decimale_effort  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.comment_effort = (COMMENTAIRE *) malloc((1 + Sortie_texte.nb_effort) * sizeof(COMMENTAIRE));
       /*Sortie_texte.comment_effort = (char *) malloc((1 + Sortie_texte.nb_effort) * 156 * sizeof(char));*/
       if (Sortie_texte.comment_effort    == NULL)
               {
               printf("Sortie_texte.comment_effort  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
	for (pa=1;pa<=Sortie_texte.nb_effort;pa++)
		{
    		do  c=fgetc(fic); while (c !=':');
    		do  c=fgetc(fic); while (c !=':');
    		
		c=fgetc(fic); 
		while ((c =='#') || (c ==' '))
			{
			c=fgetc(fic); 
			}
		for (i=1;i<=155;i++)
			{
			Sortie_texte.comment_effort[pa].texte[i] = ' ';
			}
		i = 0;
		while ((c !='#') && (i<155))
			{
			i++;
			Sortie_texte.comment_effort[pa].texte[i] = c;
			c=fgetc(fic);
			}
		 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.noeud_effort[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.axe_effort[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.decimale_effort[pa]);
		
		printf("noeud = %d ",Sortie_texte.noeud_effort[pa]);
		printf("axe = %d ",Sortie_texte.axe_effort[pa]);
		printf("decimale_effort = %d\n",Sortie_texte.decimale_effort[pa]);
		}

	/*sortie texte tension*****************************************************/
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.nb_tension_element); 
	printf("Sortie_texte.nb_tension_element  = %8d\n",Sortie_texte.nb_tension_element );
       /**********************************************/
       Sortie_texte.element_tension = (int *) malloc((1 + Sortie_texte.nb_tension_element) * sizeof(int));
       if (Sortie_texte.element_tension    == NULL)
               {
               printf("Sortie_texte.element_tension  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.element_extremite = (int *) malloc((1 + Sortie_texte.nb_tension_element) * sizeof(int));
       if (Sortie_texte.element_extremite    == NULL)
               {
               printf("Sortie_texte.element_extremite  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.decimale_tension_element = (int *) malloc((1 + Sortie_texte.nb_tension_element) * sizeof(int));
       if (Sortie_texte.decimale_tension_element    == NULL)
               {
               printf("Sortie_texte.decimale_tension_element  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.comment_tension_element = (COMMENTAIRE *) malloc((1 + Sortie_texte.nb_tension_element) * sizeof(COMMENTAIRE));
       /*Sortie_texte.comment_tension_element = (char *) malloc((1 + Sortie_texte.nb_tension_element) * 156 * sizeof(char));*/
       if (Sortie_texte.comment_tension_element    == NULL)
               {
               printf("Sortie_texte.comment_tension_element  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
	for (pa=1;pa<=Sortie_texte.nb_tension_element;pa++)
		{
    		do  c=fgetc(fic); while (c !=':');
    		do  c=fgetc(fic); while (c !=':');
    		
		c=fgetc(fic); 
		while ((c =='#') || (c ==' '))
			{
			c=fgetc(fic); 
			}
		for (i=1;i<=155;i++)
			{
			Sortie_texte.comment_tension_element[pa].texte[i] = ' ';
			}
		i = 0;
		while ((c !='#') && (i<155))
			{
			i++;
			Sortie_texte.comment_tension_element[pa].texte[i] = c;
			c=fgetc(fic);
			}
		 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.element_tension[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.element_extremite[pa]);
		if ((Sortie_texte.element_extremite[pa] < 1) || (Sortie_texte.element_extremite[pa] > 2))
			{
			printf("ATTENTION l extremite de la tension no  %d doit etre 1 ou 2",pa);
			printf(" et non  %d \n",Sortie_texte.element_extremite[pa]);
			Sortie_texte.element_extremite[pa] = 1;
			}
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.decimale_tension_element[pa]);
		
		printf("element_tension = %d ",Sortie_texte.element_tension[pa]);
		printf("element_extremite = %d ",Sortie_texte.element_extremite[pa]);
		printf("decimale_tension_element = %d\n",Sortie_texte.decimale_tension_element[pa]);
		}


	/*sortie texte tension_coulisse*****************************************************/
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.nb_tension_coulisse); 
	printf("Sortie_texte.nb_tension_coulisse  = %8d\n",Sortie_texte.nb_tension_coulisse );
       /**********************************************/
       Sortie_texte.coulisse_tension = (int *) malloc((1 + Sortie_texte.nb_tension_coulisse) * sizeof(int));
       if (Sortie_texte.coulisse_tension    == NULL)
               {
               printf("Sortie_texte.coulisse_tension  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.coulisse_extremite = (int *) malloc((1 + Sortie_texte.nb_tension_coulisse) * sizeof(int));
       if (Sortie_texte.coulisse_extremite    == NULL)
               {
               printf("Sortie_texte.coulisse_extremite  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.decimale_tension_coulisse = (int *) malloc((1 + Sortie_texte.nb_tension_coulisse) * sizeof(int));
       if (Sortie_texte.decimale_tension_coulisse    == NULL)
               {
               printf("Sortie_texte.decimale_tension_coulisse  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.comment_tension_coulisse = (COMMENTAIRE *) malloc((1 + Sortie_texte.nb_tension_coulisse) * sizeof(COMMENTAIRE));
       /*Sortie_texte.comment_tension_coulisse = (char *) malloc((1 + Sortie_texte.nb_tension_coulisse) * 156 * sizeof(char));*/
       if (Sortie_texte.comment_tension_coulisse    == NULL)
               {
               printf("Sortie_texte.comment_tension_coulisse  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
	for (pa=1;pa<=Sortie_texte.nb_tension_coulisse;pa++)
		{
    		do  c=fgetc(fic); while (c !=':');
    		do  c=fgetc(fic); while (c !=':');
    		
		c=fgetc(fic); 
		while ((c =='#') || (c ==' '))
			{
			c=fgetc(fic); 
			}
		for (i=1;i<=155;i++)
			{
			Sortie_texte.comment_tension_coulisse[pa].texte[i] = ' ';
			}
		i = 0;
		while ((c !='#') && (i<155))
			{
			i++;
			Sortie_texte.comment_tension_coulisse[pa].texte[i] = c;
			c=fgetc(fic);
			}
		 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.coulisse_tension[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.coulisse_extremite[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.decimale_tension_coulisse[pa]);
		
		printf("coulisse_tension = %d ",Sortie_texte.coulisse_tension[pa]);
		printf("coulisse_extremite = %d ",Sortie_texte.coulisse_extremite[pa]);
		printf("decimale_tension_coulisse = %d\n",Sortie_texte.decimale_tension_coulisse[pa]);
		}

	/*sortie texte position*****************************************************/
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.nb_position); 
	printf("Sortie_texte.nb_position  = %8d\n",Sortie_texte.nb_position );
       /**********************************************/
       Sortie_texte.noeud_position = (int *) malloc((1 + Sortie_texte.nb_position) * sizeof(int));
       if (Sortie_texte.noeud_position    == NULL)
               {
               printf("Sortie_texte.noeud_position  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.axe_position = (int *) malloc((1 + Sortie_texte.nb_position) * sizeof(int));
       if (Sortie_texte.axe_position    == NULL)
               {
               printf("Sortie_texte.axe_position  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.decimale_position = (int *) malloc((1 + Sortie_texte.nb_position) * sizeof(int));
       if (Sortie_texte.decimale_position    == NULL)
               {
               printf("Sortie_texte.decimale_position  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
       Sortie_texte.comment_position = (COMMENTAIRE *) malloc((1 + Sortie_texte.nb_position) * sizeof(COMMENTAIRE));
       /*Sortie_texte.comment_position = (char *) malloc((1 + Sortie_texte.nb_position) * 156 * sizeof(char));*/
       if (Sortie_texte.comment_position    == NULL)
               {
               printf("Sortie_texte.comment_position  1 = NULL  \n" );
               exit(0);
               }
       /**********************************************/
	for (pa=1;pa<=Sortie_texte.nb_position;pa++)
		{
    		do  c=fgetc(fic); while (c !=':');
    		do  c=fgetc(fic); while (c !=':');
    		
		c=fgetc(fic); 
		while ((c =='#') || (c ==' '))
			{
			c=fgetc(fic); 
			}
		for (i=1;i<=155;i++)
			{
			Sortie_texte.comment_position[pa].texte[i] = ' ';
			}
		i = 0;
		while ((c !='#') && (i<155))
			{
			i++;
			Sortie_texte.comment_position[pa].texte[i] = c;
			c=fgetc(fic);
			}
		 
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.noeud_position[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.axe_position[pa]);
		do  c=fgetc(fic); while (c !=':');	
		fscanf(fic,"%d\n",&Sortie_texte.decimale_position[pa]);
		
		printf("noeud_position = %d ",Sortie_texte.noeud_position[pa]);
		printf("axe_position = %d ",Sortie_texte.axe_position[pa]);
		printf("decimale_position = %d\n",Sortie_texte.decimale_position[pa]);
		}
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.effort_structure); 
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.diametre_prise); 
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.epaisseur_prise); 
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.volume_capture); 
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.surface_filtree); 
	do  c=fgetc(fic); while (c !=':');fscanf(fic,"%d\n",&Sortie_texte.vitesse_courant); 
	fclose(fic);	
	initialisation_min_max2();
	}


	
	
void initialisation_min_max2()
	{
	int entite,noeud;
	
	/*initialisation des min et des max*********************************************/
	minx = panneau[1].x[1];	maxx = panneau[1].x[1];
	miny = panneau[1].y[1];	maxy = panneau[1].y[1];
	/*minz = panneau[1].z[1];	maxz = panneau[1].z[1];*/
	for (entite=1;entite<=chalut.nb_panneau;entite++)
	   {
	   for (noeud=1;noeud<=panneau[entite].nb_point;noeud++)
		{
		if(panneau[entite].x[noeud] < minx) minx = panneau[entite].x[noeud];
		if(panneau[entite].x[noeud] > maxx) maxx = panneau[entite].x[noeud];
		if(panneau[entite].y[noeud] < miny) miny = panneau[entite].y[noeud];
		if(panneau[entite].y[noeud] > maxy) maxy = panneau[entite].y[noeud];
		/*if(panneau[entite].z[noeud] < minz) minz = panneau[entite].z[noeud];
		if(panneau[entite].z[noeud] > maxz) maxz = panneau[entite].z[noeud];*/
		}
	   }
	
	for (entite=1;entite<=chalut.nb_element;entite++)
	   {
	   for (noeud=1;noeud<=element[entite].nb_point;noeud++)
		{
		if(element[entite].x[noeud] < minx) minx = element[entite].x[noeud];
		if(element[entite].x[noeud] > maxx) maxx = element[entite].x[noeud];
		if(element[entite].y[noeud] < miny) miny = element[entite].y[noeud];
		if(element[entite].y[noeud] > maxy) maxy = element[entite].y[noeud];
		/*if(element[entite].z[noeud] < minz) minz = element[entite].z[noeud];
		if(element[entite].z[noeud] > maxz) maxz = element[entite].z[noeud];*/
		}
	   }
	if ((maxx == minx) && (maxy == miny) && (maxz == minz))
		{
		maxx = minx + 1.0;
		maxy = miny + 1.0;
		maxz = minz + 1.0;
		}	
	if ((maxx == minx) && (maxy == miny) && (maxz != minz))
		{
		maxx = minx + maxz - minz;
		maxy = miny + maxz - minz;
		}
	if ((maxx == minx) && (maxy != miny) && (maxz == minz))
		{
		maxx = minx + maxy - miny;
		maxz = minz + maxy - miny;
		}
	if ((maxx != minx) && (maxy == miny) && (maxz == minz))
		{
		maxy = miny + maxx - minx;
		maxz = minz + maxx - minx;
		}
	if ((maxx == minx) && (maxy != miny) && (maxz != minz))
		{
		maxx = minx + maxy - miny;
		}
	if ((maxx != minx) && (maxy == miny) && (maxz != minz))
		{
		maxy = miny + maxz - minz;
		}
	if ((maxx != minx) && (maxy != miny) && (maxz == minz))
		{
		maxz = minz + maxx - minx;
		}
		
		
	if ((maxx - minx) > (maxy - miny) && (maxx - minx) > (maxz - minz) ) ecartmax = maxx - minx;
	if ((maxy - miny) > (maxz - minz) && (maxy - miny) > (maxx - minx) ) ecartmax = maxy - miny;
	if ((maxz - minz) > (maxx - minx) && (maxz - minz) > (maxy - miny) ) ecartmax = maxz - minz;
	binx = minx - 0.05 * (maxx - minx);
	biny = miny - 0.05 * (maxy - miny);
	baxx = maxx + 0.05 * (maxx - minx);
	baxy = maxy + 0.05 * (maxy - miny);
	bcartmax = ecartmax;
	}
	

    	
    	
    	
    	
    	

    	
    	
    	
    	
    	
    	
	
