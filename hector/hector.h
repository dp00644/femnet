#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "libsx.h"		/* should come first, defines libsx stuff  */
#include "protos.h"

#if PRINCIPAL>0
#define CLASS	
#else
#define CLASS extern
#endif

#ifndef TRUE			/*provient de main.h*/
#define TRUE  1
#define FALSE 0
#endif

#define REEL			float
#define REAL			double		/*utilise dans triangle.c*/
#define RESOLUTION_X            1.
#define RESOLUTION_Y            1.

#define MARGE_X_PS		10.
#define MARGE_Y_PS		10.
#define RESOLUTION_X_PS 	(600.-2*MARGE_X_PS)
#define RESOLUTION_Y_PS 	(800.-2*MARGE_Y_PS)

#define NOM_POLICE_PS 		"Times-Roman"
#define MLEN 90

typedef struct MyProgram
	{
	int var1, var2;				/*de var1 a flags : viennent de
							main.h*/
	int curstate;					
	int flags;
	int   win_width, win_height;		/*proviennent de phobos.h*/
	}MyProgram;

void init_display(int argc, char **argv, MyProgram *me);

/* define's */
#define X_SIZE 600		/* default draw area size, change as desired */
#define Y_SIZE 600

#define NBMAXNOEUD		20000
#define NBMAXTRIANGLE		5000
#define NBMAXTRIHEXA		5000
#define NBMAXPANNEAU		200
#define NBMAXPANHEXA		200
#define NBMAXELEMENT		2000	
#define NBMAXBARRE		5000
#define NBMAXTYPEBARRE		1900
#define NBMAXNOEUDCONTOUR	100
#define NBMAXNOEUDCOTE		100
#define NBMAXNOEUDINTERIEUR	20000
#define NBMAXLIAISON		1300
#define NBMAXTYPENOEUD		1000
#define DIM1                    9000


#define RHO 1025                   /* masse volumique de l eau  */

CLASS float minx,maxx,miny,maxy,minz,maxz,ecartmax; /*valeursmaximales des coordonnees*/
CLASS float binx,baxx,biny,baxy,binz,baxz,bcartmax; /*valeursmaximales des coordonnees dessinnees*/
CLASS Widget w[100];
CLASS int total2global[NBMAXNOEUD],profondeur[NBMAXNOEUD];
CLASS char fname1[128],fname2[128],fname3[128];
CLASS char fname[128],oldfic[128],newfic[128];
CLASS float *h1x,*h2x,*h1y,*h2y;
CLASS float UU1[3],UU2[3],UU3[3],VV1[3],VV2[3],VV3[3],xx1[3],xx2[3],xx3[3],yy1[3],yy2[3],yy3[3],zz1[3],zz2[3],zz3[3];
CLASS float taille_fenetre,nb_maille_fenetre,lcm;		
CLASS int numero_polygone,numero_element,nb_point,nb_point_el;
CLASS float raideur_traction,raideur_compression,raideur_ouverture,maille_au_repos,mass_vol,diam_hydro,largeur_noeud;
CLASS float raideur_traction_el,raideur_compression_el,long_repos_el,mass_vol_el,diam_hydro_el,coef_trainee_norm_el,coef_trainee_tang_el;
CLASS int nb_barre_el,type_noeud_int_el,creation_panneau_terminee,col,couleur_element,mode_accrochage; 
CLASS float cd_normal,cd_tang,rapport,xxx1,xxx2,yyy1,yyy2,pas,pas_accrochage,x_pour_la_fonction_accrochage,y_pour_la_fonction_accrochage;
CLASS int numero_panneau,numero_element,pas_maillage,type_noeud_interieur,type_maillage;
CLASS int etat,etat6,etat7,etat9,etat15,reprendre_le_contour,reprendre_element,reprendre_liaison,creation_element_terminee;		/*sauvegarde des etats avant de faire un zoom*/
CLASS int nb_entite,dernier_pan_modifie,dernier_ele_modifie,type_de_noeud,type_noeud_propose,flag_ordre_maillage;
CLASS int *ordre_maill_entite,*ordre_maill_num_entite,limite_nb_element,limite_nb_panneau,coordonnee_neutre;
CLASS int num_pt_liaison;

   

CLASS struct Noeud
	{
	REEL x;								/*coordonnee cartesienne selon x*/
	REEL y;								/*coordonnee cartesienne selon y*/
	REEL z;								/*coordonnee cartesienne selon z*/
	REEL xcal;							/*coordonnee cartesienne calculee selon x*/
	REEL ycal;							/*coordonnee cartesienne calculee selon y*/
	REEL zcal;							/*coordonnee cartesienne calculee selon z*/
	REEL U;								/*coordonnee selon U en nb de maille pour les filets*/
	REEL V;								/*coordonnee selon V en nb de maille pour les filets*/
	REEL u;								/*coordonnee selon U en nb de maille pour les filets*/
	REEL v;								/*coordonnee selon V en nb de maille pour les filets*/
	int type; 							/*type du noeud*/
	int flag_sta; 							/*1 si visite, 0 sinon*/
	} noeud[NBMAXNOEUD],noeud_global[NBMAXNOEUD],noeud_old[NBMAXNOEUD];

CLASS struct Barre
	{
	int  noeud[3]; 							/*indice des noeuds extremite*/
	REEL longueur_repos;						/*longueur au repos*/
	REEL elongation;						/*elongation*/
	REEL longueur_tendue;                                           /*longueur tendue*/
	REEL tension;                                                  	/*tension*/
	REEL nrj;                                                       /*energie potentielle*/
	REEL pro[3];							/*proportion a la longueur de la barre*/
	int  type; 							/*type de la barre*/
	} barre[NBMAXBARRE],barre_old[NBMAXBARRE];

CLASS struct Element
	{
	int nb_barre; 				/*nb de barre dans l element*/
	int extremite[3];			/*indice des noeuds aux extremites*/
	int *noeud;				/*indice noeud a l interieur de l element*/
	int *barre;				/*indice barre a l interieur de l element*/
	float raideur_traction;			/*raideur en traction*/
	float raideur_compression;		/*raideur en compression*/
	float longueur_repos;			/*longueur au repos*/
	float rho;				/*masse volumique*/
	float diam_hydro;			/*diametre hydrodynamique*/
	float cdnormal;				/*coefficient de trainee*/
	float ftangent;				/*coefficient de trainee*/
	int flag_maillage;			/*1 si maille 0 si pas maille*/
	int type_noeud;				/*type des noeud a l interieur de l element*/
	int *type_du_noeud;			/*type des deux noeuds des extremites(cree pour hector uniquement)*/
	int flag_ordre_maillage;		/*1 si le panneau a deja ete selectionne dans la fonction ordre_de_maillage*/
	int nb_point;
	REEL x[3];							
	REEL y[3];
	float lcm;				/*longueur de cote de maille*/							
	} element[NBMAXELEMENT];

CLASS struct Coulisse
	{
	int extremite[3];			/*indice des noeuds aux extremites*/
	int nb_noeud; 				/*nb de noeud dans la coulisse exclut les extremites*/
	int *noeud;				/*indice noeud a l interieur de la coulisse*/
	float raideur_traction;			/*raideur en traction*/
	float raideur_compression;		/*raideur en compression*/
	float longueur_repos;			/*longueur au repos*/
	float lgtendue;
	float tension;
	float nrj;
	float elongation;
	float rho;				/*masse volumique*/
	float diam_hydro;			/*diametre hydrodynamique*/
	float cdnormal;				/*coefficient de trainee*/
	float ftangent;				/*coefficient de trainee*/
	int flag_maillage;			/*1 si maille 0 si pas maille*/
	int type_noeud;				/*type des noeud a l interieur de la coulisse*/
	} coulisse[NBMAXELEMENT], coulisse_old[NBMAXELEMENT];

CLASS struct Triangle
	{
	int noeud[4]; 				/*indice des sommets du triangle*/
	int type; 				/*numero du panneau*/
	int numerotation_locale; 		/*numero du triangle dans le panneau*/
	REEL U[4];				/*coordonnees des sommets en nombre de mailles selon U*/
	REEL V[4];				/*coordonnees des sommets en nombre de mailles selon V*/
	REEL n[4];				/*composante du cote de maille n*/
	REEL m[4];				/*composante du cote de maille m*/
	REEL tension1;				/*tension du cote de maille m*/
	REEL tension2;				/*tension du cote de maille n*/
	REEL lg_tendue1;			/*longueur du cote de maille m*/
	REEL lg_tendue2;			/*longueur du cote de maille n*/
	REEL nrj1;                              /*energie potentielle du cote de maille m*/
	REEL nrj2;                              /*energie potentielle du cote de maille n*/
	REEL nrj;	
	REEL Nx;				/*composante selon x de la normale normee au triangle*/
	REEL Ny;
	REEL Nz;
	REEL defaut_disc;			/*mesure du defaut de discretisation*/
	REEL nb_cote_u_ou_v;			/*nb de fils u ou v dans un triangle*/	
	}triangle[NBMAXTRIANGLE],triangle_old[NBMAXTRIANGLE];

CLASS struct Tri_Hexa
	{
	int noeud[4]; 				/*indice des sommets du triangle*/
	int type; 				/*indice des cotes du triangle*/
	REEL U[4];				/*coordonnees des sommets en nombre de mailles selon U*/
	REEL V[4];				/*coordonnees des sommets en nombre de mailles selon V*/
	REEL l[4];				/*composante du cote de maille l*/
	REEL m[4];				/*composante du cote de maille m*/
	REEL n[4];				/*composante du cote de maille n*/
	REEL tension1;				/*tension du cote de maille l*/
	REEL tension2;				/*tension du cote de maille m*/
	REEL tension3;				/*tension du cote de maille n*/
	REEL lg_tendue1;			/*longueur du cote de maille l*/
	REEL lg_tendue2;			/*longueur du cote de maille m*/
	REEL lg_tendue3;			/*longueur du cote de maille n*/
	REEL nrj1;                               /*energie potentielle du cote de maille l*/
	REEL nrj2;                               /*energie potentielle du cote de maille m*/
	REEL nrj3;                               /*energie potentielle du cote de maille n*/
	REEL nrj;	
	REEL nb_cote_l_m_n;			/*nb de fils u ou v dans un triangle*/	
	}tri_hexa[NBMAXTRIHEXA],tri_hexa_old[NBMAXTRIHEXA];


CLASS struct Panneau
	{
	int nb_noeud_contour; 				/*nb de noeuds du contour du panneau*/
	int nb_noeud_interieur; 			/*nb de noeuds interieur au panneau*/
	int nb_noeud_cote; 				/*nb de noeuds des cotes du panneau*/
	int *noeud_contour;				/*liste ordonnee des indices des noeuds du contour*/
	int *suivant_contour;				/*noeuds de cote qui suivent les noeuds du contour*/
	int *type_suivant_contour;			/*type des noeuds de cote qui suivent les noeuds du contour*/
	int *noeud_cote;				/*liste des indices des noeuds des cotes*/
	float *prop_cote;				/*liste des positions relatives des noeuds des cotes / longueur du cote*/
	int *noeud_interieur;				/*liste des indices des noeuds interieurs*/
	int *numero_triangle;				/*liste des indices des noeuds interieurs*/
	int nb_triangle_interieur; 			/*nb de triangles interieurs*/
	int triangle_interieur[NBMAXNOEUDINTERIEUR][4];	/*sommets des triangles interieurs dans la numerotation totale*/
	int nb_triangle_contour; 			/*nb de triangles poses sur le contour*/
	int triangle_contour[NBMAXNOEUDCONTOUR][4];	/*sommets des triangles poses sur le contour dans la numerotation locale*/
	float pas_maillage;				/*pas du maillage des noeuds interieurs*/
	int type_maillage;				/*type du maillage*/
	int flag_maillage;				/*1 si maille 0 si pas maille*/
	int flag_filet_contour;				/*1 si la triangulation sur le contour    est calculee 0 sinon*/
	int flag_filet;					/*1 si la triangulation sur ts les points est calculee 0 sinon*/
	float raideur_traction;				/*raideur en traction des fils constituant le filet de ce panneau*/
	float raideur_compression;			/*raideur en compression des fils constituant le filet de ce panneau*/
	float raideur_ouverture;			/*raideur a l ouverture des mailles du filet de ce panneau*/
	float longueur_repos;				/*cote de maille du filet de ce panneau*/
	float diam_hydro;				/*diametre hydro des fils du filet de ce panneau*/
	float largeurnoeud;				/*largeur des noeuds du filet de ce panneau*/
	float rho;					/*masse volumique des fils constituant le filet de ce panneau*/
	float cdnormal;					/*coef de trainee normale des fils constituant le filet de ce panneau*/
	float ftangent;					/*coef de trainee tangentielle des fils constituant le filet de ce panneau*/
	float surf_fils;				/*surface de fils de ce panneau*/
	int type_noeud;					/*type des noeud a l interieur du panneau*/
	int *type_du_noeud;				/*type de chaque noeud du contour du panneau (cree pour hector uniquement)*/
	REEL *x;							/*coordonnee cartesienne selon x*/
	REEL *y;							/*coordonnee cartesienne selon y*/
	REEL *U;							/*coordonnee "maille" selon x*/
	REEL *V;							/*coordonnee "maille" selon y*/
	int nombre_contour; 						/*numero du point du polygone*/
	int nb_point;							/*nb de points d un polygone donne*/
	int flag_ordre_maillage;			/*1 si le panneau a deja ete selectionne dans la fonction ordre_de_maillage*/
	int contour_ferme;				/*1 si le contour du panneau a ete ferme, 0 sinon*/
	float lcm;					/*longueur de cote de maille pour ce panneau*/
	} panneau[NBMAXPANNEAU];


CLASS struct Pan_Hexa
	{
	int nb_noeud_contour; 				/*nb de noeuds du contour du panneau*/
	int nb_noeud_interieur; 			/*nb de noeuds interieur au panneau*/
	int nb_noeud_cote; 				/*nb de noeuds des cotes du panneau*/
	int *noeud_contour;				/*liste ordonnee des indices des noeuds du contour*/
	int *suivant_contour;				/*noeuds de cote qui suivent les noeuds du contour*/
	int *type_suivant_contour;			/*type des noeuds de cote qui suivent les noeuds du contour*/
	int *noeud_cote;				/*liste des indices des noeuds des cotes*/
	float *prop_cote;				/*liste des positions relatives des noeuds des cotes / longueur du cote*/
	int *noeud_interieur;				/*liste des indices des noeuds interieurs*/
	int nb_tri_hexa_interieur; 			/*nb de triangles interieurs*/
	int tri_hexa_interieur[NBMAXNOEUDINTERIEUR][4];	/*sommets des triangles interieurs dans la numerotation totale*/
	int nb_tri_hexa_contour; 			/*nb de triangles poses sur le contour*/
	int tri_hexa_contour[NBMAXNOEUDCONTOUR][4];	/*sommets des triangles poses sur le contour dans la numerotation locale*/
	float pas_maillage;				/*pas du maillage des noeuds interieurs*/
	int type_maillage;				/*type du maillage*/
	int flag_maillage;				/*1 si maille 0 si pas maille*/
	int flag_filet_contour;				/*1 si la triangulation sur le contour    est calculee 0 sinon*/
	int flag_filet;					/*1 si la triangulation sur ts les points est calculee 0 sinon*/
	float raideur_traction_l,raideur_traction_m,raideur_traction_n;		/*raideur en traction des fils constituant le filet de ce panneau*/
	float raideur_compression_l,raideur_compression_m,raideur_compression_n;/*raideur en compression des fils constituant le filet de ce panneau*/
	float lo_repos,mo_repos,no_repos;		/*cote de maille l,m,n du filet de ce panneau*/
	float diam_hydro_l,diam_hydro_m,diam_hydro_n;	/*diametre hydro des fils du filet de ce panneau*/
	float rho;					/*masse volumique des fils constituant le filet de ce panneau*/
	float cdnormal;					/*coef de trainee normale des fils constituant le filet de ce panneau*/
	float ftangent;					/*coef de trainee tangentielle des fils constituant le filet de ce panneau*/
	float surf_fils;				/*surface de fils de ce panneau*/
	int type_noeud;					/*type des noeud a l interieur du panneau*/
	} pan_hexa[NBMAXPANHEXA];

CLASS struct Chalut
	{
	int nb_ordre_objet;             /*nombre d'objets(panneau,element,coulisse,pan_hexa)*/
	int nb_fils; 			/*nombre de fils dans les triangles contour constituants le chalut*/
	int nb_fils_fond_ecran; 	/*nombre de fils dans les triangles contour constituants le fond d ecran*/
	int nb_panneau; 		/*nombre de panneaux a maille losange constituants le chalut*/
	int nb_pan_hexa; 		/*nombre de panneaux a maille hexagonale constituants le chalut*/
	int nb_element; 		/*nombre de elements constituants le chalut*/
	int nb_coulisse; 		/*nombre de coulisses constituants le chalut*/
	int nb_liaison; 		/*nombre de liaisons dans le chalut*/
	int nb_lien; 			/*nombre de liens dans le chalut >=  liaisons*/
	int nb_total;			/*nombre de noeuds dans la numerotation totale dans le chalut*/
	int nb_global;			/*nombre de noeuds dans la numerotation globale dans le chalut apres renumerotation*/
	int nb_type_noeud;		/*nombre de type de noeud dans le chalut*/
	int nb_barre;			/*nombre de barre dans le chalut*/
	int nb_surface;			/*nombre de surface dans le chalut*/
	int nb_surf_hexa;		/*nombre de surface a maille hexagonale dans le chalut*/
	float surface_fils;		/*surface de fils du chalut tel que maille*/
	int orientation;		/*orientation du dessin 1 (2,3) perpendiculaire a l axe x (y,z)*/
	} chalut,chalut_old;



CLASS struct Point 
	{
	float 	mx,my,mz;
	float 	majx,majy,majz;
	float 	lonx,lony,lonz;
	float 	cdx,cdy,cdz;
	float 	fextx,fexty,fextz;
	int   	fixx,fixy,fixz;
	float 	limx,limy,limz;
	int   	senx,seny,senz;
	int   	symx,symy,symz;
	} TypeNoeud[DIM1]; 			/*bizarre comme declaration de strucuture : Point/TypeNoeud ..*/
	
CLASS struct Lien
	{
	int  nb_liaison;		/*nb de noeud utilisant cette liaison*/
	char *type; 			/*types des structures liees*/
	int  *structure; 		/*indice des structures liees*/
	int  *extremite; 		/*indice des noeuds lies (numerotation locale)*/
	int  *noeud; 			/*indice des coins lies (numerotation totale)*/
	} lien[NBMAXLIAISON];
	
CLASS struct Liaison			/*struct cree pour hector*/
	{
	int nb_pt;			/*nb de points impliques ds chaque liaison*/
	int *type;			/*0 si pa ; 1 si el*/
	int *nd;			/*numero du noeud a reinjecter dans le fichier don*/
	int *num_entite;		/*numero du panneau ou de l element suivant le cas*/
	float *x;			/*coordonnees selon x du point du pan ou de l ele qui a ete recupere en cliquant a cote*/
	float *y;			/*idem*/
	int *type_du_noeud;		/*type du noeud recupere */
	} liaison[NBMAXLIAISON];

CLASS struct ordre
	{
	char *type;
	int  *indice;
	} ordre;
	
CLASS struct numerique {
	float DIVISEUR;			/*raideur additionnelle en N*/
	int Nbmaxiterations;		/*nb max d iterations realisees dans unix*/
	float Seuilconvergence;		/*seuil de convergence en N*/
	float Deplacement;		/*deplacement maximal autorise en m*/
	float Pascalcul;		/*pas de calcul en dynamique en s*/
	float Passtockage;		/*pas de stockage en dynamique en s*/
	float Debutstockage;		/*debut du stockage en dynamique en s*/
	float Finstockage;		/*fin du stockage et du calculen dynamique en s*/
	} Numerique;	

CLASS struct houle {
	float hauteur;			/*hauteur de crete a creux en m*/
	float periode;			/*periode en s > 0*/	
	float direction;		/*direction dans XoY relativement a oX en degre*/
	float Depth1;			/*profondeur d eau en m > 0*/
	} Houle;	

CLASS struct courant {
	float vitesse;			/*amplitude de vitesse en m/s*/
	float direction;		/*direction dans XoY relativement a oX en degre*/
	} Courant;	

CLASS struct prise {
	float volume;			/*volume de la prise en m3 ne tient pas compte des symetrie*/
	float seuil;			/*seuil sur ce volume en m3*/
	float cd;			/*coef de trainee sur la prise*/
	float front;			/*position du front en m*/
	} Prise;	

CLASS struct fond {
	float raideur;			/*raideur a l enfoncement en N/m*/
	float coef_frottement;		/*rapport effort horizontal sur vertical*/
	} Fond;
	
typedef struct 	commentaire {
	char texte[156];
	} 	COMMENTAIRE;
	
CLASS struct sortie_texte 
	{
	int  nb_distance;			/*nb de distance entre 2 points affichees*/
	COMMENTAIRE *comment_distance;		/*commentaires associes aux distances*/
	char *type_structure1_distance; 	/*types des structures */
	int  *numero_structure1_distance; 	/*indice des structures */
	int  *noeud1_distance;			/*premiere extremite de la distance en numerotation locale*/		
	char *type_structure2_distance; 	/*types des structures */
	int  *numero_structure2_distance; 	/*indice des structures */
	int  *noeud2_distance;			/*seconde extremite de la distance*/		
	int  *decimale_distance;		/*nb de decimale pour l affichage de la distance*/
			
	int  nb_effort;				/*nb d effort selon un axe a un point affiches*/
	COMMENTAIRE *comment_effort;		/*commentaires associes aux distances*/
	char *type_structure_effort; 		/*types des structures */
	int  *numero_structure_effort; 		/*indice des structures */
	int  *noeud_effort;			/*noeud en numerotation locale*/		
	int  *axe_effort;			/*axe de l effort 1 : x, 2 : y et 3 : z*/		
	int  *decimale_effort;			/*nb de decimale pour l affichage */

	int  nb_tension_element;		/*nb de tension dans un element affichees*/
	COMMENTAIRE *comment_tension_element;	/*commentaires associes aux distances*/
	int *element_tension;			/*premiere extremite de la distance*/		
	int *element_extremite;			/*premiere extremite de la distance*/		
	int *decimale_tension_element;		/*nb de decimale pour l affichage de la distance*/

	int  nb_tension_coulisse;		/*nb de tension dans une coulisse affichees*/
	COMMENTAIRE *comment_tension_coulisse;	/*commentaires associes aux distances*/
	int *coulisse_tension;			/*premiere extremite de la distance*/		
	int *coulisse_extremite;		/*premiere extremite de la distance*/		
	int *decimale_tension_coulisse;		/*nb de decimale pour l affichage de la distance*/

	int  nb_position;			/*nb de position selon un axe a un point affichees*/
	COMMENTAIRE *comment_position;		/*commentaires associes aux distances*/
	char *type_structure_position; 		/*types des structures */
	int  *numero_structure_position; 	/*indice des structures */
	int  *noeud_position;			/*noeud en numerotation locale*/		
	int  *axe_position;			/*axe de l effort 1 : x, 2 : y et 3 : z*/		
	int *decimale_position;			/*nb de decimale pour l affichage de la distance*/
	
	int  nb_parametre;			/*nb de position selon un axe a un point affichees*/
	COMMENTAIRE *comment_parametre;		/*commentaires associes aux distances*/
	int *decimale_parametre;		/*nb de decimale pour l affichage de la distance*/

	int effort_structure;			/*si 1 affiche l effort sur toute la structure (N) selon les axes X Y et Z*/	
	int diametre_prise;			/*si 1 affiche diametre_prise (m)*/	
	int epaisseur_prise;			/*si 1 affiche epaisseur_prise (m)*/	
	int vitesse_courant;			/*si 1 affiche vitesse_courant (m/s)*/	
	int volume_capture;			/*si 1 affiche volume_capture (m3)*/	
	int surface_filtree;			/*si 1 affiche surface_filtree (m2)*/	
	
	}  Sortie_texte;

#define PI 		3.141592653589793 

typedef struct {
	int  tag;
	char *label;
	void *data;
	int  init;
	} TagList;
	
CLASS char triswitches[10];

CLASS int Flag_Fichier_charge; 		/* 1 si le fichier est charge 0 sinon */
CLASS int Flag_Fichier_charge_don; 	/* 1 si le fichier est charge 0 sinon */
CLASS int Flag_Fichier_charge_mdg; 	/* 1 si le fichier est charge 0 sinon */
CLASS int Flag_Fichier_charge_sta; 	/* 1 si le fichier est charge 0 sinon */
CLASS int Flag_Fichier_cree; 		/* 1 si le fichier est cree 0 sinon */
CLASS int Flag_Fichier_cree_sta; 	/* 1 si le fichier est cree 0 sinon */
CLASS int Flag_Fichier_cree_mdg; 	/* 1 si le fichier est cree 0 sinon */
CLASS int Flag_Mesh;    		/* 1 si mesh est fait 0 sinon */
CLASS int periode_fils;    		/*periode fils  */



#define TAG_NULL          0
#define TAG_STRING        1
#define TAG_INT           2
#define TAG_FLOAT         3
#define TAG_LABEL         4
#define TAG_WINDOW_LABEL  5
#define TAG_DONE          99

#define TAG_INIT    1
#define TAG_NOINIT  0

int GetValues_2(TagList *tags);

/*marge sur le graphique*/
#define marge 0.05


CLASS FILE *fic_ps; 		/* handle du fichier Postscript */
CLASS int hauteur_police_ps; 
CLASS int largeur_police_ps; 
CLASS float epaisseur_trait_ps;
CLASS char mode_portrait;	/* PostScript : sortie paysage ou portrait */
CLASS char flag_ps; 		/* Sortie Postscript ? */
CLASS char flag_ortho_norm; 	/* flag : tracer dans un repere orthonorme */
CLASS int taille_police_x; 	/* sur l'ecran */
CLASS int taille_police_y; 	/* sur l'ecran */
CLASS int delta_caract_x;	/* offset graphique : nb de demi caractere horizontal */
CLASS int delta_caract_y;	/* offset graphique : nb de demi caractere vertical */
CLASS char flag_delta_carac;	/* flag : utilise dans les routines graphiques pour effectuer un decalage de delta_carac sur les 
				   coordonnees */
CLASS char ligne_motif[10][5]; /* definition des styles (motif) des traces de ligne */
CLASS float RESOLUTION_X_FEN; /* Largeur de la fenetre sur l'ecran */
CLASS float RESOLUTION_Y_FEN; /* Hauteur de la fenetre sur l'ecran */


CLASS FILE *f1;

