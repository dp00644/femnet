#include "hector.h"              /* where program specific stuff is defined */

int button_downx,button_downy,button_number;


void button_down(Widget w, int which_button, int x, int y, void *data)
{
	MyProgram *me = (MyProgram *)data;
	
	button_downx = x;
	button_downy = y;
	/*button_number = which_button;*/
	/* SetMouseMotionCB(w, motion);*/
	/*printf("You clicked button %d at (%d,%d)", which_button, x,y);*/
}

void button_up_zoom(Widget w, int which_button, int x, int y, void *data)
	{
	
	MyProgram *me = (MyProgram *)data;
	float caxx,caxy,cinx,ciny;
	
	caxx = baxx;
	caxy = baxy;
	cinx = binx;
	ciny = biny;
  	/*printf("You released button %d at (%d,%d)\n", which_button, x,y);*/
  		
	if (which_button == 1)		/*clic gauche*/
		{		
		/*bouton gauche actionne : zoom autour du segment clique*/
		if (x < button_downx) 
			{
			binx = cinx + (float) (x           ) / RESOLUTION_X_FEN  * (caxx - cinx);
			baxx = cinx + (float) (button_downx) / RESOLUTION_X_FEN  * (caxx - cinx);
			}
		if (x > button_downx) 
			{
			binx = cinx + (float) (button_downx) / RESOLUTION_X_FEN  * (caxx - cinx);
			baxx = cinx + (float) (x           ) / RESOLUTION_X_FEN  * (caxx - cinx);
			}
		if (y > button_downy) 
			{
			biny = caxy + (float) (y           ) / RESOLUTION_Y_FEN  * (ciny - caxy);
			baxy = caxy + (float) (button_downy) / RESOLUTION_Y_FEN  * (ciny - caxy);
			}
		if (y < button_downy) 
			{
			biny = caxy + (float) (button_downy) / RESOLUTION_Y_FEN  * (ciny - caxy);
			baxy = caxy + (float) (y           ) / RESOLUTION_Y_FEN  * (ciny - caxy);
			}
		if (x == button_downx || y == button_downy) 
			{
			binx = minx - 0.05 * (maxx - minx);
			biny = miny - 0.05 * (maxy - miny);
			baxx = maxx + 0.05 * (maxx - minx);
			baxy = maxy + 0.05 * (maxy - miny);
			}
		if ((baxx - binx) > (baxy - biny)) bcartmax = baxx - binx;
		if ((baxy - biny) > (baxx - binx)) bcartmax = baxy - biny;
		}
		
	if (which_button == 3)		/*clic droit*/
		{
		/*bouton droit actionne : deplacement de la figure du segment clique*/
		binx = binx + (float) (button_downx - x) / RESOLUTION_X_FEN  * (caxx - cinx);
		baxx = baxx + (float) (button_downx - x) / RESOLUTION_X_FEN  * (caxx - cinx);
		biny = biny + (float) (button_downy - y) / RESOLUTION_Y_FEN  * (ciny - caxy);
		baxy = baxy + (float) (button_downy - y) / RESOLUTION_Y_FEN  * (ciny - caxy);
		}
	dessiner();
	button_number = 0;
	}





void zoom()
	{
	etat = GetMenuItemChecked(w[4]);
	
 	if (etat==0) 
 		{
 		printf("zoom rendu actif\n");
 		//etat6=GetMenuItemChecked(w[6]);		/*etat de contour*/
 		//etat7=GetMenuItemChecked(w[7]); 	/*etat de fermer contour*/
 		//etat9=GetMenuItemChecked(w[9]); 	/*etat de element*/
  		etat15=GetMenuItemChecked(w[15]); 	/*etat de liaison*/
		//if (etat6==1) reprendre_le_contour=TRUE;
 		//if (etat9==1) reprendre_element=TRUE;
 		if (etat15==1) reprendre_liaison=TRUE;
 		/*printf("etat6 %d  etat7 %d  etat9 %d etat15 %d\n",etat6,etat7,etat9,etat15);*/
		//SetMenuItemChecked(w[6],0);
		//SetMenuItemChecked(w[7],0);
		//SetMenuItemChecked(w[9],0);
		SetMenuItemChecked(w[15],0);
 		SetButtonUpCB(w[2],    button_up_zoom);
 		SetMenuItemChecked(w[4],1);
 		}
	if (etat==1) 
 		{
 		/*printf("etat6 %d  etat7 %d  etat9 %d etat15 %d\n",etat6,etat7,etat9,etat15);*/
		
		//SetMenuItemChecked(w[6],etat6);	/*sans doute inutile*/
		//SetMenuItemChecked(w[7],etat7);
		//SetMenuItemChecked(w[9],etat9);
		SetMenuItemChecked(w[15],etat15);
		
 		printf("zoom rendu inactif\n ");
  		SetButtonUpCB(w[2],    NULL);
		SetMenuItemChecked(w[4],0);
		if (reprendre_le_contour) 
			{
			//SetMenuItemChecked(w[6],0);
			//printf("reprise de la creation du panneau\n");
			//contour();
			}
		if (reprendre_element) 
			{
			/*
			SetMenuItemChecked(w[9],0);
			printf("reprise de la creation de l element\n");
			creer_element();
			*/
			}
		if (reprendre_liaison) 
			{
			/*SetMenuItemChecked(w[15],0);*/
			printf("reprise de la creation de l element\n");
			creer_liaison();
			}
		}
 	}

