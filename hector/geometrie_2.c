#include "hector.h"   

int i;          

void choix_type_de_noeud()
   {
   if (chalut.nb_panneau>0||chalut.nb_element>0)
      {
      TagList tags[] = 
  	      {
    	      {TAG_INT,	"numero a affecter : ",	&type_de_noeud,  	TAG_INIT}, 
   	      {TAG_DONE,	NULL,           	NULL,     		TAG_NOINIT}
	      };



      if(GetValues_2(tags))  printf("annulation\n");
      else
    	      {
	      /*printf("type_de_noeud %d\n",type_de_noeud);*/
	      SetButtonUpCB(w[2],   button_up_type_de_noeud );
    	      //SetMenuItemChecked(w[22],1);
	      }
      }
    /*else printf("il n y a rien de cree !!"); 		ne marche pas ... */
    }
      

 
void button_up_type_de_noeud(Widget w, int which_button, int x, int y, void *data)
  {
  int num_entite,num_pt,mini_num_entite,mini_num_pt,entite;
  float abs_x,x_liaison,dist,mini_dist;
  float abs_y,y_liaison,xx,yy;
  mini_dist=10000;
  
  Color(BLACK);

  if (which_button == 1)	/*clic gauche*/
    {
    
    DrawBox(x,y,3,3);
    x_liaison=binx + (float) x/ RESOLUTION_X_FEN*(baxx-binx);	/*coord x qui prend en compte le zoom. origine en bas a gauche*/
    y_liaison=baxy - (float) y/ RESOLUTION_Y_FEN*(baxy-biny);	/*idem*/
    for (num_entite=1 ; num_entite<=chalut.nb_panneau ; num_entite++)	/*num_entite : num du panneau ou num de l element suivant le cas*/
       {
       for (num_pt=1 ; num_pt<=panneau[num_entite].nb_point ; num_pt++)
	  {
	  abs_x=x_liaison - panneau[num_entite].x[num_pt];		/*abs_x : dist selon x entre le point clique et le point en cours (panneau[].x[])*/
	  abs_y=y_liaison - panneau[num_entite].y[num_pt];
	  dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));			/*calcul de la distance*/

	  if (dist<=mini_dist)			/*recuperation des donnees du point le plus proche sur ce panneau*/
	     {
	     mini_dist=dist;				/*distance mini*/
	     entite=0;
	     mini_num_entite=num_entite;
	     mini_num_pt=num_pt;
	     }
	  }
       }

    for (num_entite=1 ; num_entite<=chalut.nb_element ; num_entite++)	/*meme chose que pour les panneaux*/
       {
       for (num_pt=1 ; num_pt<=2 ; num_pt++)
	  {
	  abs_x=x_liaison - element[num_entite].x[num_pt];
	  abs_y=y_liaison - element[num_entite].y[num_pt];
	  dist=sqrt((abs_x*abs_x)+(abs_y*abs_y));

	  if (dist<=mini_dist)
	     {
	     mini_dist=dist;				/*distance mini*/
	     entite=1;
	     mini_num_entite=num_entite;
	     mini_num_pt=num_pt;
	     }
	  }
       }

     if (entite==0)
	{
	printf("panneau %d noeud %d \n",mini_num_entite,mini_num_pt);
	
	xx = panneau[mini_num_entite].x[mini_num_pt];
	yy = panneau[mini_num_entite].y[mini_num_pt];
	
	i = appartient_liaison(xx,yy);	/*renvoie le num de la liaison a laquelle le point appartient, 0 si n appartient a rien*/
 	
 	/*
	x = ((REEL) panneau[mini_num_entite].x[mini_num_pt] - binx) / (baxx - binx);
	y = ((REEL) panneau[mini_num_entite].y[mini_num_pt] - biny) / (baxy - biny);
        DrawBox(x,y,3,3);
	*/
	
	if (i==0) panneau[mini_num_entite].type_du_noeud[mini_num_pt]=type_de_noeud;
	else	changer_tous_les_types_de_la_liaison();	/*cas ou le noeud appartient a une liaison*/
	}
	
	
	
     if (entite==1)
	{
	printf("element %d noeud %d \n",mini_num_entite,mini_num_pt);
	
	xx = element[mini_num_entite].x[mini_num_pt];
	yy = element[mini_num_entite].y[mini_num_pt];
	
	i = appartient_liaison(xx,yy);	/*renvoie le num de la liaison a laquelle le point appartient, 0 si n appartient a rien*/
	printf("i %d\n",i);
 	/*
	x = ((REEL) element[mini_num_entite].x[mini_num_pt] - binx) / (baxx - binx);
	y = ((REEL) element[mini_num_entite].y[mini_num_pt] - biny) / (baxy - biny);
        DrawBox(x,y,3,3);*/
	
	if (i==0) element[mini_num_entite].type_du_noeud[mini_num_pt]=type_de_noeud;
	else	changer_tous_les_types_de_la_liaison();	/*cas ou le noeud appartient a une liaison*/
	   
	}
     }
   }

void fin_type_de_noeud()
    {
    
    SetButtonUpCB(w[2],    NULL);
    SetMenuItemChecked(w[22],0);
    SetMenuItemChecked(w[4],0);
    }

void changer_tous_les_types_de_la_liaison()
   {
   int point,num_nd,num_entite_2;
  
   printf("TOUS LES POINTS DE LA LIAISON %d SONT MIS AU TYPE %d\n",i,type_de_noeud);
   for (point=1 ; point <= liaison[i].nb_pt ; point++)
      {
      num_entite_2 = liaison[i].num_entite[point];
      num_nd = liaison[i].nd[point];
      if (liaison[i].type[point]==0)	/*si le type est un panneau*/
	  {
	  printf("panneau[%d].type_du_noeud[%d]\n",num_entite_2,num_nd,panneau[num_entite_2].type_du_noeud[num_nd]);
	  panneau[num_entite_2].type_du_noeud[num_nd] = type_de_noeud;
	  }
      if (liaison[i].type[point]==1) 	/*si le type est un element*/
	  {
	  printf("element[%d].type_du_noeud[%d]\n",num_entite_2,num_nd,element[num_entite_2].type_du_noeud[num_nd]);
	  element[num_entite_2].type_du_noeud[num_nd] = type_de_noeud;
	  }
       }
    }





