
/*prototypes de fct_sx.c*/
char *GetFile(char *_path);				

/* fond_decran protos */
void maillage_losange_fond_ecran1();
void maillage_losange_fond_ecran2();
void hachu3(float vh[],float uh1[],float uh2[],float nh,float Uh,float Ui);
void initialisation_nb_fils();
/*void initialisation_nb_fils(int nombre_fils);*/
void dessiner_fils_contour();
void Line(float x1,float y1,float x2, float y2);
void Color(int c);
void Line_ps(float x1,float y1,float x2,float y2);
void Color_ps(int c);
void Chk_ps_open();
void initialisation_min_max();
void initialisation_fond_ecran();
void initialisation_unique();

/*protos de hector.c*/
/*void init_display(int argc, char **argv, MyProgram *me);	mise dans hector.h impossible de mettre ici*/
void initialisation();
void dessiner_contour();	/*PAS BESOIN DE LES DECLARER ON DIRAIT !!!*/
void dessiner_element();	/*PAS BESOIN DE LES DECLARER ON DIRAIT !!!*/
void quit(Widget w,    void *data);
void initialisation_type_de_noeud();
void initialisation_environnement();

/*protos de dessiner.c*/
void dessiner();
void redisplay(Widget w, int new_width, int new_height, void *data);
void Checked_numerotation_panneau();
void dessiner_numero_panneau();
void Checked_numerotation_element();
void dessiner_numero_element();
void Checked_numerotation_liaison();
void dessiner_numero_liaison();
void Checked_dessiner_ordre_maillage();
void Checked_dessiner_nb_mailles();
void dessiner_nb_mailles();

/*protos de zoom.c*/
void button_down(Widget w, int which_button, int x, int y, void *data);
void button_up_zoom(Widget w, int which_button, int x, int y, void *data);
void zoom();

/*protos de choix_utilisateur*/
void coordonnees_par_defaut();
void coordonnees_utilisateur();
void demande_coordonnees_utilisateur();
void changer_lcm();
void demande_lcm_utilisateur();

/*protos de geometrie.c*/
void button_up_contour(Widget w, int which_button, int x, int y, void *data);
void button_up_element(Widget w, int which_button, int x, int y, void *data);
void contour();
void creer_element();
void fermer_contour();
void stop();
void dessiner_contour();
void creer_liaison();
void button_up_liaison(Widget w, int which_button, int x, int y, void *data);
void terminer_liaison();
void ordre_maillage();
void button_up_ordre_maillage(Widget w, int which_button, int x, int y, void *data);
void stop_ordre_maillage();
void dessiner_liaison();
void realloc_panneau();
void affectation_carateristiques_meca_pan();
void affectation_carateristiques_meca_ele();
void dessiner_ordre_maillage();

/*protos de geometrie_2.c*/
void choix_type_de_noeud();
void button_up_type_de_noeud(Widget w, int which_button, int x, int y, void *data);
void fin_type_de_noeud();
void changer_tous_les_types_de_la_liaison();

/*protos de creation_fichier_don.c*/
void creer_fichier_don(Widget w, void *data);
void init_don();
void init_don_el();
void modifier_parametres_pan();
void modifier_parametres_ele();

/*protos de dessiner.c*/
void dessiner_numero_panneau();
void Text(float x,float y,char *str);
void modif_pas_accrochage();
void accrochage();
void accrochage_lancement();
void afficher_tout_ou_rien();

/*protos de aide_au_dessin.c*/
void button_up_effacer2(Widget w, int which_button, int x, int y, void *data);
void effacer();
void effacer_liaison();
void button_up_effacer_liaison(Widget w, int which_button, int x, int y, void *data);

/*protos de outils.c*/
int appartient_liaison(float x, float y);
float liaison_x(int num_liaison,int num_point);
float liaison_y(int num_liaison,int num_point);
void click_me(Widget w, void *data);

/*protos de chargement_don.c*/
void charger();
void initialisation_min_max2();


/*protos de alloc.c*/
int *Malloc_int(int nb);
