#define PRINCIPAL 1

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#include "libsx.h"             /* should come first, defines libsx stuff  */
#include "hector.h"         /* prototypes for callback functions       */

void main(int argc, char **argv)
{
  MyProgram mydata;

  mydata.var1  = 1;                   /* do your initializations here */
  mydata.var2  = 2;
  mydata.flags = 0;
  
  col=GetRGBColor(255,0,0);
  
  
  
  
  /*if (col==-1) printf("Erreur de couleur dessiner_chalut.nb_panneau\n");*/
  SetColor(col);
  
  taille_fenetre = 100;
  lcm = 3;				/*longueur d un cote de maille*/
  init_display(argc, argv, &mydata);  /* setup the display */

  MainLoop();                         /* go right into the main loop */
}



/* This function sets up the display.  For any kind of a real program, 
 * you'll probably want to save the values returned by the MakeXXX calls
 * so that you have a way to refer to the display objects you have 
 * created (like if you have more than one drawing area, and want to
 * draw into both of them).
 */

void init_display(int argc, char **argv, MyProgram *me)
{

  if (OpenDisplay(argc, argv) == FALSE) return;
  initialisation();
  
  			     /*rendu a w[27]  place = w[100]*/
  
  w[0]  = MakeMenu("file");
  //w[3]  = MakeMenuItem(w[0],  "diamond mesh",	maillage_losange_fond_ecran1, me);
  ///w[3]  = MakeMenuItem(w[0],  " ",	maillage_losange_fond_ecran1, me);
  w[24] = MakeMenuItem(w[0],  "open .don file",		charger, me);
  //w[10] = MakeMenuItem(w[0],  "modify panel parameters",	modifier_parametres_pan, me);
  //w[11] = MakeMenuItem(w[0],  "modify element parameters",	modifier_parametres_ele, me);
  //w[10] = MakeMenuItem(w[0],  " ",	modifier_parametres_pan, me);
  //w[11] = MakeMenuItem(w[0],  " ",	modifier_parametres_ele, me);
  w[8]  = MakeMenuItem(w[0],  "create .don file",	creer_fichier_don, me);
  w[1]  = MakeMenuItem(w[0],  "quit",	                quit, me);
 
  w[5]  = MakeMenu("drawing");
  //w[6]  = MakeMenuItem(w[5], "create panel",	contour, me);
  //w[7]  = MakeMenuItem(w[5], "close panel",	fermer_contour, me);
  //w[9]  = MakeMenuItem(w[5], "create element",	creer_element, me);
  //w[6]  = MakeMenuItem(w[5], " ",	contour, me);
  //w[7]  = MakeMenuItem(w[5], " ",	fermer_contour, me);
  //w[9]  = MakeMenuItem(w[5], " ",	creer_element, me);
  w[15] = MakeMenuItem(w[5], "create link",	creer_liaison, me);
  w[16] = MakeMenuItem(w[5], "close link",	terminer_liaison, me);
  //w[20] = MakeMenuItem(w[5], "erase",	effacer, me);
  //w[21] = MakeMenuItem(w[5], "erase link",	effacer_liaison, me);
  //w[20] = MakeMenuItem(w[5], " ",	effacer, me);
  //w[21] = MakeMenuItem(w[5], " ",	effacer_liaison, me);

  //w[19] = MakeMenu("tools");
  //w[14] = MakeMenuItem(w[19],  "hanging",	accrochage_lancement, me);
  //w[13] = MakeMenuItem(w[19],  "modify hanging step",	modif_pas_accrochage, me);
  //w[18] = MakeMenuItem(w[19],  "modify mesh side length",	changer_lcm, me);
  //w[22] = MakeMenuItem(w[19],  "define node types",	choix_type_de_noeud, me);
  //w[23] = MakeMenuItem(w[19],  "end define node types",	fin_type_de_noeud, me);
  //w[17] = MakeMenuItem(w[19],  "define meshing order",	ordre_maillage, me);
  //w[19] = MakeMenu(" ");
  //w[14] = MakeMenuItem(w[19],  " ",	accrochage_lancement, me);
  //w[13] = MakeMenuItem(w[19],  " ",	modif_pas_accrochage, me);
  //w[18] = MakeMenuItem(w[19],  " ",	changer_lcm, me);
  //w[22] = MakeMenuItem(w[19],  " ",	choix_type_de_noeud, me);
  //w[23] = MakeMenuItem(w[19],  " ",	fin_type_de_noeud, me);
  //w[17] = MakeMenuItem(w[19],  " ",	ordre_maillage, me);

  w[27] = MakeMenu("display");
  //w[29] = MakeMenuItem(w[27],  "all / nothing",		afficher_tout_ou_rien, me);
  //w[29] = MakeMenuItem(w[27],  " ",		afficher_tout_ou_rien, me);
  //w[12] = MakeMenuItem(w[27],  "panels numbering",	Checked_numerotation_panneau, me);
  //w[25] = MakeMenuItem(w[27],  "elements numbering",	Checked_numerotation_element, me);
  //w[12] = MakeMenuItem(w[27],  " ",	Checked_numerotation_panneau, me);
  //w[25] = MakeMenuItem(w[27],  " ",	Checked_numerotation_element, me);
  w[26] = MakeMenuItem(w[27],  "links numbering",	Checked_numerotation_liaison, me);
  //w[28] = MakeMenuItem(w[27],  "meshing order",	Checked_dessiner_ordre_maillage, me);
  //w[30] = MakeMenuItem(w[27],  "meshes number",		Checked_dessiner_nb_mailles, me);
  //w[28] = MakeMenuItem(w[27],  " ",	Checked_dessiner_ordre_maillage, me);
  //w[30] = MakeMenuItem(w[27],  " ",		Checked_dessiner_nb_mailles, me);

  w[4]  = MakeButton("zoom",    zoom,     me);

  w[2]  = MakeDrawArea(X_SIZE, Y_SIZE, redisplay, me);
  
  SetWidgetPos(w[5], PLACE_RIGHT, w[0], NO_CARE, NULL);		/*dessin*/
  //SetWidgetPos(w[19], PLACE_RIGHT, w[5], NO_CARE, NULL);	/*outils*/
  SetWidgetPos(w[27], PLACE_RIGHT, w[5], NO_CARE, NULL);	/*affichage*/
   SetWidgetPos(w[4], PLACE_RIGHT, w[27], NO_CARE, NULL);	/*zoom*/

  SetWidgetPos(w[2], PLACE_UNDER, w[0], NO_CARE, NULL);
  
  
  SetButtonDownCB(w[2],  button_down);
  
  /* This call actually causes the whole thing to be displayed on the
   * screen.  You have to call this function before doing any drawing
   * into the window.
   */
   
  ShowDisplay();

  GetStandardColors();
}

void initialisation()
  {
  int n;
  initialisation_type_de_noeud();
  initialisation_environnement();
  init_don();
  init_don_el();
  binx=(-5);
  biny=(-5);
  baxx=105;
  baxy=105;
  chalut.nb_panneau=0;
  chalut.nb_element=0;
  reprendre_le_contour=FALSE;
  reprendre_element=FALSE;
  creation_panneau_terminee=FALSE;
  creation_element_terminee=FALSE;
  for(n=1;n<=chalut.nb_panneau;n++)
	  {
	  panneau[n].contour_ferme=0;
	  }
  pas_accrochage=0.5;
  mode_accrochage=FALSE;
  chalut.nb_pan_hexa=0;
  chalut.nb_coulisse=0;
  chalut.nb_liaison=0;
  nb_entite=0;
  dernier_pan_modifie=1;
  dernier_ele_modifie=1;
  type_de_noeud=1;
  coordonnee_neutre = 2;		/*coordonnee qui sera egale a zero ds .don*/
  flag_ordre_maillage = 0;		/*vaut 0 si il n y a pas d ordre de maillage defini, et 1 si il est defini*/
  }

void initialisation_type_de_noeud()
  {
  int i;
  chalut.nb_type_noeud = 3; 		/*valeur par defaut*/
  for (i=1 ; i<=chalut.nb_type_noeud ; i++)
    {
    TypeNoeud[i].mx = 0.0; TypeNoeud[i].my = 0.0; TypeNoeud[i].mz = 0.0; 
    TypeNoeud[i].majx = 0.0; TypeNoeud[i].majy = 0.0; TypeNoeud[i].majz = 0.0; 
    TypeNoeud[i].lonx = 0.0; TypeNoeud[i].lony = 0.0; TypeNoeud[i].lonz = 0.0; 
    TypeNoeud[i].cdx = 1.2; TypeNoeud[i].cdy = 1.2; TypeNoeud[i].cdz = 1.2; 
    TypeNoeud[i].fextx = 0.0; TypeNoeud[i].fexty = 0.0; TypeNoeud[i].fextz = 0.0; 
    }
    
  TypeNoeud[1].fixx = 1; TypeNoeud[1].fixy = 1; TypeNoeud[1].fixz = 1; 
  TypeNoeud[1].limx = 0.0; TypeNoeud[1].limy = 0.0; TypeNoeud[1].limz = 0.0; 
  TypeNoeud[1].senx = 0; TypeNoeud[1].seny = 0; TypeNoeud[1].senz = 0; 
  TypeNoeud[1].symx = 0; TypeNoeud[1].symy = 0; TypeNoeud[1].symz = 0; 
  
  TypeNoeud[2].fixx = 0; TypeNoeud[2].fixy = 0; TypeNoeud[2].fixz = 0; 
  TypeNoeud[2].limx = 0; TypeNoeud[2].limy = 0.0; TypeNoeud[2].limz = -50.0; 
  TypeNoeud[2].senx = 0; TypeNoeud[2].seny = 0; TypeNoeud[2].senz = 1; 
  TypeNoeud[2].symx = 0; TypeNoeud[2].symy = 0; TypeNoeud[2].symz = 0; 
  
  TypeNoeud[3].fixx = 0; TypeNoeud[3].fixy = 0; TypeNoeud[3].fixz = 0; 
  TypeNoeud[3].limx = 0; TypeNoeud[3].limy = 0.0; TypeNoeud[3].limz = -50.0; 
  TypeNoeud[3].senx = 0; TypeNoeud[3].seny = 0; TypeNoeud[3].senz = 1; 
  TypeNoeud[3].symx = 0; TypeNoeud[3].symy = 1; TypeNoeud[3].symz = 0; 
  
  }


void initialisation_environnement()
  {
  Courant.direction = 0.0;
  Courant.vitesse = 2.058;
  
  Houle.periode = 10.0;
  Houle.hauteur = 0.0;
  Houle.direction = 0.0;
  Houle.Depth1 = 50.0;
  
  Prise.volume = 0.0;
  Prise.seuil = 0.00001;
  Prise.cd = 1.0;
  
  Fond.coef_frottement = 0.5;
  Fond.raideur = 5000000;
  }




void redisplay(Widget w, int new_width, int new_height, void *data)	/*redessine systematiquement la figure*/
	{
	/*MyProgram *me = data;*/
	RESOLUTION_X_FEN = (float)new_width ;		/*variable du zoom*/
	RESOLUTION_Y_FEN = (float)new_height ;
	dessiner();
	}


void dessiner()
	{
	int mode;
	ClearDrawArea();
	///mode = GetMenuItemChecked(w[3]);
	///if (mode == 1)  dessiner_fils_contour();
	dessiner_contour();
	dessiner_element();
	dessiner_liaison();
	//mode = GetMenuItemChecked(w[12]);
	//if (mode == 1)  dessiner_numero_panneau();
	//mode = GetMenuItemChecked(w[25]);
	//if (mode == 1)  dessiner_numero_element();
	mode = GetMenuItemChecked(w[26]);
	if (mode == 1)  dessiner_numero_liaison();
	//mode = GetMenuItemChecked(w[28]);
	//if (mode == 1)  dessiner_ordre_maillage();
	//mode = GetMenuItemChecked(w[30]);
	//if (mode == 1)  dessiner_nb_mailles();
	}

void quit(Widget w, void *data)
{
  /* Do any cleanup that is necessary for your program here */

  exit(0);
}


