#include "hector.h" 

	
void Text(float x,float y,char *str)
	{
	float dx,dy;

	if (flag_delta_carac == TRUE)
		{
		dx =  (float)(taille_police_x*delta_caract_x/2);
		dy =  (float)(taille_police_y*delta_caract_y/2);
		}
	else
		{
		dx=0;
		dy=0;
		}

  	DrawText(str, 
		(int)(                    ( RESOLUTION_X_FEN * x )/RESOLUTION_X+dx),
		(int)(RESOLUTION_Y_FEN - (( RESOLUTION_Y_FEN * y )/RESOLUTION_Y+dy)) 
		);
	
	}

void Checked_numerotation_panneau()
	{
	int mode;
	/*
	mode = GetMenuItemChecked(w[12]);
	if (mode == 0) SetMenuItemChecked(w[12],1);
	if (mode == 1) SetMenuItemChecked(w[12],0);
	dessiner();
	*/
	}
	
void dessiner_numero_panneau()
	{
	int pa,no;
	float x_1,y_1;
	char str[80];
	
	Color(BLUE);

	for (pa=1;pa<=chalut.nb_panneau;pa++)
		{
		x_1 = 0.0;
		y_1 = 0.0;
		sprintf(str,"%d",pa);
		for (no=1;no<=panneau[pa].nb_point;no++)
			{ 
			x_1 += (float) (panneau[pa].x[no] - binx) / (baxx - binx);
			y_1 += (float) (panneau[pa].y[no] - biny) / (baxy - biny);
			}
		x_1 = (float) (x_1 / panneau[pa].nb_point);
		y_1 = (float) (y_1 / panneau[pa].nb_point);
		Text(x_1,y_1,str);
		}
	}

void Checked_numerotation_element()
	{
	int mode;
	/*
	mode = GetMenuItemChecked(w[25]);
	if (mode == 0) SetMenuItemChecked(w[25],1);
	if (mode == 1) SetMenuItemChecked(w[25],0);
	dessiner();
	*/
	}
	
void dessiner_numero_element()
	{
	int pa,deb,fin,col;
	float x_1,y_1;
	char str[80];
	
	col=GetRGBColor(0,255,0);
	if (col==-1) printf("Erreur de couleur dessiner_numero_element\n");
	SetColor(col);
	
	Color(RED);

	for (pa=1;pa<=chalut.nb_element;pa++)
		{ 
		sprintf(str,"%d",pa);
		deb = element[pa].extremite[1];
		fin = element[pa].extremite[2];
		x_1  = (float) (element[pa].x[1] - binx) / (baxx - binx);
		y_1  = (float) (element[pa].y[1] - biny) / (baxy - biny);
		x_1 += (float) (element[pa].x[2] - binx) / (baxx - binx);
		y_1 += (float) (element[pa].y[2] - biny) / (baxy - biny);
		x_1 = (float) (x_1 / 2.0);
		y_1 = (float) (y_1 / 2.0);
		Text(x_1,y_1,str);
		}
	}
	
void Checked_numerotation_liaison()
	{
	int mode;
	mode = GetMenuItemChecked(w[26]);
	if (mode == 0) SetMenuItemChecked(w[26],1);
	if (mode == 1) SetMenuItemChecked(w[26],0);
	dessiner();
	}
	
void dessiner_numero_liaison()
   {
   int zi,zj,no_struct,no_pt;
   float xx,yy;
   char str[80];
   col=GetRGBColor(0,0,255);
   if (col==-1) printf("Erreur de couleur dessiner_numero_liaison\n");
   SetColor(col);
   
   Color(BLACK);
   for (zi=1;zi<=chalut.nb_liaison;zi++)
      {
      sprintf(str,"%d",zi);
      for (zj=1;zj<=liaison[zi].nb_pt;zj++)
         {
         no_struct = liaison[zi].num_entite[zj];
         no_pt = liaison[zi].nd[zj];
         
         if (liaison[zi].type[zj] == 0)
            {
            xx = panneau[no_struct].x[no_pt];
            yy = panneau[no_struct].y[no_pt];
            }
         if (liaison[zi].type[zj] == 1)
            {
            xx = element[no_struct].x[no_pt];
            yy = element[no_struct].y[no_pt];
            }
         xx = (float) (xx - binx)/(baxx - binx);
         yy = (float) (yy - biny)/(baxy - biny);
         Text(xx,yy,str);
         }
      }
   }	
	
void modif_pas_accrochage()
     {
      TagList tags[] = 
  	     {
    	     {TAG_FLOAT,	"hanging step (m): ",	&pas_accrochage,  	TAG_INIT}, 
 	     {TAG_DONE,	NULL,           	NULL,     		TAG_NOINIT}
  	     };

     if(GetValues_2(tags))
    	     printf("Cancelled\n");
     else
    	     {
    	     printf("hanging step after modification %f\n",pas_accrochage);
    	     }
     }
   
void accrochage_lancement()
	{
	/*
	int mode_accrochage_lancement = GetMenuItemChecked(w[14]);
	if (mode_accrochage_lancement)
	   {
	   SetMenuItemChecked(w[14],0);
	   mode_accrochage=FALSE;
	   }
	else
	   {
	   SetMenuItemChecked(w[14],1);
	   mode_accrochage=TRUE;
	   }
	*/
 	}

	
void Checked_dessiner_ordre_maillage()
	{
	int mode;
/*
	mode = GetMenuItemChecked(w[28]);
	if (mode == 0) SetMenuItemChecked(w[28],1);
	if (mode == 1) SetMenuItemChecked(w[28],0);
	dessiner();
*/
	}
	

void dessiner_ordre_maillage()
   {
   int j,entite;
   Color(YELLOW);
   
   printf("chalut.nb_ordre_objet %d\n",chalut.nb_ordre_objet);
   for (entite = 1;entite<=chalut.nb_panneau; entite++)
      {
      if (panneau[entite].nb_point>=2 && panneau[entite].flag_ordre_maillage==1)
	   {
	   /*printf("panneau[%d].nb_point %d\n",entite,panneau[entite].nb_point);*/
	   for (j=2;j<=panneau[entite].nb_point;j++)
	      {
	      xxx1 = panneau[entite].x[j - 1];
	      xxx2 = panneau[entite].x[j];
	      yyy1 = panneau[entite].y[j - 1];
	      yyy2 = panneau[entite].y[j];
	      xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);
	      xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
	      yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
	      yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);

	      Line(xxx1,yyy1,xxx2,yyy2);
	      }
	   xxx1=xxx2;
	   yyy1=yyy2;
	   xxx2=panneau[entite].x[1];
	   yyy2=panneau[entite].y[1];
	   xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
	   yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);

	   Line(xxx1,yyy1,xxx2,yyy2);
	   }
       }

    for (entite = 1;entite<=chalut.nb_panneau; entite++)
      {
      if (element[entite].nb_point>=2 && element[entite].flag_ordre_maillage==1)
	   {
	   xxx1 = element[entite].x[1];
	   xxx2 = element[entite].x[2];
	   yyy1 = element[entite].y[1];
	   yyy2 = element[entite].y[2];
	   xxx1 = ((REEL) xxx1 - binx) / (baxx - binx);
	   xxx2 = ((REEL) xxx2 - binx) / (baxx - binx);
	   yyy1 = ((REEL) yyy1 - biny) / (baxy - biny);
	   yyy2 = ((REEL) yyy2 - biny) / (baxy - biny);

	   Line(xxx1,yyy1,xxx2,yyy2);
	   }
       }
   }

	
void Checked_dessiner_nb_mailles()
	{
	int mode;
	//mode = GetMenuItemChecked(w[30]);
	//if (mode == 0) SetMenuItemChecked(w[30],1);
	//if (mode == 1) SetMenuItemChecked(w[30],0);
	//dessiner();
	}
	

void dessiner_nb_mailles()
	{
	int pa,no;
	float x_1,y_1;
	char str[80];
	
	Color(BLACK);

	for (pa=1;pa<=chalut.nb_panneau;pa++)
		{
		for (no=1;no<=panneau[pa].nb_point;no++)
			{ 
			x_1 = (float) (panneau[pa].x[no] - binx) / (baxx - binx);
			y_1 = (float) (panneau[pa].y[no] - biny) / (baxy - biny);
			sprintf(str,"%g",panneau[pa].U[no]);
			Text(x_1,y_1,str);
			sprintf(str,"%g",panneau[pa].V[no]);
			Text(x_1,y_1-0.02,str);
			}
		}
	}


void afficher_tout_ou_rien()
	{
	int mode;
/*
	mode = GetMenuItemChecked(w[29]);
	if (mode == 0) 
	   {
	   SetMenuItemChecked(w[12],1);
	   SetMenuItemChecked(w[25],1);
	   SetMenuItemChecked(w[26],1);
	   SetMenuItemChecked(w[28],1);
	   SetMenuItemChecked(w[29],1);
	   }
	if (mode == 1)
	   {
	   SetMenuItemChecked(w[12],0);
	   SetMenuItemChecked(w[25],0);
	   SetMenuItemChecked(w[26],0);
	   SetMenuItemChecked(w[28],0);
	   SetMenuItemChecked(w[29],0);
	   }
	dessiner();
*/
	}
	







