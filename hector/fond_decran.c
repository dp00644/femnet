				/*ATTENTION Chk_ps_open A ETE ENLEVE.. cf triangle.c de phobos*/
				
				
/*
pour l instant les hexa ne sont pas traites mais ils sont 
conserves dans dessiner_fils_contour pour la suite des modifications
*/

#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include "libsx.h"
#include "hector.h"
#include <string.h>		/*pas sur que ce soit utile : provient de phobos.h*/
#include <stdlib.h>


	/*fonction juste pour permettre d afficher le message d erreur si le maillage est deja cree*/
	
void maillage_losange_fond_ecran1()
	{
	/*///
	if(GetMenuItemChecked(w[3])==0) 
		{
		maillage_losange_fond_ecran2();
		}
	else printf("le maillage est deja cree\n");
	///*/
	}


				/*creation des fils dans toute la fenetre*/
				
void maillage_losange_fond_ecran2()			/* COPIE DE fils_contour_calculer() */
	{
	int ind,triangle_rectangle;
	float U1,U2,U3,V1,V2,V3,n1,n2,n3,m1,m2,m3,nh,Uh,Ui;
	float ve1[4],ve2[4],ve3[4],u12[4],u23[4],u31[4],v12[4],v23[4],v31[4],vh[4],uh1[4],uh2[4];
	
	periode_fils = 1;
   		
	coordonnees_utilisateur();					/*demande les coordonnees a l utilisateur, en affecte par defaut sinon*/
	initialisation_fond_ecran();					/*malloc des variables de la fonction en cours*/
   	///if(GetMenuItemChecked(w[3])==0) initialisation_unique();	/*initialisation qui ne doit etre faite qu une seul fois (cf mallocs)*/
	initialisation_min_max();
   	
	for (triangle_rectangle = 1 ; triangle_rectangle <= 2 ; triangle_rectangle++)
		{
		U1 = VV1[triangle_rectangle] + UU1[triangle_rectangle]; V1 = VV1[triangle_rectangle] - UU1[triangle_rectangle];
		U2 = VV2[triangle_rectangle] + UU2[triangle_rectangle]; V2 = VV2[triangle_rectangle] - UU2[triangle_rectangle];
		U3 = VV3[triangle_rectangle] + UU3[triangle_rectangle]; V3 = VV3[triangle_rectangle] - UU3[triangle_rectangle];

 		U1 = (float) U1 / periode_fils;		V1 = (float) V1 / periode_fils;
		U2 = (float) U2 / periode_fils;		V2 = (float) V2 / periode_fils;
		U3 = (float) U3 / periode_fils;		V3 = (float) V3 / periode_fils;

		ve1[1] = xx1[triangle_rectangle]; 	ve1[2] = yy1[triangle_rectangle]; 	ve1[3] = zz1[triangle_rectangle];
		ve2[1] = xx2[triangle_rectangle]; 	ve2[2] = yy2[triangle_rectangle]; 	ve2[3] = zz2[triangle_rectangle];
		ve3[1] = xx3[triangle_rectangle]; 	ve3[2] = yy3[triangle_rectangle]; 	ve3[3] = zz3[triangle_rectangle];

		n1 = fabs(U3-U2);
		n2 = fabs(U1-U3);
		n3 = fabs(U2-U1);

		m1 = fabs(V3-V2);
		m2 = fabs(V1-V3);
		m3 = fabs(V2-V1);

		if (n3 != 0) 
			{
			u12[1] = (ve2[1]-ve1[1]) / n3; 
			u12[2] = (ve2[2]-ve1[2]) / n3; 
			u12[3] = (ve2[3]-ve1[3]) / n3; 
			}
		else 
			{
			u12[1] = 0.0;
			u12[2] = 0.0;
			u12[3] = 0.0;
			}
		if (m3 != 0) 
			{
			v12[1] = (ve2[1]-ve1[1]) / m3; 
			v12[2] = (ve2[2]-ve1[2]) / m3; 
			v12[3] = (ve2[3]-ve1[3]) / m3; 
			}
		else 
			{
			v12[1] = 0.0;
			v12[2] = 0.0;
			v12[3] = 0.0;
			}

		if (n2 != 0) 
			{
			u31[1] = (ve1[1]-ve3[1]) / n2; 
			u31[2] = (ve1[2]-ve3[2]) / n2; 
			u31[3] = (ve1[3]-ve3[3]) / n2; 
			}
		else 
			{
			u31[1] = 0.0;
			u31[2] = 0.0;
			u31[3] = 0.0;
			}
		if (m2 != 0) 
			{
			v31[1] = (ve1[1]-ve3[1]) / m2; 
			v31[2] = (ve1[2]-ve3[2]) / m2; 
			v31[3] = (ve1[3]-ve3[3]) / m2; 
			}
		else 
			{
			v31[1] = 0.0;
			v31[2] = 0.0;
			v31[3] = 0.0;
			}

		if (n1 != 0) 
			{
			u23[1] = (ve3[1]-ve2[1]) / n1; 
			u23[2] = (ve3[2]-ve2[2]) / n1; 
			u23[3] = (ve3[3]-ve2[3]) / n1; 
			}
		else 
			{
			u23[1] = 0.0;
			u23[2] = 0.0;
			u23[3] = 0.0;
			}
		if (m1 != 0) 
			{
			v23[1] = (ve3[1]-ve2[1]) / m1; 
			v23[2] = (ve3[2]-ve2[2]) / m1; 
			v23[3] = (ve3[3]-ve2[3]) / m1; 
			}
		else 
			{
			v23[1] = 0.0;
			v23[2] = 0.0;
			v23[3] = 0.0;
			}
		if ((n1>=n2) && (n1>=n3)) ind = 1;
		if ((n2>=n3) && (n2>=n1)) ind = 2;
		if ((n3>=n1) && (n3>=n2)) ind = 3;

		if (ind == 1)
			{
		  	vh [1] =  ve2[1]; vh [2] =  ve2[2]; vh [3] =  ve2[3]; /*vh sommet de reference du triangle*/
  			uh1[1] =  u23[1]; uh1[2] =  u23[2]; uh1[3] =  u23[3]; /*uh vecteur de separation des fils*/
  			uh2[1] = -u12[1]; uh2[2] = -u12[2]; uh2[3] = -u12[3];
  			nh = n3;  
		  	Uh = U2; Ui = U3;  
  			hachu3(vh,uh1,uh2,nh,Uh,Ui);
  			vh [1] =  ve3[1]; vh [2] =  ve3[2]; vh [3] =  ve3[3]; 
  			uh1[1] = -u23[1]; uh1[2] = -u23[2]; uh1[3] = -u23[3]; 
	  		uh2[1] =  u31[1]; uh2[2] =  u31[2]; uh2[3] =  u31[3];
	  		nh = n2;  
  			Uh = U3; Ui = U2;  
  			hachu3(vh,uh1,uh2,nh,Uh,Ui);
	  		}
		if (ind == 2)
			{
  			vh [1] =  ve1[1]; vh [2] =  ve1[2]; vh [3] =  ve1[3]; 
	  		uh1[1] = -u31[1]; uh1[2] = -u31[2]; uh1[3] = -u31[3]; 
  			uh2[1] =  u12[1]; uh2[2] =  u12[2]; uh2[3] =  u12[3];
	  		nh = n3;  
  			Uh = U1; Ui = U3;  

	  		hachu3(vh,uh1,uh2,nh,Uh,Ui);
  			vh [1] =  ve3[1]; vh [2] =  ve3[2]; vh [3] =  ve3[3]; 
	  		uh1[1] =  u31[1]; uh1[2] =  u31[2]; uh1[3] =  u31[3]; 
  			uh2[1] = -u23[1]; uh2[2] = -u23[2]; uh2[3] = -u23[3];
	  		nh = n1;  
  			Uh = U3; Ui = U1;  
  			hachu3(vh,uh1,uh2,nh,Uh,Ui);
	  		}

		if (ind == 3)
			{
  			vh [1] =  ve1[1]; vh [2] =  ve1[2]; vh [3] =  ve1[3]; 
  			uh1[1] = -u31[1]; uh1[2] = -u31[2]; uh1[3] = -u31[3]; 
		  	uh2[1] =  u12[1]; uh2[2] =  u12[2]; uh2[3] =  u12[3];
  			nh = n2;  
  			Uh = U1; Ui = U2;  
  			hachu3(vh,uh1,uh2,nh,Uh,Ui);
	  		vh [1] =  ve2[1]; vh [2] =  ve2[2]; vh [3] =  ve2[3]; 
	  		uh1[1] = -u12[1]; uh1[2] = -u12[2]; uh1[3] = -u12[3]; 
  			uh2[1] =  u23[1]; uh2[2] =  u23[2]; uh2[3] =  u23[3];
  			nh = n1;  
	  		Uh = U2; Ui = U1;  
  			hachu3(vh,uh1,uh2,nh,Uh,Ui);
	  		}

		if ((m1>=m2) && (m1>=m3)) ind = 1;
		if ((m2>=m3) && (m2>=m1)) ind = 2;
		if ((m3>=m1) && (m3>=m2)) ind = 3;

		if (ind == 1)
			{
		  	vh [1] =  ve2[1]; vh [2] =  ve2[2]; vh [3] =  ve2[3]; 
  			uh1[1] =  v23[1]; uh1[2] =  v23[2]; uh1[3] =  v23[3]; 
  			uh2[1] = -v12[1]; uh2[2] = -v12[2]; uh2[3] = -v12[3];
  			nh = m3;  
	  		Uh = V2; Ui = V3;  
	  		hachu3(vh,uh1,uh2,nh,Uh,Ui);
  			vh [1] =  ve3[1]; vh [2] =  ve3[2]; vh [3] =  ve3[3]; 
  			uh1[1] = -v23[1]; uh1[2] = -v23[2]; uh1[3] = -v23[3]; 
	  		uh2[1] =  v31[1]; uh2[2] =  v31[2]; uh2[3] =  v31[3];
  			nh = m2;  
	  		Uh = V3; Ui = V2;  
  			hachu3(vh,uh1,uh2,nh,Uh,Ui);
	  		}
		if (ind == 2)
			{
  			vh [1] =  ve1[1]; vh [2] =  ve1[2]; vh [3] =  ve1[3]; 
	  		uh1[1] = -v31[1]; uh1[2] = -v31[2]; uh1[3] = -v31[3]; 
  			uh2[1] =  v12[1]; uh2[2] =  v12[2]; uh2[3] =  v12[3];
  			nh = m3;  
	  		Uh = V1; Ui = V3;  
		  	hachu3(vh,uh1,uh2,nh,Uh,Ui);
  			vh [1] =  ve3[1]; vh [2] =  ve3[2]; vh [3] =  ve3[3]; 
  			uh1[1] =  v31[1]; uh1[2] =  v31[2]; uh1[3] =  v31[3]; 
  			uh2[1] = -v23[1]; uh2[2] = -v23[2]; uh2[3] = -v23[3];
		  	nh = m1;  
  			Uh = V3; Ui = V1;  
  			hachu3(vh,uh1,uh2,nh,Uh,Ui);
  			}
		if (ind == 3)
			{
  			vh [1] =  ve1[1]; vh [2] =  ve1[2]; vh [3] =  ve1[3]; 
  			uh1[1] = -v31[1]; uh1[2] = -v31[2]; uh1[3] = -v31[3]; 
  			uh2[1] =  v12[1]; uh2[2] =  v12[2]; uh2[3] =  v12[3];
		  	nh = m2;  
  			Uh = V1; Ui = V2;  
  			hachu3(vh,uh1,uh2,nh,Uh,Ui);
	  		vh [1] =  ve2[1]; vh [2] =  ve2[2]; vh [3] =  ve2[3]; 
	  		uh1[1] = -v12[1]; uh1[2] = -v12[2]; uh1[3] = -v12[3]; 
  			uh2[1] =  v23[1]; uh2[2] =  v23[2]; uh2[3] =  v23[3];
  			nh = m1;  
	  		Uh = V2; Ui = V1;  
	  		hachu3(vh,uh1,uh2,nh,Uh,Ui);
  			}
		}
	dessiner_fils_contour();
	
	///SetMenuItemChecked(w[3],1);
	}
	

void hachu3(float vh[],float uh1[],float uh2[],float nh,float Uh,float Ui)
	{
	/*
	vh est le sommet a partir duquel on calcule les fils
	uh1 est le premier vecteur de separation des fils sur le premier cote
	uh2 est le second  vecteur de separation des fils sur le second  cote
	*/
	float debut;
	int ind,nb;
	col=GetRGBColor(0,255,255);	
	
	if (col==-1) printf("Erreur de couleur hachu3\n");
	SetColor(col);

	
	
	if (Ui > Uh)
		{
		debut = (float) (ceil ((double) Uh) - Uh); 
		}
	else 
		{
		debut = (float) (Uh - floor ((double) Uh));
		}
	nb = (int) (floor(nh-debut+1));
	for (ind=1;ind<=nb;ind++)
		{
		
		chalut.nb_fils_fond_ecran++;
		/**********************************************/
		h1x = (float *) realloc(h1x, (1+ chalut.nb_fils_fond_ecran) * sizeof(float));
		if (h1x    == NULL)
			{
			printf("h1x  2 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		h1y = (float *) realloc(h1y, (1+ chalut.nb_fils_fond_ecran) * sizeof(float));
		if (h1y    == NULL)
			{
			printf("h1y  2 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		h2x = (float *) realloc(h2x, (1+ chalut.nb_fils_fond_ecran) * sizeof(float));
		if (h2x    == NULL)
			{
			printf("h2x  2 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		h2y = (float *) realloc(h2y, (1+ chalut.nb_fils_fond_ecran) * sizeof(float));
		if (h2y    == NULL)
			{
			printf("h2y  2 = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		
		h1x[chalut.nb_fils_fond_ecran] = vh[1]+debut*uh1[1] + (ind-1) * uh1[1]; /*coordonnee selon x de l extremite 1 du fil*/
		h2x[chalut.nb_fils_fond_ecran] = vh[1]+debut*uh2[1] + (ind-1) * uh2[1]; /*coordonnee selon x de l extremite 2 du fil*/
		h1y[chalut.nb_fils_fond_ecran] = vh[2]+debut*uh1[2] + (ind-1) * uh1[2]; /*coordonnee selon y de l extremite 1 du fil*/
		h2y[chalut.nb_fils_fond_ecran] = vh[2]+debut*uh2[2] + (ind-1) * uh2[2]; /*coordonnee selon y de l extremite 2 du fil*/

		/*printf("fil no %d h1x %8.2f h2x %8.2f h1y %8.2f h2y %8.2f\n",chalut.nb_fils_fond_ecran,h1x,h2x,h1y,h2y);*/
		}
	}

	
void dessiner_fils_contour()
	{
	int no_tri_hexa,deb,no_pa;
	int pre,sec,tro,um,vm,minu,maxu,minv,maxv,int_temp;
	float x1,x2,x3,y_1,y2,y3;
	float a1,a2,b1,b2,ru,rv;
	float u1,u2,u3,v1,v2,v3,xm,ym,alpha,beta,denom,vect_ux,vect_vx,vect_uy,vect_vy;
	int appartient_triangle4(double um, double vm, double u1, double v1, double u2, double v2, double u3, double v3);
	void trace_fil_hexa(float a1, float b1, float a2, float b2, float x1, float y_1, float x2, float y2, float x3, float y3);
	
	/*
	x1,x2,y_1,y2 sont les coordonnees des extremites de segment a desssiner.
	x1,x2,y_1,y2 sont compris entre 0 et 1. 
	0 correspond au bas ou a gauche ?
	1 correspond en haut et a droite de l ecran ?
	*/
	   	
	Color(GREEN);
	for (deb=1;deb<=chalut.nb_fils_fond_ecran;deb++)
		{
		x1 = ((REEL) h1x[deb] - binx) / (baxx - binx); /*coordonnee comprise entre 0 et 1 du fil selon x*/
		y_1 = ((REEL) h1y[deb] - biny) / (baxy - biny);
		x2 = ((REEL) h2x[deb] - binx) / (baxx - binx);
		y2 = ((REEL) h2y[deb] - biny) / (baxy - biny);
		Line(x1,y_1,x2,y2);
		}

	Line(x1,y_1,x2,y2);

	/*
	x1,x2,y_1,y2 sont les coordonnees des extremites de segment a desssiner.
	x1,x2,y_1,y2 sont compris entre 0 et 1. 
	0 correspond au bas ou a gauche ?
	1 correspond en haut et a droite de l ecran ?
	*/


	Color(BLUE);
			/********************************************************/
			/*******************TRAITEMENT DES HEXAS*****************/
			/********************************************************/

	for (no_pa=1;no_pa<=chalut.nb_pan_hexa;no_pa++)
		{ 
		for (no_tri_hexa=1;no_tri_hexa<=pan_hexa[no_pa].nb_tri_hexa_contour;no_tri_hexa++)
			{ 

			int_temp = pan_hexa[no_pa].tri_hexa_contour[no_tri_hexa][1];	pre = pan_hexa[no_pa].noeud_contour[int_temp];
			int_temp = pan_hexa[no_pa].tri_hexa_contour[no_tri_hexa][2];	sec = pan_hexa[no_pa].noeud_contour[int_temp];
			int_temp = pan_hexa[no_pa].tri_hexa_contour[no_tri_hexa][3];	tro = pan_hexa[no_pa].noeud_contour[int_temp];


			u1 = (float) noeud[pre].U / periode_fils;	v1 = (float) noeud[pre].V / periode_fils;
			u2 = (float) noeud[sec].U / periode_fils;	v2 = (float) noeud[sec].V / periode_fils;
			u3 = (float) noeud[tro].U / periode_fils;	v3 = (float) noeud[tro].V / periode_fils;
			x1 = noeud[pre].x;	y_1 = noeud[pre].y;
			x2 = noeud[sec].x;	y2 = noeud[sec].y;
			x3 = noeud[tro].x;	y3 = noeud[tro].y;



	       		denom = (u3-u1)*(v2-v1) - (u2-u1)*(v3-v1);
	       		/* precautions en cas de denominateur nul*/
	       		if (fabs(denom) <= 0.00001)
	               		{  
	               		} 
	       		/*calcul des vecteur elementaires cartesiens de cote de maille vect_u et vect_v*/

	       		ru = u1+1.0;
	       		rv = v1+0.0;


	     		alpha =   ( (rv-v1)*(u3-u1) - (ru-u1)*(v3-v1) ) / denom;
	     		beta  =   ( (ru-u1)*(v2-v1) - (rv-v1)*(u2-u1) ) / denom;

	       		/* determination des coordonnees cartesiennes du sommet courant proche d une position d equilibre */
	       		vect_ux = alpha * ( x2 - x1 )+ beta * ( x3 - x1 );
	       		vect_uy = alpha * ( y2 - y_1 )+ beta * ( y3 - y_1 );

	       		ru = u1+0.0;
	       		rv = v1+1.0;

	     		alpha =   ( (rv-v1)*(u3-u1) - (ru-u1)*(v3-v1) ) / denom;
	     		beta  =   ( (ru-u1)*(v2-v1) - (rv-v1)*(u2-u1) ) / denom;

	       		/* determination des coordonnees cartesiennes du sommet courant proche d une position d equilibre */
	       		vect_vx = alpha * ( x2 - x1 )+ beta * ( x3 - x1 );
	       		vect_vy = alpha * ( y2 - y_1 )+ beta * ( y3 - y_1 );



			minu = (int) floor(u1);		maxu = (int) ceil(u1);
			minv = (int) floor(v1);		maxv = (int) ceil(v1);
			if (minu>u2) minu = (int) floor(u2);  if (maxu<u2) maxu = (int) ceil(u2);
			if (minu>u3) minu = (int) floor(u3);  if (maxu<u3) maxu = (int) ceil(u3);
			if (minv>v2) minv = (int) floor(v2);  if (maxv<v2) maxv = (int) ceil(v2);
			if (minv>v3) minv = (int) floor(v3);  if (maxv<v3) maxv = (int) ceil(v3);


			for (um=minu;um<=maxu;um++)
				{ 
				for (vm=minv;vm<=maxv;vm++)
					{
	       				/* determination des coefficients alpha et beta definis tels que 1m = alpha*12 + beta*13 */
	     				alpha =   ( (vm-v1)*(u3-u1) - (um-u1)*(v3-v1) ) / denom;
	     				beta  =   ( (um-u1)*(v2-v1) - (vm-v1)*(u2-u1) ) / denom;

	       				/* determination des coordonnees cartesiennes de l origine de la maille proche d une position d equilibre */
	       				xm = alpha * ( x2 - x1 )+ beta * ( x3 - x1 ) + x1;
	       				ym = alpha * ( y2 - y_1 )+ beta * ( y3 - y_1 ) + y_1;

	         			/*trace des 6 fils a l interieur d une maille*/
					/*
					Line(((REEL) xm-0.08-binx) / (baxx-binx),((REEL) ym-biny) / (baxy-biny),((REEL) xm+0.08-binx) / (baxx-binx),((REEL) ym-biny) / (baxy-biny));
					Line(((REEL) xm-binx) / (baxx-binx),((REEL) ym-0.08-biny) / (baxy-biny),((REEL) xm-binx) / (baxx-binx),((REEL) ym+0.08-biny) / (baxy-biny));
	         			*/

					a1 = xm;
					b1 = ym;
					a2 = xm +vect_ux;
					b2 = ym +vect_uy;
					/*
	         			printf("a1= %f,b1 = %f\n",a1,b1);
	         			printf("a2= %f,b2 = %f\n",a2,b2);
	         			printf("x1= %f,y_1 = %f\n",x1,y_1);
	         			printf("x2= %f,y2 = %f\n",x2,y2);
	         			printf("x3= %f,y3 = %f\n",x3,y3);
					trace_fil_hexa( a1,  b1,  a2,  b2,  x1,  y_1,  x2,  y2,  x3,  y3);


					a2 = xm +vect_vx;
					b2 = ym +vect_vy;
					trace_fil_hexa( a1,  b1,  a2,  b2,  x1,  y_1,  x2,  y2,  x3,  y3);
					*/

					a1 = xm +vect_ux*1.0/6.0 +vect_vx*1.0/2.0;
					b1 = ym +vect_uy*1.0/6.0 +vect_vy*1.0/2.0;
					a2 = xm +vect_ux*0.0/6.0 +vect_vx*0.0/2.0;
					b2 = ym +vect_uy*0.0/6.0 +vect_vy*0.0/2.0;
					trace_fil_hexa( a1,  b1,  a2,  b2,  x1,  y_1,  x2,  y2,  x3,  y3);
					a2 = xm +vect_ux*0.0/6.0 +vect_vx*2.0/2.0;
					b2 = ym +vect_uy*0.0/6.0 +vect_vy*2.0/2.0;
					trace_fil_hexa( a1,  b1,  a2,  b2,  x1,  y_1,  x2,  y2,  x3,  y3);
					a2 = xm +vect_ux*3.0/6.0 +vect_vx*1.0/2.0;
					b2 = ym +vect_uy*3.0/6.0 +vect_vy*1.0/2.0;
					trace_fil_hexa( a1,  b1,  a2,  b2,  x1,  y_1,  x2,  y2,  x3,  y3);
					a1 = xm +vect_ux*4.0/6.0 +vect_vx*2.0/2.0;
					b1 = ym +vect_uy*4.0/6.0 +vect_vy*2.0/2.0;
					trace_fil_hexa( a1,  b1,  a2,  b2,  x1,  y_1,  x2,  y2,  x3,  y3);
					a1 = xm +vect_ux*4.0/6.0 +vect_vx*0.0/2.0;
					b1 = ym +vect_uy*4.0/6.0 +vect_vy*0.0/2.0;
					trace_fil_hexa( a1,  b1,  a2,  b2,  x1,  y_1,  x2,  y2,  x3,  y3);
					a2 = xm +vect_ux*6.0/6.0 +vect_vx*0.0/2.0;
					b2 = ym +vect_uy*6.0/6.0 +vect_vy*0.0/2.0;
					trace_fil_hexa( a1,  b1,  a2,  b2,  x1,  y_1,  x2,  y2,  x3,  y3);
					}
				}
			}
		}
	}
	
void trace_fil_hexa(float a1, float b1, float a2, float b2, float x1, float y_1, float x2, float y2, float x3, float y3)
	{
	/*a1,b1 a a2,b2 est le fil a trace dans le triangle de sommets x1,y_1 x2,y2 x3,y3*/
	int inside1,inside2,secant;
	double xx,yy;
	int appartient_triangle4(double um, double vm, double u1, double v1, double u2, double v2, double u3, double v3);
	int segment_secant(double a1, double b1, double a2, double b2, double x1, double y_1, double x2, double y2);
	void intersection_segment(double a1, double b1, double a2, double b2, double x1, double y_1, double x2, double y2, double *x,double *y);
	
	inside1 = appartient_triangle4((double) a1, (double) b1, (double) x1, (double) y_1, (double) x2, (double) y2, (double) x3, (double) y3);
	inside2 = appartient_triangle4((double) a2, (double) b2, (double) x1, (double) y_1, (double) x2, (double) y2, (double) x3, (double) y3);
	if ((inside1 == 1) && (inside2 == 1))
		{
		/*ici les 2 extremites a1,b1 et a2,b2 du fil sont dans le triangle 1 2 3, on peut le trace*/
		Line(((REEL) a1-binx) / (baxx-binx),((REEL) b1-biny) / (baxy-biny),((REEL) a2-binx) / (baxx-binx),((REEL) b2-biny) / (baxy-biny));
		}
	/*ici l extremite a1,b1 est dans le triangle 1 2 3 et a2,b2 est a l exterieur*/
	if ((inside1 == 1) && (inside2 != 1))
		{
		secant =  segment_secant((double) a1, (double) b1, (double) a2, (double) b2, (double) x1, (double) y_1, (double) x2, (double) y2);
		if (secant == 1)
			{
			intersection_segment((double) a1, (double) b1, (double) a2, (double) b2, (double) x1, (double) y_1, (double) x2, (double) y2, &xx, &yy);
			Line(((REEL) a1-binx) / (baxx-binx),((REEL) b1-biny) / (baxy-biny),((REEL) xx-binx) / (baxx-binx),((REEL) yy-biny) / (baxy-biny));
			}
		secant =  segment_secant((double) a1, (double) b1, (double) a2, (double) b2, (double) x2, (double) y2, (double) x3, (double) y3);
		if (secant == 1)
			{
			intersection_segment((double) a1, (double) b1, (double) a2, (double) b2, (double) x2, (double) y2, (double) x3, (double) y3, &xx, &yy);
			Line(((REEL) a1-binx) / (baxx-binx),((REEL) b1-biny) / (baxy-biny),((REEL) xx-binx) / (baxx-binx),((REEL) yy-biny) / (baxy-biny));
			}
		secant =  segment_secant((double) a1, (double) b1, (double) a2, (double) b2, (double) x3, (double) y3, (double) x1, (double) y_1);
		if (secant == 1)
			{
			intersection_segment((double) a1, (double) b1, (double) a2, (double) b2, (double) x3, (double) y3, (double) x1, (double) y_1, &xx, &yy);
			Line(((REEL) a1-binx) / (baxx-binx),((REEL) b1-biny) / (baxy-biny),((REEL) xx-binx) / (baxx-binx),((REEL) yy-biny) / (baxy-biny));
			}
		}
	if ((inside1 != 1) && (inside2 == 1))
		{
		secant =  segment_secant((double) a1, (double) b1, (double) a2, (double) b2, (double) x1, (double) y_1, (double) x2, (double) y2);
		if (secant == 1)
			{
			intersection_segment((double) a1, (double) b1, (double) a2, (double) b2, (double) x1, (double) y_1, (double) x2, (double) y2, &xx, &yy);
			Line(((REEL) a2-binx) / (baxx-binx),((REEL) b2-biny) / (baxy-biny),((REEL) xx-binx) / (baxx-binx),((REEL) yy-biny) / (baxy-biny));
			}
		secant =  segment_secant((double) a1, (double) b1, (double) a2, (double) b2, (double) x2, (double) y2, (double) x3, (double) y3);
		if (secant == 1)
			{
			intersection_segment((double) a1, (double) b1, (double) a2, (double) b2, (double) x2, (double) y2, (double) x3, (double) y3, &xx, &yy);
			Line(((REEL) a2-binx) / (baxx-binx),((REEL) b2-biny) / (baxy-biny),((REEL) xx-binx) / (baxx-binx),((REEL) yy-biny) / (baxy-biny));
			}
		secant =  segment_secant((double) a1, (double) b1, (double) a2, (double) b2, (double) x3, (double) y3, (double) x1, (double) y_1);
		if (secant == 1)
			{
			intersection_segment((double) a1, (double) b1, (double) a2, (double) b2, (double) x3, (double) y3, (double) x1, (double) y_1, &xx, &yy);
			Line(((REEL) a2-binx) / (baxx-binx),((REEL) b2-biny) / (baxy-biny),((REEL) xx-binx) / (baxx-binx),((REEL) yy-biny) / (baxy-biny));
			}
		}
	}
	
int appartient_triangle4(double um, double vm, double u1, double v1, double u2, double v2, double u3, double v3)
	{
	float  S, S_ref ;
	int inside;
	float surface_triangle(float X1, float y_1, float X2, float Y2, float X3, float Y3 );

	/*
	printf(" ");
	printf("um   : %8.2lf ,vm  :  %8.2lf ",um,vm);
	RECHERCHE SI LE NOEUD M DE COORDONNEES FILAIRE um ET vm EST A L INTERIEUR DU CONTOUR  DU triangle 1 2 3 
	de coordonnees u1,v1, u2,v2, u3,v3
	SI A L INTERIEUR DU CONTOUR RETURN = 1
	SI A L EXTERIEUR DU CONTOUR RETURN = 0
	*/

	/*S_REF = SURFACE DU TRIANGLE u1,v1, u2,v2, u3,v3*/
	S_ref = surface_triangle( (float) u1, (float) v1, (float) u2, (float) v2, (float) u3, (float) v3);
	/*S = somme des 3 SURFACEs des TRIANGLEs 12M 13M 23M*/
	S = 0.0;
	S    += surface_triangle( (float) u1, (float) v1, (float) u2, (float) v2, (float) um, (float) vm);
	S    += surface_triangle((float)  u2, (float) v2, (float) u3, (float) v3, (float) um, (float) vm);
	S    += surface_triangle( (float) u3, (float) v3, (float) u1, (float) v1, (float) um, (float) vm);
	/*printf("S_ref   : %8.2lf , S :  %8.2lf \n",S_ref,S);*/
	if ((S / S_ref > 0.999) && (S / S_ref < 1.001))
		{
		/*LE NOEUD M EST A L INTERIEUR DU triangle*/
		inside = 1;
		}
	else
		{
		/*LE NOEUD M EST A L exterieur DU triangle*/
		inside = 0;
		}
	return(inside);
	}
	
int segment_secant(double a1, double b1, double a2, double b2, double x1, double y_1, double x2, double y2)
	{
	float  S, S_ref ;
	int secant;
	float surface_triangle(float X1, float y_1, float X2, float Y2, float X3, float Y3 );

	/*
	RECHERCHE SI LE segment a1,b1 a a2,b2 coupe le segment x1,y_1 a x2,y2
	SI les segments sont secants RETURN = 1
	SI non RETURN = 0
	*/

	/*S_REF = somme des SURFACEs du TRIANGLE a1,b1, x1,y_1, x2,y2 et du triangle a2,b2, x1,y_1, x2,y2*/
	/*S     = somme des SURFACEs du TRIANGLE x1,y_1, a1,b1, a2,b2 et du triangle x2,y2, a1,b1, a2,b2*/
	
	S_ref 	= 0.0;
	S_ref += surface_triangle( (float) a1, (float) b1, (float) x1, (float) y_1, (float) x2, (float) y2);
	S_ref += surface_triangle( (float) a2, (float) b2, (float) x1, (float) y_1, (float) x2, (float) y2);
	S 	= 0.0;
	S    += surface_triangle( (float) x1, (float) y_1, (float) a1, (float) b1, (float) a2, (float) b2);
	S    += surface_triangle( (float) x2, (float) y2, (float) a1, (float) b1, (float) a2, (float) b2);
	
	/*printf("S_ref   : %8.5f , S :  %8.5f \n",S_ref,S);*/
	
	if (S_ref != 0.0)
		{
		if ((S / S_ref > 0.999) && (S / S_ref < 1.001))
			{
			/*les segments sont secants*/
			secant = 1;
			}
		else
			{
			/*les segments ne sont pas secants*/
			secant = 0;
			}
		}
	else
		{
		/*les 2 segments sont alignes il faudrait verifier s ils se chevauchent*/
		secant = 0;
		}
	/*printf("secant   : %d d \n",secant);*/
	return(secant);
	}
	
void intersection_segment(double a1, double b1, double a2, double b2, double x1, double y_1, double x2, double y2, double *x,double *y)
  	{
  	/*calcul le point d intersection x y de 2 segments a1,b1 a a2,b2 et de x1,y_1 a x2,y2*/
  	

  	
	*x = a1;
	*y = b1;
	
	if (b2 != b1)
		{
		if ((a2-a1)*(y2-y_1) != (b2-b1)*(x2-x1))
			{
			*y = (b1*(a2-a1)/(b2-b1)*(y2-y_1) - y_1*(x2-x1) - (a1-x1)*(y2-y_1)) / ((a2-a1)/(b2-b1)*(y2-y_1) - (x2-x1));
			*x = a1 + (a2-a1)/(b2-b1) * (*y-b1);
			}
		else
			{
			/*pas sur du tout : A VERIFIER*/
			*x = a1;
			*y = b1;
			}
		}
	else
		{
		*y = b1;
		if (y2 != y_1)
			{
			*x = x1 +(x2-x1)/(y2-y_1)*(*y-y_1);
			}
		else
			{
			/*pas sur du tout : A VERIFIER*/
			*x = a1;
			}
		}
	
  	}
  	


float surface_triangle(float X1, float y_1, float X2, float Y2, float X3, float Y3 )
	{
	float A,B,C,p,S;
	
	/*X1,X2,X3,y_1,Y2,Y3 = COORDONNEES DU TRIANGLE*/

  	/* COTES A OPPOSE AU SOMMET_1*/ 
  	/* COTES B OPPOSE AU SOMMET_2*/ 
  	/* COTES C OPPOSE AU SOMMET_3*/
    	A = sqrt((X3-X2)*(X3-X2) + (Y3-Y2)*(Y3-Y2));
    	B = sqrt((X1-X3)*(X1-X3) + (y_1-Y3)*(y_1-Y3));
    	C = sqrt((X2-X1)*(X2-X1) + (Y2-y_1)*(Y2-y_1));
  
    	/*P : DEMI PERIMETRE DU TRIANGLE*/
    	p = (A + B + C)/2;

    	/*S : SURFACE DU TRIANGLE*/
    	S = sqrt(p*(p-A)*(p-B)*(p-C));
    	
	return S;
	}


void Line(float x1,float y_1,float x2, float y2)
	{
	float dx,dy;
	
	if (flag_delta_carac == TRUE)
		{
		dx =  (float)(taille_police_x*delta_caract_x/2);
		dy =  (float)(taille_police_y*delta_caract_y/2);
		printf("dx %8.2f  dy %8.2f\n",dx,dy);
		}
	else
		{
		dx=0;
		dy=0;
		}
	
	DrawLine(
 	        (int)       		(  RESOLUTION_X_FEN * x1 )/RESOLUTION_X+dx,
		(int) RESOLUTION_Y_FEN -(( RESOLUTION_Y_FEN * y_1 )/RESOLUTION_Y+dy),
		(int) 	                (  RESOLUTION_X_FEN * x2 )/RESOLUTION_X+dx,
		(int) RESOLUTION_Y_FEN -(( RESOLUTION_Y_FEN * y2 )/RESOLUTION_Y+dy)
		);

	if (flag_ps == TRUE) Line_ps(x1,y_1,x2,y2);
	}


void Color(int c)
	{
	if (flag_ps == TRUE) Color_ps(c);
	SetColor(c);		
	}


void Line_ps(float x1,float y_1,float x2,float y2)
	{
	float fx1,fy_1;
	float fx2,fy2;
	float dx,dy,rx,ry;
	printf("Line_ps");
	exit(0);
	if (flag_delta_carac == TRUE)
		{
		dx = (float)(largeur_police_ps*delta_caract_x/2);
		dy = (float)(hauteur_police_ps*delta_caract_y/2);
		}
	else
		{
		dx=0;
		dy=0;
		}

	rx= RESOLUTION_X_PS;
	ry= RESOLUTION_Y_PS;
	if (flag_ortho_norm== TRUE)
		{
		if(mode_portrait == FALSE) ry = ry /  ((RESOLUTION_Y-(RESOLUTION_Y*0.3))/RESOLUTION_Y+1.);
		}

	if (mode_portrait == TRUE)
		{
		fx1 = ( rx * x1 )/RESOLUTION_X+MARGE_X_PS+dx;
		fy_1 = ( ry * y_1 )/RESOLUTION_Y+MARGE_Y_PS+dy;
		fx2 = ( rx * x2 )/RESOLUTION_X+MARGE_X_PS+dx;
		fy2 = ( ry * y2 )/RESOLUTION_Y+MARGE_Y_PS+dy;
		}
	else
		{
		fx1 = ( ry * x1 )/RESOLUTION_Y+MARGE_Y_PS+dx;
		fy_1 = ( rx * y_1 )/RESOLUTION_X+MARGE_X_PS+dy;
		fx2 = ( ry * x2 )/RESOLUTION_Y+MARGE_Y_PS+dx;
		fy2 = ( rx * y2 )/RESOLUTION_X+MARGE_X_PS+dy;
		}

	fprintf(fic_ps,"%.4f %.4f moveto %.4f %.4f  lineto stroke \n",fx1,fy_1,fx2,fy2);
	}


void Color_ps(int c)
	{
	/*c== BLUE;*/
	if (c== BLACK) fprintf(fic_ps,"0 0 0 setrgbcolor\n"); 
	else if (c== BLUE)  fprintf(fic_ps,"0 0 1 setrgbcolor\n"); 
	else if (c== GREEN) fprintf(fic_ps,"0 1 0 setrgbcolor\n");
	/*else if (c== 3) fprintf(fic_ps,"0 1 1 setrgbcolor\n");*/
	else if (c== RED) fprintf(fic_ps,"1 0 0 setrgbcolor\n");
	/*else if (c== 5) fprintf(fic_ps,"1 0 1 setrgbcolor\n");*/
	else if (c== YELLOW) fprintf(fic_ps,"1 1 0 setrgbcolor\n");
	else if (c== WHITE) fprintf(fic_ps,"1 1 1 setrgbcolor\n");
	else  fprintf(fic_ps,"%f setgray\n", ((float) c)/ 256.);
	}
	
	
void initialisation_min_max()
	{
	int no;
	
	/*initialisation des min et des max*********************************************/
	minx = 0;	maxx = taille_fenetre;
	miny = 0;	maxy = taille_fenetre;
	minz = 0;	maxz = 0;
	for (no=1;no<=chalut.nb_total;no++)
		{
		if(noeud[no].x < minx) minx = noeud[no].x;
		if(noeud[no].x > maxx) maxx = noeud[no].x;
		if(noeud[no].y < miny) miny = noeud[no].y;
		if(noeud[no].y > maxy) maxy = noeud[no].y;
		if(noeud[no].z < minz) minz = noeud[no].z;
		if(noeud[no].z > maxz) maxz = noeud[no].z;
		}
	if ((maxx == minx) && (maxy == miny) && (maxz == minz))
		{
		maxx = minx + 1.0;
		maxy = miny + 1.0;
		maxz = minz + 1.0;
		}	
	if ((maxx == minx) && (maxy == miny) && (maxz != minz))
		{
		maxx = minx + maxz - minz;
		maxy = miny + maxz - minz;
		}
	if ((maxx == minx) && (maxy != miny) && (maxz == minz))
		{
		maxx = minx + maxy - miny;
		maxz = minz + maxy - miny;
		}
	if ((maxx != minx) && (maxy == miny) && (maxz == minz))
		{
		maxy = miny + maxx - minx;
		maxz = minz + maxx - minx;
		}
	if ((maxx == minx) && (maxy != miny) && (maxz != minz))
		{
		maxx = minx + maxy - miny;
		}
	if ((maxx != minx) && (maxy == miny) && (maxz != minz))
		{
		maxy = miny + maxz - minz;
		}
	if ((maxx != minx) && (maxy != miny) && (maxz == minz))
		{
		maxz = minz + maxx - minx;
		}
		
		
			if ((maxx - minx) > (maxy - miny) && (maxx - minx) > (maxz - minz) ) ecartmax = maxx - minx;
	if ((maxy - miny) > (maxz - minz) && (maxy - miny) > (maxx - minx) ) ecartmax = maxy - miny;
	if ((maxz - minz) > (maxx - minx) && (maxz - minz) > (maxy - miny) ) ecartmax = maxz - minz;
	binx = minx - 0.05 * (maxx - minx);
	biny = miny - 0.05 * (maxy - miny);
	baxx = maxx + 0.05 * (maxx - minx);
	baxy = maxy + 0.05 * (maxy - miny);
	bcartmax = ecartmax;
	}
	
			/*cette initialisation doit etre faite 
			chaque fois que l on relance le calcul des fils
			du fond d ecran*/

void initialisation_fond_ecran()
	{

	
   	chalut.nb_fils_fond_ecran = 0;
	/**********************************************/
  	h1x = (float *) malloc((1 + chalut.nb_fils_fond_ecran) * sizeof(float));
	if (h1x    == NULL)
		{
		printf("h1x  = NULL  \n" );
		exit(0);
		}
	/**********************************************/
  	h2x = (float *) malloc((1 + chalut.nb_fils_fond_ecran) * sizeof(float));
	if (h2x    == NULL)
		{
		printf("h2x  = NULL  \n" );
		exit(0);
		}
	/**********************************************/
  	h1y = (float *) malloc((1 + chalut.nb_fils_fond_ecran) * sizeof(float));
	if (h1y    == NULL)
		{
		printf("h1y  = NULL  \n" );
		exit(0);
		}
	/**********************************************/
  	h2y = (float *) malloc((1 + chalut.nb_fils_fond_ecran) * sizeof(float));
	if (h2y    == NULL)
		{
		printf("h2y  = NULL  \n" );
		exit(0);
		}
	/**********************************************/
	

	}
	
	

				/*cette initialisation ne doit etre faite 
				qu une seule fois tout au cours du programme*/

void initialisation_unique()
	{
	int i;
		
			/*MALLOC DES VARIABLES DU panneau....voir si tout n est pas obsolete maintenant que l on ne demande plus le nb 
			de panneau et d elements a creer d entree a l utilisateur*/



	for(i=1;i<=chalut.nb_panneau;i++)
		{
		/**********************************************/
  		panneau[i].x = (float *) malloc((1 + 0) * sizeof(float));
		if (panneau[i].x    == NULL)
			{
			printf("panneau[i].x  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		panneau[i].y = (float *) malloc((1 + 0) * sizeof(float));
		if (panneau[i].y    == NULL)
			{
			printf("panneau[i].y  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		panneau[i].U = (float *) malloc((1 + 0) * sizeof(float));
		if (panneau[i].U    == NULL)
			{
			printf("panneau[i].U  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		panneau[i].V = (float *) malloc((1 + 0) * sizeof(float));
		if (panneau[i].V    == NULL)
			{
			printf("panneau[i].V  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
	panneau[i].type_du_noeud = Malloc_int(0+1);
	element[i].type_du_noeud = Malloc_int(0+1);
		}
	
		
	for(i=1;i<=chalut.nb_liaison;i++)
		{
		/**********************************************/
  		liaison[i].type = (int *) malloc((1 + 0) * sizeof(int));
		if (liaison[i].type   == NULL)
			{
			printf("liaison[i].type  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		liaison[i].num_entite = (int *) malloc((1 + 0) * sizeof(int));
		if (liaison[i].num_entite   == NULL)
			{
			printf("liaison[i].num_entite  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
  		liaison[i].nd = (int *) malloc((1 + 0) * sizeof(int));
		if (liaison[i].nd   == NULL)
			{
			printf("liaison[i].nd  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
   		liaison[i].x = (float *) malloc((1 + 0) * sizeof(float));
		if (liaison[i].x   == NULL)
			{
			printf("liaison[i].x  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
 		liaison[i].y = (float *) malloc((1 + 0) * sizeof(float));
		if (liaison[i].y   == NULL)
			{
			printf("liaison[i].y  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
 		liaison[i].type_du_noeud = (int *) malloc((1 + 0) * sizeof(int));
		if (liaison[i].type_du_noeud   == NULL)
			{
			printf("liaison[i].type_du_noeud  = NULL  \n" );
			exit(0);
			}
		/**********************************************/
		}
	}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
